// get colors array from the string
function getChartColorsArray(chartId) {
    var colors = $(chartId).attr('data-colors');
    var colors = JSON.parse(colors);
    return colors.map(function (value) {
        var newValue = value.replace(' ', '');
        if (newValue.indexOf('--') != -1) {
            var color = getComputedStyle(document.documentElement).getPropertyValue(newValue);
            if (color) return color;
        } else {
            return newValue;
        }
    })
}

//  MINI CHART

// mini-1
var barchartColors = getChartColorsArray("#mini-chart1");
var options = {
    series: [60, 40],
    chart: {
        type: 'donut',
        height: 110,
    },
    colors: barchartColors,
    legend: {
        show: false
    },
    dataLabels: {
        enabled: false
    }
};

var chart = new ApexCharts(document.querySelector("#mini-chart1"), options);
chart.render();

function initCounterNumber() {
    var counter = document.querySelectorAll('.counter-value');
    var speed = 250; // The lower the slower
    counter.forEach(function(counter_value) {
        function updateCount() {
            var target = +counter_value.getAttribute('data-target');
            var count = +counter_value.innerText;
            var inc = target / speed;
            if (inc < 1) {
                inc = 1;
            }
            // Check if target is reached
            if (count < target) {
                // Add inc to count and output in counter_value
                counter_value.innerText = (count + inc).toFixed(0);
                // Call function every ms
                setTimeout(updateCount, 1);
            } else {
                counter_value.innerText = target;
            }
        };
        updateCount();
    });
}