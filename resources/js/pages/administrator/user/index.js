$(document).ready(function () {
    $('.datatable').DataTable({
      responsive: false,
      ajax: {
        url: route_datatable,
        type: "post",
        data: function (data) {
            data._token = $("meta[name='csrf-token']").attr("content");
            data.keyword = $("#keyword").val();
            data.rw = $("#rw").val();
            data.kelurahan = $("#kelurahan").val();
            data.kecamatan = $("#kecamatan").val();
        }
    },
    });
    $(".dataTables_length select").addClass('form-select form-select-sm');
  })