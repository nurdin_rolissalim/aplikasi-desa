@extends('layouts.master')
@section('title')
    Administrator - Hak Akses
@endsection
@section('css')
    <link href="{{ URL::asset('assets/libs/dropzone/dropzone.min.css') }}" rel="stylesheet">
@endsection
@section('content')
    @component('components.breadcrumb')
        @slot('li_1')
            <a href="{{ route('role.index') }}">Hak Akses</a>
        @endslot
        @slot('title')
            Form Hak Akses
        @endslot
    @endcomponent

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form method="post" class="needs-validation form-validate" novalidate action={{ $route }}>
                        @csrf

                        <input type="hidden" value="{{ $data->id ?? '' }}" id="id" name="id">
                        <div class="row mb-3">
                            <label for="name" class="col-sm-3 col-form-label">Nama</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" value="{{ $data->name ?? '' }}"
                                    placeholder="Nama" id="name" name="name" required>
                                <div class="invalid-feedback">
                                    Nama harus diisi
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="status" class="col-sm-3 col-form-label">Status</label>
                            <div class="col-sm-9">
                                <div class="form-check mb-3">
                                    <input class="form-check-input" type="radio" name="status" id="active"
                                        value="1" {{ !empty($data->status && $data->status == 1) ? 'checked' : '' }}>
                                    <label class="form-check-label" for="aktif">
                                        Aktif
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="status" id="inaktive"
                                        value="0" {{ !empty($data->status && $data->status == 0) ? 'checked' : '' }}>
                                    <label class="form-check-label" for="status">
                                        TIdak Aktif
                                    </label>
                                </div>
                            </div>
                        </div>

                        <button class="btn btn-primary" type="submit">Simpan</button>
                    </form>
                </div>
                <!-- end card body -->
            </div>
        </div>
    </div>
    <!-- end col -->
    <!-- end row -->
@endsection
@section('script')
    <script src="{{ URL::asset('assets/libs/dropzone/dropzone.min.js') }}"></script>
    <script src="{{ URL::asset('assets/js/pages/form-validation.init.js') }}"></script>
    <script src="{{ URL::asset('/assets/js/app.min.js') }}"></script>
@endsection
