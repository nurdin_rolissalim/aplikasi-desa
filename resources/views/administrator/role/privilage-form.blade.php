@extends('layouts.master')
@section('title')
    Administrator - Hak Akses
@endsection
@section('css')
    <link href="{{ URL::asset('assets/libs/dropzone/dropzone.min.css') }}" rel="stylesheet">
@endsection
@section('content')
    @component('components.breadcrumb')
        @slot('li_1')
            <a href="{{ route('role.index') }}">Hak Akses</a>
        @endslot
        @slot('title')
            Setting Hak Akses {{ $data->name }}
        @endslot
    @endcomponent

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form method="post" class="needs-validation form-validate" novalidate action={{ $route }}>
                        @csrf

                        <input type="hidden" value="{{ $data->id ?? '' }}" id="id" name="id">
                        <div class="list-data-menu">
                            @if ($menus['total'] > 0)
                                @foreach ($menus['data'] as $item)
                                    @if ($item['child'] === null)
                                        <div class="list-item-menu border p-2 ms-3 mb-1  "
                                            style="border-left:0.3rem solid #32a1ce !important">
                                            <div class="d-flex justify-content-between">
                                                <div>{{ $item['name'] }}
                                                    <input class="ms-1 role_menu form-check-input" type="checkbox"
                                                        name="{{ $item['id'] }}[]" value="all"
                                                        {{ !empty($permission && $permission[$item['id']] && array_search('all', $permission[$item['id']]) > -1) ? 'checked' : '' }}
                                                        data-item-id="{{ $item['id'] }}" id="all_{{ $item['id'] }}">
                                                    <label class=" form-check-label" for="all_{{ $item['id'] }}">
                                                        All
                                                    </label>
                                                </div>
                                                <div class="d-flex flex-row">
                                                    <!-- item-->
                                                    <div class="ms-1">
                                                        <input class="ms-1 role_menu_checked form-check-input"
                                                            type="checkbox" id="view_{{ $item['id'] }}"
                                                            name="{{ $item['id'] }}[]"
                                                            {{ !empty($permission && $permission[$item['id']] && array_search('view', $permission[$item['id']]) > -1) ? 'checked' : '' }}
                                                            data-checked-parent="{{ $item['id'] }}" value="view">
                                                        <label class=" form-check-label" for="view_{{ $item['id'] }}">
                                                            View
                                                        </label>
                                                    </div>

                                                    <div class="ms-1">
                                                        <input class="ms-1 form-check-input role_menu_checked"
                                                            type="checkbox" id="add_{{ $item['id'] }}"
                                                            name="{{ $item['id'] }}[]"
                                                            {{ !empty($permission && $permission[$item['id']] && array_search('add', $permission[$item['id']]) > -1) ? 'checked' : '' }}
                                                            data-checked-parent="{{ $item['id'] }}" value="add">
                                                        <label class=" form-check-label" for="add_{{ $item['id'] }}">
                                                            Tambah
                                                        </label>
                                                    </div>
                                                    <div class="ms-1">
                                                        <input class="ms-1 form-check-input role_menu_checked"
                                                            type="checkbox" id="edit_{{ $item['id'] }}"
                                                            name="{{ $item['id'] }}[]"
                                                            {{ !empty($permission && $permission[$item['id']] && array_search('edit', $permission[$item['id']]) > -1) ? 'checked' : '' }}
                                                            data-checked-parent="{{ $item['id'] }}" value="edit">
                                                        <label class=" form-check-label" for="edit_{{ $item['id'] }}">
                                                            Edit
                                                        </label>
                                                    </div>
                                                    <div class="ms-1">
                                                        <input class="ms-1 form-check-input role_menu_checked"
                                                            type="checkbox" id="delete_{{ $item['id'] }}"
                                                            name="{{ $item['id'] }}[]"
                                                            {{ !empty($permission && $permission[$item['id']] && array_search('delete', $permission[$item['id']]) > -1) ? 'checked' : '' }}
                                                            data-checked-parent="{{ $item['id'] }}" value="delete">
                                                        <label class=" form-check-label" for="delete_{{ $item['id'] }}">
                                                            Delete
                                                        </label>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    @else
                                        <div class="list-item-menu">
                                            <div class="list-item-menu border p-2 ms-3 mb-1  "
                                                style="border-left:0.3rem solid #32a1ce !important">
                                                <div class="d-flex justify-content-between">
                                                    <div>{{ $item['name'] }}
                                                        <input class="ms-1 role_menu form-check-input" type="checkbox"
                                                            value="all" id="all_{{ $item['id'] }}"
                                                            data-item-id="{{ $item['id'] }}"
                                                            {{ !empty($permission && $permission[$item['id']] && array_search('all', $permission[$item['id']]) > -1) ? 'checked' : '' }}
                                                            name="{{ $item['id'] }}[]">
                                                        <label class=" form-check-label" for="all_{{ $item['id'] }}">
                                                            All
                                                        </label>
                                                    </div>
                                                    <div class="d-flex flex-row">
                                                        <!-- item-->
                                                        <div class="ms-1">
                                                            <input class="ms-1 role_menu_checked form-check-input"
                                                                type="checkbox" id="view_{{ $item['id'] }}"
                                                                {{ !empty($permission && $permission[$item['id']] && array_search('view', $permission[$item['id']]) > -1) ? 'checked' : '' }}
                                                                name="{{ $item['id'] }}[]" value="view"
                                                                data-checked-parent="{{ $item['id'] }}">
                                                            <label class=" form-check-label"
                                                                for="view_{{ $item['id'] }}">
                                                                View
                                                            </label>
                                                        </div>

                                                        <div class="ms-1">
                                                            <input class="ms-1 form-check-input role_menu_checked"
                                                                type="checkbox" id="add_{{ $item['id'] }}"
                                                                name="{{ $item['id'] }}[]" value="add"
                                                                {{ !empty($permission && $permission[$item['id']] && array_search('add', $permission[$item['id']]) > -1) ? 'checked' : '' }}
                                                                data-checked-parent="{{ $item['id'] }}">
                                                            <label class=" form-check-label" for="add_{{ $item['id'] }}">
                                                                Tambah
                                                            </label>
                                                        </div>
                                                        <div class="ms-1">
                                                            <input class="ms-1 form-check-input role_menu_checked"
                                                                type="checkbox" id="edit_{{ $item['id'] }}"
                                                                name="{{ $item['id'] }}[]" value="edit"
                                                                {{ !empty($permission && $permission[$item['id']] && array_search('edit', $permission[$item['id']]) > -1) ? 'checked' : '' }}
                                                                data-checked-parent="{{ $item['id'] }}">
                                                            <label class=" form-check-label"
                                                                for="edit_{{ $item['id'] }}">
                                                                Edit
                                                            </label>
                                                        </div>
                                                        <div class="ms-1">
                                                            <input class="ms-1 form-check-input role_menu_checked"
                                                                type="checkbox" id="delete_{{ $item['id'] }}"
                                                                name="{{ $item['id'] }}[]" value="delete"
                                                                {{ !empty($permission && $permission[$item['id']] && array_search('delete', $permission[$item['id']]) > -1) ? 'checked' : '' }}
                                                                data-checked-parent="{{ $item['id'] }}">
                                                            <label class=" form-check-label"
                                                                for="delete_{{ $item['id'] }}">
                                                                Delete
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="list-item-menu  ms-3  mb-1">
                                                @foreach ($item['child'] as $itemChild)
                                                    @if ($itemChild['child'] === null)
                                                        <div class="list-item-menu border p-2 ms-3 mb-1  "
                                                            style="border-left:0.3rem solid #32a1ce !important">
                                                            <div class="d-flex justify-content-between">
                                                                <div>{{ $itemChild['name'] }}
                                                                    <input class="ms-1 role_menu form-check-input"
                                                                        type="checkbox" value="all"
                                                                        {{ !empty($permission && $permission[$itemChild['id']] && array_search('all', $permission[$itemChild['id']]) > -1) ? 'checked' : '' }}
                                                                        data-item-id="{{ $itemChild['id'] }}"
                                                                        name="{{ $itemChild['id'] }}[]"
                                                                        id="all_{{ $itemChild['id'] }}">
                                                                    <label class=" form-check-label"
                                                                        for="all_{{ $itemChild['id'] }}">
                                                                        All
                                                                    </label>
                                                                </div>
                                                                <div class="d-flex flex-row">
                                                                    <!-- item-->
                                                                    <div class="ms-1">
                                                                        <input
                                                                            class="ms-1 role_menu_checked form-check-input"
                                                                            type="checkbox"
                                                                            {{ !empty($permission && $permission[$itemChild['id']] && array_search('view', $permission[$itemChild['id']]) > -1) ? 'checked' : '' }}
                                                                            id="view_{{ $itemChild['id'] }}"
                                                                            name="{{ $itemChild['id'] }}[]"
                                                                            value="view"
                                                                            data-checked-parent="{{ $itemChild['id'] }}">
                                                                        <label class=" form-check-label"
                                                                            for="view_{{ $itemChild['id'] }}">
                                                                            View
                                                                        </label>
                                                                    </div>

                                                                    <div class="ms-1">
                                                                        <input
                                                                            class="ms-1 form-check-input role_menu_checked"
                                                                            type="checkbox"
                                                                            {{ !empty($permission && $permission[$itemChild['id']] && array_search('add', $permission[$itemChild['id']]) > -1) ? 'checked' : '' }}
                                                                            id="add_{{ $itemChild['id'] }}"
                                                                            name="{{ $itemChild['id'] }}[]"
                                                                            value="add"
                                                                            data-checked-parent="{{ $itemChild['id'] }}">
                                                                        <label class=" form-check-label"
                                                                            for="add_{{ $itemChild['id'] }}">
                                                                            Tambah
                                                                        </label>
                                                                    </div>
                                                                    <div class="ms-1">
                                                                        <input
                                                                            class="ms-1 form-check-input role_menu_checked"
                                                                            type="checkbox"
                                                                            {{ !empty($permission && $permission[$itemChild['id']] && array_search('edit', $permission[$itemChild['id']]) > -1) ? 'checked' : '' }}
                                                                            id="edit_{{ $itemChild['id'] }}"
                                                                            name="{{ $itemChild['id'] }}[]"
                                                                            value="edit"
                                                                            data-checked-parent="{{ $itemChild['id'] }}">
                                                                        <label class=" form-check-label"
                                                                            for="edit_{{ $itemChild['id'] }}">
                                                                            Edit
                                                                        </label>
                                                                    </div>
                                                                    <div class="ms-1">
                                                                        <input
                                                                            class="ms-1 form-check-input role_menu_checked"
                                                                            type="checkbox"
                                                                            {{ !empty($permission && $permission[$itemChild['id']] && array_search('delete', $permission[$itemChild['id']]) > -1) ? 'checked' : '' }}
                                                                            id="delete_{{ $itemChild['id'] }}"
                                                                            name="{{ $itemChild['id'] }}[]"
                                                                            value="delete"
                                                                            data-checked-parent="{{ $itemChild['id'] }}">
                                                                        <label class=" form-check-label"
                                                                            for="delete_{{ $itemChild['id'] }}">
                                                                            Delete
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @else
                                                        <div class="list-item-menu ms-3">
                                                            <div class="list-item-menu border p-2 ms-3 mb-1  "
                                                                style="border-left:0.3rem solid #32a1ce !important">
                                                                <div class="d-flex justify-content-between">
                                                                    <div>{{ $itemChild['name'] }}
                                                                        <input class="ms-1 role_menu form-check-input"
                                                                            type="checkbox" value="all"
                                                                            {{ !empty($permission && $permission[$itemChild['id']] && array_search('view', $permission[$itemChild['id']]) > -1) ? 'checked' : '' }}
                                                                            data-item-id="{{ $itemChild['id'] }}"
                                                                            name="{{ $itemChild['id'] }}[]"
                                                                            id="all_{{ $itemChild['id'] }}">
                                                                        <label class=" form-check-label"
                                                                            for="all_{{ $itemChild['id'] }}">
                                                                            All
                                                                        </label>
                                                                    </div>
                                                                    <div class="d-flex flex-row">
                                                                        <!-- item-->
                                                                        <div class="ms-1">
                                                                            <input
                                                                                class="ms-1 role_menu_checked form-check-input"
                                                                                type="checkbox"
                                                                                {{ !empty($permission && $permission[$itemChild['id']] && array_search('all', $permission[$itemChild['id']]) > -1) ? 'checked' : '' }}
                                                                                id="view_{{ $itemChild['id'] }}"
                                                                                name="{{ $itemChild['id'] }}[]"
                                                                                value="view"
                                                                                data-checked-parent="{{ $itemChild['id'] }}">
                                                                            <label class=" form-check-label"
                                                                                for="view_{{ $itemChild['id'] }}">
                                                                                View
                                                                            </label>
                                                                        </div>

                                                                        <div class="ms-1">
                                                                            <input
                                                                                class="ms-1 form-check-input role_menu_checked"
                                                                                type="checkbox" value="add"
                                                                                {{ !empty($permission && $permission[$itemChild['id']] && array_search('add', $permission[$itemChild['id']]) > -1) ? 'checked' : '' }}
                                                                                name="{{ $itemChild['id'] }}[]}"
                                                                                id="add_{{ $itemChild['id'] }}"
                                                                                data-checked-parent="{{ $itemChild['id'] }}">
                                                                            <label class=" form-check-label"
                                                                                for="add_{{ $itemChild['id'] }}">
                                                                                Tambah
                                                                            </label>
                                                                        </div>
                                                                        <div class="ms-1">
                                                                            <input
                                                                                class="ms-1 form-check-input role_menu_checked"
                                                                                type="checkbox"
                                                                                {{ !empty($permission && $permission[$itemChild['id']] && array_search('edit', $permission[$itemChild['id']]) > -1) ? 'checked' : '' }}
                                                                                id="edit_{{ $itemChild['id'] }}"
                                                                                name="{{ $itemChild['id'] }}[]"
                                                                                value="edit"
                                                                                data-checked-parent="{{ $itemChild['id'] }}">
                                                                            <label class=" form-check-label"
                                                                                for="edit_{{ $itemChild['id'] }}">
                                                                                Edit
                                                                            </label>
                                                                        </div>
                                                                        <div class="ms-1">
                                                                            <input
                                                                                class="ms-1 form-check-input role_menu_checked"
                                                                                type="checkbox"
                                                                                {{ !empty($permission && $permission[$itemChild['id']] && array_search('delete', $permission[$itemChild['id']]) > -1) ? 'checked' : '' }}
                                                                                id="delete_{{ $itemChild['id'] }}"
                                                                                name="{{ $itemChild['id'] }}[]"
                                                                                value="delete"
                                                                                data-checked-parent="{{ $itemChild['id'] }}">
                                                                            <label class=" form-check-label"
                                                                                for="delete_{{ $itemChild['id'] }}">
                                                                                Delete
                                                                            </label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="list-item-menu  ms-3  mb-1">
                                                                @foreach ($itemChild['child'] as $itemChild2)
                                                                    @if ($itemChild2['child'] === null)
                                                                        <div class="list-item-menu border p-2 ms-3 mb-1  "
                                                                            style="border-left:0.3rem solid #32a1ce !important">
                                                                            <div class="d-flex justify-content-between">
                                                                                <div>{{ $itemChild2['name'] }}
                                                                                    <input
                                                                                        {{ !empty($permission && $permission[$itemChild2['id']] && array_search('all', $permission[$itemChild2['id']]) > -1) ? 'checked' : '' }}
                                                                                        class="ms-1 role_menu form-check-input"
                                                                                        type="checkbox" value="all"
                                                                                        data-item-id="{{ $itemChild2['id'] }}"
                                                                                        name="{{ $itemChild2['id'] }}[]"
                                                                                        id="all_{{ $itemChild2['id'] }}">
                                                                                    <label class=" form-check-label"
                                                                                        for="all_{{ $itemChild2['id'] }}">
                                                                                        All
                                                                                    </label>
                                                                                </div>
                                                                                <div class="d-flex flex-row">
                                                                                    <!-- item-->
                                                                                    <div class="ms-1">
                                                                                        <input
                                                                                            {{ !empty($permission && $permission[$itemChild2['id']] && array_search('view', $permission[$itemChild2['id']]) > -1) ? 'checked' : '' }}
                                                                                            class="ms-1 role_menu_checked form-check-input"
                                                                                            type="checkbox"
                                                                                            id="view_{{ $itemChild2['id'] }}"
                                                                                            name="{{ $itemChild2['id'] }}"
                                                                                            data-checked-parent="{{ $itemChild2['id'] }}">
                                                                                        <label class=" form-check-label"
                                                                                            for="view_{{ $itemChild2['id'] }}">
                                                                                            View
                                                                                        </label>
                                                                                    </div>

                                                                                    <div class="ms-1">
                                                                                        <input
                                                                                            class="ms-1 form-check-input role_menu_checked"
                                                                                            type="checkbox" value="add"
                                                                                            {{ !empty($permission && $permission[$itemChild2['id']] && array_search('add', $permission[$itemChild2['id']]) > -1) ? 'checked' : '' }}
                                                                                            name="{{ $itemChild2['id'] }}[]"
                                                                                            id="add_{{ $itemChild2['id'] }}"
                                                                                            data-checked-parent="{{ $itemChild2['id'] }}">
                                                                                        <label class=" form-check-label"
                                                                                            for="add_{{ $itemChild2['id'] }}">
                                                                                            Tambah
                                                                                        </label>
                                                                                    </div>
                                                                                    <div class="ms-1">
                                                                                        <input
                                                                                            class="ms-1 form-check-input role_menu_checked"
                                                                                            type="checkbox"
                                                                                            {{ !empty($permission && $permission[$itemChild2['id']] && array_search('edit', $permission[$itemChild2['id']]) > -1) ? 'checked' : '' }}
                                                                                            id="edit_{{ $itemChild2['id'] }}"
                                                                                            name="{{ $itemChild2['id'] }}[]"
                                                                                            value="edit"
                                                                                            data-checked-parent="{{ $itemChild['id'] }}">
                                                                                        <label class=" form-check-label"
                                                                                            for="edit_{{ $itemChild2['id'] }}">
                                                                                            Edit
                                                                                        </label>
                                                                                    </div>
                                                                                    <div class="ms-1">
                                                                                        <input
                                                                                            {{ !empty($permission && $permission[$itemChild2['id']] && array_search('delete', $permission[$itemChild2['id']]) > -1) ? 'checked' : '' }}
                                                                                            class="ms-1 form-check-input role_menu_checked"
                                                                                            type="checkbox"
                                                                                            id="delete_{{ $itemChild2['id'] }}"
                                                                                            name="{{ $itemChild2['id'] }}[]"
                                                                                            value="delete"
                                                                                            data-checked-parent="{{ $itemChild2['id'] }}">
                                                                                        <label class=" form-check-label"
                                                                                            for="delete_{{ $itemChild2['id'] }}">
                                                                                            Delete
                                                                                        </label>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    @endif
                                                                @endforeach
                                                            </div>
                                                        </div>
                                                    @endif
                                                @endforeach
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                            @endif
                        </div>
                        <button class="btn btn-primary mt-3" type="submit">Simpan</button>
                    </form>
                </div>
                <!-- end card body -->
            </div>
        </div>
    </div>
    <!-- end col -->
    <!-- end row -->
@endsection
@section('script')
    <script src="{{ URL::asset('assets/libs/dropzone/dropzone.min.js') }}"></script>
    <script src="{{ URL::asset('assets/js/pages/form-validation.init.js') }}"></script>
    <script src="{{ URL::asset('js/pages/administrator/role/privilage.js') }}"></script>
    <script src="{{ URL::asset('/assets/js/app.min.js') }}"></script>
@endsection
