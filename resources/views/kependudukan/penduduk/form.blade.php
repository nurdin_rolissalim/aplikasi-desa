@extends('layouts.admin.master')
@section('title')
    Kependudukan - Form Penduduk
@endsection
@section('css')
    <link href="{{ URL::asset('assets/libs/dropzone/dropzone.min.css') }}" rel="stylesheet">
@endsection
@section('content')
    @component('components.breadcrumb')
        @slot('li_1')
            <a href="{{ route('penduduk.index') }}"> Penduduk</a>
        @endslot
        @slot('title')
            Form Penduduk
        @endslot
    @endcomponent

    <div class="row">
        <div class="col-md-4">
            <div class="card">
                <div class="card-body">
                    <div>
                        <form action="#" class="dropzone">
                            @csrf
                            <div class="fallback">
                                <input name="file" type="file" multiple="multiple">
                            </div>
                            <div class="dz-message needsclick">
                                <div class="mb-3">
                                    <i class="display-4 text-muted bx bx-cloud-upload"></i>
                                </div>

                                <h5>Drop files here or click to upload.</h5>
                            </div>
                        </form>
                    </div>

                    <div class="text-center mt-4">
                        <button type="button" class="btn btn-primary waves-effect waves-light">Send
                            Files</button>
                    </div>
                </div>
                <!-- end card body -->
            </div>
        </div>
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <div>
                        <a href="{{ route('penduduk.index') }}"
                            class="btn btn-primary waves-effect btn-label waves-light">
                            <i class="bx bx-plus label-icon"></i> Kembali ke Daftar Penduduk
                        </a>
                    </div>
                </div>
                <div class="card-body">
                    <form class="needs-validation" novalidate>
                        <div class="row mb-3">
                            <div class="col-sm-6">
                                <label for="date_of_birth" class="form-label">Tanggal Pindah Masuk</label>
                                <input class="form-control" type="date" value="" id="date_of_birth" name="date_of_birth">
                            </div>
                            <div class="col-sm-6">
                                <label for="date_of_birth" class="form-label">Tanggal Lapor</label>
                                <input class="form-control" type="date" value="" id="date_of_birth" name="date_of_birth">
                            </div>
                        </div>


                        <div class="p-2 mb-2 bg-primary text-white">Data Diri</div>
                        <div class="row ">
                            <div class="col-sm-4 mb-3">
                                <label for="nik" class="form-label">NIK</label>
                                <input type="text" class="form-control" placeholder="NIK" id="nik" name="nik" required>
                                <div class="invalid-feedback">
                                    NIK harus diisi
                                </div>
                            </div>
                            <div class="col-sm-8 mb-3">
                                <label for="nama" class="form-label">Nama Lengkap</label>
                                <input type="text" class="form-control" placeholder="Nama Pegawai" id="nama" name="nama"
                                    required>
                                <div class="invalid-feedback">
                                    Nama Lengkap harus diisi
                                </div>
                            </div>
                        </div>
                        <div class="table-rep-plugin">
                            <div class="table-responsive mb-0" data-pattern="priority-columns">
                                <table id="tech-companies-1" class="table">
                                    <thead>
                                        <tr class="table-light">
                                            <th>Wajib Identitas</th>
                                            <th data-priority="1">Identitas Elektronik</th>
                                            <th data-priority="3">Status Rekam</th>
                                            <th data-priority="1">Tag ID Card</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <th></th>
                                            <td>
                                                <select class="form-select" id="identitas_elektronik"
                                                    name="identitas_elektronik">
                                                    <option>Pilih Identitas-EL</option>
                                                    <option value="Belum">Belum</option>
                                                    <option value="KTP-EL">KTP-EL</option>
                                                    <option value="KIA">KIA</option>
                                                </select>
                                            </td>
                                            <td>
                                                <select class="form-select" id="status_rekam" name="status_rekam">
                                                    <option>Pilih Status Rekam</option>
                                                    <option value="Belum Rekam">Belum Rekam</option>
                                                    <option value="Sudah Rekam">Sudah Rekam</option>
                                                    <option value="Card Printed">Card Printed</option>
                                                    <option value="Print Ready Record">Print Ready Record</option>
                                                    <option value="Card Shipped">Card Shipped</option>
                                                    <option value="Sent for Card Printing">Sent for Card Printing</option>
                                                    <option value="Card Issue">Card Issue</option>
                                                </select>
                                            </td>
                                            <td>
                                                <input type="text" class="form-control" placeholder="Tag ID Card"
                                                    id="tag_id_card" name="tag_id_card">
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>

                        </div>



                        <div class="row mb-3">
                            <div class="col-sm-4">
                                <label for="tempat_penerbit" class="form-label">Tempat Penerbitan KTP</label>
                                <input type="text" class="form-control" placeholder="Tempat Penerbitan KTP"
                                    id="tempat_penerbit" nama="tempat_penerbit">
                            </div>
                            <div class="col-sm-4">
                                <label for="tgl_penerbit" class="form-label">Tanggal Penerbitan KTP</label>
                                <input class="form-control" type="date" value="" id="tgl_penerbit" name="tgl_penerbit">
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-sm-4">
                                <label for="no_kk" class="form-label">No KK</label>
                                <input type="text" class="form-control" placeholder="No KK" id="no_kk" nama="no_kk">
                            </div>
                            <div class="col-sm-5">
                                <label for="relation" class="form-label">Hubungan Dengan Keluarga</label>
                                <select class="form-select" id="relation" name="relation">
                                    <option>Pilih Hubungan Dengan Keluarga</option>
                                    <option value="Kepala Rumah Tangga">Kepala Rumah Tangga</option>
                                    <option value="Suami">Suami</option>
                                    <option value="Istri">Istri</option>
                                    <option value="Anak">Anak</option>
                                    <option value="Menantu">Menantu</option>
                                    <option value="Cucu">Cucu</option>
                                    <option value="Orang Tua">Orang Tua</option>
                                    <option value="Mertua">Mertua</option>
                                    <option value="Famili Lain">Famili Lain</option>
                                    <option value="Pembantu">Pembantu</option>
                                    <option value="Lainnya">Lainnya</option>
                                </select>
                            </div>
                            <div class="col-sm-3">
                                <label for="jenis_kelamin" class="form-label">Jenis Kelamin</label>
                                <select class="form-select" id="jenis_kelamin" name="jenis_kelamin">
                                    <option>Pilih Jenis Kelamin</option>
                                    <option value="Laki-Laki">Laki-Laki</option>
                                    <option value="Perempuan">Perempuan</option>
                                </select>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-sm-6">
                                <label for="agama" class="form-label">Agama</label>
                                <select class="form-select" id="agama" name="agama">
                                    <option>Pilih Agama</option>
                                    <option value="Islam">Islam</option>
                                    <option value="Kristen">Kristen</option>
                                    <option value="Khatolik">Khatolik</option>
                                    <option value="Hindu">Hindu</option>
                                    <option value="Budha">Budha</option>
                                    <option value="Khonghucu">Khonghucu</option>
                                </select>
                            </div>
                            <div class="col-sm-6">
                                <label for="status_penduduk" class="form-label">Status Penduduk</label>
                                <select class="form-select" id="status_penduduk" name="status_penduduk">
                                    <option>Pilih Status Penduduk</option>
                                    <option value="Tetap">Tetap</option>
                                    <option value="Tidak Tetap">Tidak Tetap</option>
                                </select>
                            </div>
                        </div>
                        <div class="p-2 mb-2 bg-primary text-white">Data Kelahiran</div>
                        <div class="row ">
                            <div class="col-sm-4 mb-3">
                                <label for="no_akta" class="form-label">No Akta Kelahiran</label>
                                <input type="text" class="form-control" placeholder="No Akta Kelahiran" id="nino_aktak"
                                    name="no_akta" required>
                                <div class="invalid-feedback">
                                    No Akta Kelahiran harus diisi
                                </div>
                            </div>
                            <div class="col-sm-8 mb-3">
                                <label for="tempat_kelahiran" class="form-label">Tempat Kelahiran</label>
                                <input type="text" class="form-control" placeholder="Tempat Kelahiran"
                                    id="tempat_kelahiran" name="tempat_kelahiran" required>
                                <div class="invalid-feedback">
                                    Tempat Kelahiran harus diisi
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-sm-4">
                                <label for="tgl_lahir" class="form-label">Tanggal Lahir</label>
                                <input type="text" class="form-control" type="date" value="" id="tgl_lahir"
                                    nama="tgl_lahir">
                            </div>
                            <div class="col-sm-4">
                                <label for="waktu_lahir" class="form-label">Waktu Lahir</label>
                                <input class="form-control" type="date" value="" id="waktu_lahir" name="waktu_lahir">
                            </div>
                            <div class="col-sm-4">
                                <label for="tempat_dilahirkan" class="form-label">Tempat Dilahirkan</label>
                                <select class="form-select" id="tempat_dilahirkan" name="tempat_dilahirkan">
                                    <option>Pilih Tempat Dilahirkan</option>
                                    <option value="RS/RB">RS/RB</option>
                                    <option value="Puskesmas">Puskesmas</option>
                                    <option value="Polindes">Polindes</option>
                                    <option value="Rumah">Rumah</option>
                                    <option value="Lainnya">Lainnya</option>
                                </select>
                            </div>
                        </div>

                        <div class="row mb-3">
                            <div class="col-sm-4">
                                <label for="jenis_kelahiran" class="form-label">Jenis Kelahiran</label>
                                <select class="form-select" id="jenis_kelahiran" name="jenis_kelahiran">
                                    <option>Pilih Jenis Kelahiran</option>
                                    <option value="Tunggal">Tunggal</option>
                                    <option value="Kembar 1">Kembar 1</option>
                                    <option value="Kembar 2">Kembar 2</option>
                                    <option value="Kembar 3">Kembar 3</option>
                                    <option value="Kembar 4">Kembar 4</option>
                                </select>
                            </div>
                            <div class="col-sm-4">
                                <label for="waktu_lahir" class="form-label">Anak Ke (Isi Dengan Angka)</label>
                                <input class="form-control" type="number" value="" id="waktu_lahir" name="waktu_lahir">
                            </div>
                            <div class="col-sm-4">
                                <label for="penolong_kelahiran" class="form-label">Penolong Kelahiran</label>
                                <select class="form-select" id="penolong_kelahiran" name="penolong_kelahiran">
                                    <option>Pilih Penolong Kelahiran</option>
                                    <option value="Dokter">Dokter</option>
                                    <option value="Bidan">Bidan</option>
                                    <option value="Dukun">Dukun</option>
                                    <option value="Lainnya">Lainnya</option>
                                </select>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-sm-4">
                                <label for="berat_lahir" class="form-label">Berat Lahir (gram)</label>
                                <input type="text" class="form-control" type="number" value="" id="berat_lahir"
                                    nama="berat_lahir">
                            </div>
                            <div class="col-sm-4">
                                <label for="panjang_lahir" class="form-label">Panjang Lahir (cm)</label>
                                <input class="form-control" type="number" value="" id="panjang_lahir"
                                    name="panjang_lahir">
                            </div>
                        </div>
                        <div class="p-2 mb-2 bg-primary text-white">Pendidikan dan Pekerjaan</div>
                        <div class="row mb-3">
                            <div class="col-sm-4">
                                <label for="pendidikan" class="form-label">Pendidikan</label>
                                <select class="form-select" id="pendidikan" name="pendidikan">
                                    <option>Pilih Pendidikan</option>
                                    <option value="Tidak/Belum Sekolah">Tidak/Belum Sekolah</option>
                                    <option value="Belum Tamat SD/Sederajat">Belum Tamat SD/Sederajat</option>
                                    <option value="Belum Tamata SLTP/Sederajat">Belum Tamat SLTP/Sederajat</option>
                                    <option value="Belum Tamata SLTA/Sederajat">Belum Tamat SLTA/Sederajat</option>
                                    <option value="Diploma I/II">Diploma I/II</option>
                                    <option value="Akademi/Diploma III/S.Muda">Akademi/Diploma III/S.Muda</option>
                                    <option value="Diploma IV/Strata I">Diploma IV/Strata I</option>
                                    <option value="Strata II">Strata II</option>
                                    <option value="Strata III">Strata III</option>
                                </select>
                            </div>
                            <div class="col-sm-4">
                                <label for="pendidikan_sedang_ditempuh" class="form-label">Pendidikan Sedang
                                    Ditempuh</label>
                                <select class="form-select" id="pendidikan_sedang_ditempuh"
                                    name="pendidikan_sedang_ditempuh">
                                    <option>Pilih Pendidikan Sedang Ditempuh</option>
                                    <option value="Belum Masuk TK/Kelompok Bermain">Belum Masuk TK/Kelompok Bermain</option>
                                    <option value="Sedang TK/Kelompok Bermain">Sedang TK/Kelompok Bermain</option>
                                    <option value="Tidak Pernah Sekolah">Tidak Pernah Sekolah</option>
                                    <option value="Sedang SD/Sederajat">Sedang SD/Sederajat</option>
                                    <option value="Sedang SLTP">Sedang SLTP/Sederajat</option>
                                    <option value="Sedang SLTA">Sedang SLTA/Sederajat</option>
                                    <option value="Sedang D1/Sederajat">Diploma I/Sederajat</option>
                                    <option value="Sedang D2/Sederajat">Diploma II/Sederajat</option>
                                    <option value="Sedang D3/Sederajat">Diploma III/Sederajat</option>
                                    <option value="Sedang D4/Sederajat">Diploma IV/Sederajat</option>
                                    <option value="Sedang Strata I/Sederajat">Sedang Strata I/Sederajat</option>
                                    <option value="Sedang Strata II/Sederajat">Sedang Strata II/Sederajat</option>
                                    <option value="Sedang Strata III/Sederajat">Sedang Strata III/Sederajat</option>
                                    <option value="Sedang SLB A/Sederajat">Sedang SLB A/Sederajat</option>
                                    <option value="Sedang SLB B/Sederajat">Sedang SLB B/Sederajat</option>
                                    <option value="Sedang SLB C/Sederajat">Sedang SLB C/Sederajat</option>
                                    <option value="Tidak Bisa Membaca/Menulis Bahasa Latin/Arab">Tidak Bisa Membaca/Menulis
                                        Bahasa Latin/Arab</option>
                                    <option value="Tidak Sedang Sekolah">Tidak Sedang Sekolah</option>

                                </select>
                            </div>
                            <div class="col-sm-4">
                                <label for="pekerjaan" class="form-label">Pekerjaan</label>
                                <select class="form-select" id="pekerjaan" name="pekerjaan">
                                    <option>Pilih Pekerjaan</option>
                                    <option value="Belum/Tidak Bekerja">Belum/Tidak Bekerja</option>
                                    <option value="Mengurus Rumah Tangga">Mengurus Rumah Tangga</option>
                                    <option value="Pelajar/Mahasiswa">Pelajar/Mahasiswa</option>
                                    <option value="Pensiunan">Pensiunan</option>
                                    <option value="PEGAWAI NEGERI SIPIL (PNS)">PEGAWAI NEGERI SIPIL (PNS)</option>
                                    <option value="TENTARA NASIONAL INDONESIA (TNI)">TENTARA NASIONAL INDONESIA (TNI)
                                    </option>
                                    <option value="KEPOLISIAN RI (POLRI)">KEPOLISIAN RI (POLRI)</option>
                                    <option value="PERDAGANGAN">PERDAGANGAN</option>
                                    <option value="PETANI/PEKEBUN">PETANI/PEKEBUN</option>
                                    <option value="PETERNAK">PETERNAK</option>
                                    <option value="NELAYAN/PERIKANAN">NELAYAN/PERIKANAN</option>
                                    <option value="INDUSTRI">INDUSTRI</option>
                                    <option value="KONSTRUKSI">KONSTRUKSI</option>
                                    <option value="TRANSPORTASI">TRANSPORTASI</option>
                                    <option value="KARYAWAN SWASTA">KARYAWAN SWASTA</option>
                                    <option value="KARYAWAN BUMN">KARYAWAN BUMN</option>
                                    <option value="KARYAWAN BUMD">KARYAWAN BUMD</option>
                                    <option value="KARYAWAN HONORER">KARYAWAN HONORER</option>
                                    <option value="BURUH HARIAN LEPAS">BURUH HARIAN LEPAS</option>
                                    <option value="BURUH TANI/PERKEBUNAN">BURUH TANI/PERKEBUNAN</option>
                                    <option value="BURUH NELAYAN/PERIKANAN">BURUH NELAYAN/PERIKANAN</option>
                                    <option value="BURUH PETERNAKAN">BURUH PETERNAKAN</option>
                                    <option value="PEMBANTU RUMAH TANGGA">PEMBANTU RUMAH TANGGA</option>
                                    <option value="TUKANG CUKUR">TUKANG CUKUR</option>
                                    <option value="TUKANG LISTRIK">TUKANG LISTRIK</option>
                                    <option value="TUKANG BATU">TUKANG BATU</option>
                                    <option value="TUKANG KAYU">TUKANG KAYU</option>
                                    <option value="28">TUKANG SOL SEPATU</option>
                                    <option value="29">TUKANG LAS/PANDAI BESI</option>
                                    <option value="30">TUKANG JAHIT</option>
                                    <option value="31">TUKANG GIGI</option>
                                    <option value="32">PENATA RIAS</option>
                                    <option value="33">PENATA BUSANA</option>
                                    <option value="34">PENATA RAMBUT</option>
                                    <option value="35">MEKANIK</option>
                                    <option value="36">SENIMAN</option>
                                    <option value="37">TABIB</option>
                                    <option value="38">PARAJI</option>
                                    <option value="39">PERANCANG BUSANA</option>
                                    <option value="40">PENTERJEMAH</option>
                                    <option value="41">IMAM MASJID</option>
                                    <option value="42">PENDETA</option>
                                    <option value="43">PASTOR</option>
                                    <option value="44">WARTAWAN</option>
                                    <option value="45">USTADZ/MUBALIGH</option>
                                    <option value="46">JURU MASAK</option>
                                    <option value="47">PROMOTOR ACARA</option>
                                    <option value="48">ANGGOTA DPR-RI</option>
                                    <option value="49">ANGGOTA DPD</option>
                                    <option value="50">ANGGOTA BPK</option>
                                    <option value="51">PRESIDEN</option>
                                    <option value="52">WAKIL PRESIDEN</option>
                                    <option value="53">ANGGOTA MAHKAMAH KONSTITUSI</option>
                                    <option value="54">ANGGOTA KABINET KEMENTERIAN</option>
                                    <option value="55">DUTA BESAR</option>
                                    <option value="56">GUBERNUR</option>
                                    <option value="57">WAKIL GUBERNUR</option>
                                    <option value="58">BUPATI</option>
                                    <option value="59">WAKIL BUPATI</option>
                                    <option value="60">WALIKOTA</option>
                                    <option value="61">WAKIL WALIKOTA</option>
                                    <option value="62">ANGGOTA DPRD PROVINSI</option>
                                    <option value="63">ANGGOTA DPRD KABUPATEN/KOTA</option>
                                    <option value="64">DOSEN</option>
                                    <option value="65">GURU</option>
                                    <option value="66">PILOT</option>
                                    <option value="67">PENGACARA</option>
                                    <option value="68">NOTARIS</option>
                                    <option value="69">ARSITEK</option>
                                    <option value="70">AKUNTAN</option>
                                    <option value="71">KONSULTAN</option>
                                    <option value="72">DOKTER</option>
                                    <option value="73">BIDAN</option>
                                    <option value="74">PERAWAT</option>
                                    <option value="75">APOTEKER</option>
                                    <option value="76">PSIKIATER/PSIKOLOG</option>
                                    <option value="77">PENYIAR TELEVISI</option>
                                    <option value="78">PENYIAR RADIO</option>
                                    <option value="79">PELAUT</option>
                                    <option value="80">PENELITI</option>
                                    <option value="81">SOPIR</option>
                                    <option value="82">PIALANG</option>
                                    <option value="83">PARANORMAL</option>
                                    <option value="84">PEDAGANG</option>
                                    <option value="85">PERANGKAT DESA</option>
                                    <option value="86">KEPALA DESA</option>
                                    <option value="87">BIARAWATI</option>
                                    <option value="88">WIRASWASTA</option>
                                    <option value="89">LAINNYA</option>
                                </select>
                            </div>

                        </div>

                        <div class="p-2 mb-2 bg-primary text-white">Data Kewarganegaraan</div>
                        <div class="row mb-3">
                            <div class="col-sm-12">
                                <label for="suku" class="form-label">Suku/Etnis</label>
                                <select class="form-select" id="suku" name="suku">
                                    <option>Pilih Suku/Etnis</option>
                                    <option value="Sunda">Sunda</option>
                                    <option value="Jawa">Jawa</option>
                                </select>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-sm-4">
                                <label for="status_warga_negara" class="form-label">Status Warga Negara</label>
                                <select class="form-select" id="status_warga_negara" name="status_warga_negara">
                                    <option>Pilih Status Warga Negara</option>
                                    <option value="">Pilih Warga Negara</option>
                                    <option value="1">WNI</option>
                                    <option value="2">WNA</option>
                                    <option value="3">DUA KEWARGANEGARAAN</option>
                                </select>
                            </div>
                            <div class="col-sm-8">
                                <label for="nomor_passport" class="form-label">Nomor Paspor</label>
                                <input class="form-control" type="number" value="" id="nomor_passport"
                                    name="nomor_passport">
                            </div>

                        </div>

                        <div class="row mb-3">
                            <div class="col-sm-4">
                                <label for="tgl_passport" class="form-label">Tanggal Berakhir Paspor</label>
                                <input class="form-control" type="date" value="" id="tgl_passport" name="tgl_passport">
                            </div>
                            <div class="col-sm-8">
                                <label for="nomor_kitas" class="form-label">Nomor KITAS/KITAP</label>
                                <input class="form-control" type="string" value="" placeholder="Nomor KITAS/KITAP"
                                    id="nomor_kitas" name="nomor_kitas">
                            </div>

                        </div>
                        <div class="row mb-3">
                            <div class="col-sm-4">
                                <label for="negara_asal" class="form-label">Negara Asal</label>
                                <input class="form-control" type="string" value="" id="negara_asal" name="negara_asal">
                            </div>

                        </div>
                        <div class="p-2 mb-2 bg-primary text-white">Data Orang Tua</div>
                        <div class="row mb-3">
                            <div class="col-sm-4">
                                <label for="nik_ayah" class="form-label">NIK Ayah</label>
                                <input class="form-control" type="string" value="" id="nik_ayah" name="nik_ayah">
                            </div>
                            <div class="col-sm-8">
                                <label for="nama_ayah" class="form-label">Nama Ayah</label>
                                <input class="form-control" type="string" value="" placeholder="Nama Ayah" id="nama_ayah"
                                    name="nama_ayah">
                            </div>

                        </div>
                        <div class="row mb-3">
                            <div class="col-sm-4">
                                <label for="nik_ibu" class="form-label">NIK Ibu</label>
                                <input class="form-control" type="string" value="" id="nik_ibu" name="nik_ibu">
                            </div>
                            <div class="col-sm-8">
                                <label for="nama_ibu" class="form-label">Nama Ibu</label>
                                <input class="form-control" type="string" value="" placeholder="Nama Ibu" id="nama_ibu"
                                    name="nama_ibu">
                            </div>

                        </div>
                        <div class="p-2 mb-2 bg-primary text-white">Alamat</div>
                        <div class="row mb-3">
                            <div class="col-sm-4">
                                <label for="dusun" class="form-label">Dusun</label>
                                <input class="form-control" type="string" value="" id="dusun" name="dusun">
                            </div>
                            <div class="col-sm-4">
                                <label for="rt" class="form-label">RT</label>
                                <input class="form-control" type="string" value="" placeholder="RT" id="rt" name="rt">
                            </div>
                            <div class="col-sm-4">
                                <label for="rw" class="form-label">RW</label>
                                <input class="form-control" type="string" value="" placeholder="RW" id="rw" name="rw">
                            </div>

                        </div>
                        <div class="row mb-3">
                            <div class="col-sm-12 mb-3">
                                <label for="telepon" class="form-label">No Telepon</label>
                                <input class="form-control" type="string" value="" placeholder="No Telepon" id="telepon"
                                    name="telepon">
                            </div>

                            <div class="col-sm-12 mb-3">
                                <label for="email" class="form-label">Email</label>
                                <input class="form-control" type="email" value="" placeholder="Email" id="email"
                                    name="email">
                            </div>
                            <div class="col-sm-12 mb-3">
                                <label for="alamat_sebelumnya" class="form-label">Alamat Sebelumnya</label>
                                <input class="form-control" type="string" value="" placeholder="Alamat Sebelumnya"
                                    id="alamat_sebelumnya" name="alamat_sebelumnya">
                            </div>

                            <div class="col-sm-12 mb-3">
                                <label for="alamat_sekarang" class="form-label">Alamat Sekarang</label>
                                <input class="form-control" type="string" placeholder="Alamat Sekarang" value=""
                                    id="alamat_sekarang" name="alamat_sekarang">
                            </div>
                        </div>
                        <div class="p-2 mb-2 bg-primary text-white">Status Perkawinan</div>
                        <div class="row mb-3">
                            <div class="col-sm-4 mb-3">
                                <label for="status_perkawinan" class="form-label">Status Perkawinan</label>
                                <select class="form-select" id="status_perkawinan" name="status_perkawinan">
                                    <option value="">Pilih Status Perkawinan</option>
                                    <option value="1">BELUM KAWIN</option>
                                    <option value="2">KAWIN</option>
                                    <option value="3">CERAI HIDUP</option>
                                    <option value="4">CERAI MATI</option>
                                </select>
                            </div>

                            <div class="col-sm-4 mb-3">
                                <label for="akta_nikah" class="form-label">No Akta Nikah</label>
                                <input class="form-control" type="string" value="" placeholder="No Akta Nikah"
                                    id="akta_nikah" name="akta_nikah">
                            </div>
                            <div class="col-sm-4 mb-3">
                                <label for="tgl_nikah" class="form-label">Tanggal Nikah</label>
                                <input class="form-control" type="date" value="" id="tgl_nikah" name="tgl_nikah">
                            </div>

                            <div class="col-sm-8 mb-3">
                                <label for="akta_cerai" class="form-label">Akta Cerai</label>
                                <input class="form-control" type="string" placeholder="Akta Cerai" value=""
                                    id="akta_cerai" name="akta_cerai">
                            </div>
                            <div class="col-sm-4 mb-3">
                                <label for="tgl_cerai" class="form-label">Tanggal Cerai</label>
                                <input class="form-control" type="date" value="" id="tgl_cerai" name="tgl_cerai">
                            </div>
                        </div>
                        <div class="p-2 mb-2 bg-primary text-white">Status Kesehatan</div>
                        <div class="row mb-3">
                            <div class="col-sm-4 mb-3">
                                <label for="golongan_darah" class="form-label">Golongan Darah</label>
                                <select class="form-select" id="golongan_darah" name="golongan_darah">
                                    <option value="">Pilih Golongan Darah</option>
                                    <option value="1">A</option>
                                    <option value="2">B</option>
                                    <option value="3">AB</option>
                                    <option value="4">O</option>
                                    <option value="5">A+</option>
                                    <option value="6">A-</option>
                                    <option value="7">B+</option>
                                    <option value="8">B-</option>
                                    <option value="9">AB+</option>
                                    <option value="10">AB-</option>
                                    <option value="11">O+</option>
                                    <option value="12">O-</option>
                                    <option value="13">TIDAK TAHU</option>
                                </select>
                            </div>

                            <div class="col-sm-4 mb-3">
                                <label for="email" class="form-label">Cacat </label>
                                <select class="form-select" name="cacat_id">
                                    <option value="">Pilih Jenis Cacat</option>
                                    <option value="1">CACAT FISIK</option>
                                    <option value="2">CACAT NETRA/BUTA</option>
                                    <option value="3">CACAT RUNGU/WICARA</option>
                                    <option value="4">CACAT MENTAL/JIWA</option>
                                    <option value="5">CACAT FISIK DAN MENTAL</option>
                                    <option value="6">CACAT LAINNYA</option>
                                    <option value="7">TIDAK CACAT</option>
                                </select>
                            </div>
                            <div class="col-sm-4 mb-3">
                                <label for="sakit_menahun_id" class="form-label">Sakit Menahun</label>
                                <select class="form-select " name="sakit_menahun_id">
                                    <option value="">Pilih Sakit Menahun</option>
                                    <option value="1">JANTUNG</option>
                                    <option value="2">LEVER</option>
                                    <option value="3">PARU-PARU</option>
                                    <option value="4">KANKER</option>
                                    <option value="5">STROKE</option>
                                    <option value="6">DIABETES MELITUS</option>
                                    <option value="7">GINJAL</option>
                                    <option value="8">MALARIA</option>
                                    <option value="9">LEPRA/KUSTA</option>
                                    <option value="10">HIV/AIDS</option>
                                    <option value="11">GILA/STRESS</option>
                                    <option value="12">TBC</option>
                                    <option value="13">ASTHMA</option>
                                    <option value="14">TIDAK ADA/TIDAK SAKIT</option>
                                </select>
                            </div>

                            <div class="col-sm-12 mb-3">
                                <label for="alamat_sekarang" class="form-label">Alamat Sekarang</label>
                                <input class="form-control" type="string" placeholder="Alamat Sekarang" value=""
                                    id="alamat_sekarang" name="alamat_sekarang">
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-sm-4 mb-3">
                                <label for="golongan_darah" class="form-label">Apsektor KB</label>
                                <select class="form-select" name="cara_kb_id">
                                    <option value="">Pilih Cara KB Saat Ini</option>
                                    <option value="1">PIL</option>
                                    <option value="2">IUD</option>
                                    <option value="3">SUNTIK</option>
                                    <option value="4">KONDOM</option>
                                    <option value="5">SUSUK KB</option>
                                    <option value="6">STERILISASI WANITA</option>
                                    <option value="7">STERILISASI PRIA</option>
                                    <option value="99">LAINNYA</option>
                                </select>
                            </div>

                            <div class="col-sm-4 mb-3">
                                <label for="email" class="form-label">Cacat </label>
                                <select class="form-select input-sm" name="id_asuransi">
                                    <option value="">Pilih Asuransi</option>
                                    <option value="1">TIDAK/BELUM PUNYA</option>
                                    <option value="2">BPJS PENERIMA BANTUAN IURAN</option>
                                    <option value="3">BPJS NON PENERIMA BANTUAN IURAN</option>
                                    <option value="99">ASURANSI LAINNYA</option>
                                </select>
                            </div>

                            <div class="col-sm-12 mb-3">
                                <label for="nomor_bpjs" class="form-label">Nomor BPJS</label>
                                <input class="form-control" type="string" placeholder="No BPJS" value="" id="nomor_bpjs"
                                    name="nomor_bpjs">
                            </div>
                        </div>
                        <div class="p-2 mb-2 bg-primary text-white">Lainnya</div>
                        <div class="row mb-3">
                            <div class="col-sm-4 mb-3">
                                <label for="telepon" class="form-label">Dapat Membaca</label>
                                <select class="form-select" id="bahasa_id" name="bahasa_id">
                                    <option value="0">Pilih Isian</option>
                                    <option value="1">LATIN</option>
                                    <option value="2">DAERAH</option>
                                    <option value="3">ARAB</option>
                                    <option value="4">ARAB DAN LATIN</option>
                                    <option value="5">ARAB DAN DAERAH</option>
                                    <option value="6">ARAB, LATIN DAN DAERAH</option>
                                </select>
                            </div>

                            <div class="col-sm-8 mb-3">
                                <label for="ket" class="form-label">Keterangan</label>
                                <textarea name="ket" id="ket" class="form-control"></textarea>
                            </div>
                        </div>
                        <button class="btn btn-primary" type="submit">Simpan</button>
                    </form>
                </div>
                <!-- end card body -->
            </div>
        </div>
    </div>
    <!-- end col -->
    <!-- end row -->
@endsection
@section('script')
    <script src="{{ URL::asset('assets/libs/dropzone/dropzone.min.js') }}"></script>
    <script src="{{ URL::asset('assets/js/pages/form-validation.init.js') }}"></script>
    <script src="{{ URL::asset('/assets/js/app.min.js') }}"></script>
@endsection
