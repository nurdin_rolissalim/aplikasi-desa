@extends('layouts.admin.master')
@section('title')
    Kependudukan - Form Penduduk
@endsection
@section('css')
    <link href="{{ URL::asset('assets/libs/dropzone/dropzone.min.css') }}" rel="stylesheet">
@endsection
@section('content')
    @component('components.breadcrumb')
        @slot('li_1')
            <a href="{{ route('penduduk.index') }}"> Penduduk</a>
        @endslot
        @slot('title')
            Form Penduduk
        @endslot
    @endcomponent

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div>
                        <a href="{{ route('daftarPersyaratan.index') }}"
                            class="btn btn-primary waves-effect btn-label waves-light">
                            <i class="bx bx-plus label-icon"></i> Kembali ke Daftar Persyaratan
                        </a>
                    </div>
                </div>
                <div class="card-body">
                    <form class="needs-validation" novalidate>
                        <div class="row mb-3">
                            <label for="nama_dokumen" class="col-sm-3 col-form-label">Nama Dokumen</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" placeholder="Nama Dokumen" id="nama_dokumen"
                                    name="nama_dokumen" required>
                                <div class="invalid-feedback">
                                    Nama Dokumen harus diisi
                                </div>
                                <button class="btn btn-primary mt-3" type="submit">Simpan</button>
                            </div>
                        </div>                       
                    </form>
                </div>
                <!-- end card body -->
            </div>
        </div>
    </div>
    <!-- end col -->
    <!-- end row -->
@endsection
@section('script')
    <script src="{{ URL::asset('assets/libs/dropzone/dropzone.min.js') }}"></script>
    <script src="{{ URL::asset('assets/js/pages/form-validation.init.js') }}"></script>
    <script src="{{ URL::asset('/assets/js/app.min.js') }}"></script>
@endsection
