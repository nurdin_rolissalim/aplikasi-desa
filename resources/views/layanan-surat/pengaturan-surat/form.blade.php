@extends('layouts.admin.master')
@section('title')
    Kependudukan - Form Penduduk
@endsection
@section('css')
    <link href="{{ URL::asset('assets/libs/dropzone/dropzone.min.css') }}" rel="stylesheet">
@endsection
@section('content')
    @component('components.breadcrumb')
        @slot('li_1')
            <a href="{{ route('penduduk.index') }}"> Penduduk</a>
        @endslot
        @slot('title')
            Form Penduduk
        @endslot
    @endcomponent

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div>
                        <a href="{{ route('pengaturanSurat.index') }}"
                            class="btn btn-primary waves-effect btn-label waves-light">
                            <i class="bx bx-plus label-icon"></i> Kembali ke Daftar Format Surat
                        </a>
                    </div>
                </div>
                <div class="card-body">
                    <form class="needs-validation" novalidate>
                        <div class="row mb-3">
                            <label for="kode_desa" class="col-sm-3 col-form-label">Kode/Klasifikasi Surat</label>
                            <div class="col-sm-9">
                                <select class="form-select" id="identitas_elektronik" name="identitas_elektronik">
                                    <option>Pilih Kode/Klasifikasi Surat</option>
                                    <option value="Belum">Belum</option>
                                    <option value="KTP-EL">KTP-EL</option>
                                    <option value="KIA">KIA</option>
                                </select>
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="kode_desa" class="col-sm-3 col-form-label">Layanan Surat</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" placeholder="Layanan Surat" id="no_kk"
                                    nama="no_kk">
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="kode_desa" class="col-sm-3 col-form-label">Pemohon Surat</label>
                            <div class="col-sm-9">
                                <div class="form-check mb-3">
                                    <input class="form-check-input" type="radio" name="formRadios" id="formRadios1" checked>
                                    <label class="form-check-label" for="formRadios1">
                                        Warga
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="formRadios" id="formRadios2">
                                    <label class="form-check-label" for="formRadios2">
                                        Bukan Warga
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="row mb-2">
                            <label for="kode_desa" class="col-sm-3 col-form-label">Masa Berlaku</label>
                            <div class="col-sm-1">
                                <input type="text" class="form-control" placeholder="Masa Berlaku" value="1" id="no_kk"
                                    nama="no_kk">
                            </div>
                            <div class="col-sm-3">

                                <select class="form-select" id="identitas_elektronik" name="identitas_elektronik">
                                    <option value="d">Hari</option>
                                    <option value="w">Minggu</option>
                                    <option value="M">Bulan</option>
                                    <option value="y">Tahun</option>
                                </select>
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="kode_desa" class="col-sm-3 col-form-label">Sediakan di Layanan Mandiri</label>
                            <div class="col-sm-9">
                                <div class="form-check mb-3">
                                    <input class="form-check-input" type="radio" name="layanan_mandiri" id="layanan_mandiri"
                                        checked>
                                    <label class="form-check-label" for="layanan_mandiri">
                                        Tidak
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="layanan_mandiri"
                                        id="layanan_mandiri">
                                    <label class="form-check-label" for="layanan_mandiri">
                                        Ya
                                    </label>
                                </div>
                            </div>
                        </div>
                        <button class="btn btn-primary" type="submit">Simpan</button>
                    </form>
                </div>
                <!-- end card body -->
            </div>
        </div>
    </div>
    <!-- end col -->
    <!-- end row -->
@endsection
@section('script')
    <script src="{{ URL::asset('assets/libs/dropzone/dropzone.min.js') }}"></script>
    <script src="{{ URL::asset('assets/js/pages/form-validation.init.js') }}"></script>
    <script src="{{ URL::asset('/assets/js/app.min.js') }}"></script>
@endsection
