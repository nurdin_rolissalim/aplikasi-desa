@extends('layouts.admin.master')
@section('title')
    Info Desa - Form Aparat Desa
@endsection
@section('css')
    <link href="{{ URL::asset('assets/libs/dropzone/dropzone.min.css') }}" rel="stylesheet">
@endsection
@section('content')
    @component('components.breadcrumb')
        @slot('li_1')
            <a href="{{ route('aparatDesa.index') }}"> Aparat Desa</a>
        @endslot
        @slot('title')
            Ubah Aparat Desa
        @endslot
    @endcomponent

    <div class="row">
        <div class="col-md-4">
            <div class="card">
                <div class="card-body">
                    <div>
                        <form action="#" class="dropzone">
                            @csrf
                            <div class="fallback">
                                <input name="file" type="file" multiple="multiple">
                            </div>
                            <div class="dz-message needsclick">
                                <div class="mb-3">
                                    <i class="display-4 text-muted bx bx-cloud-upload"></i>
                                </div>

                                <h5>Drop files here or click to upload.</h5>
                            </div>
                        </form>
                    </div>

                    <div class="text-center mt-4">
                        <button type="button" class="btn btn-primary waves-effect waves-light">Send
                            Files</button>
                    </div>
                </div>
                <!-- end card body -->
            </div>
        </div>
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    <form class="needs-validation" novalidate>
                        <div class="row mb-3">
                            <label for="nama _pegawai" class="col-sm-3 col-form-label">Nama Pegawai</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" placeholder="Nama Pegawai" id="nama_pegawai"
                                    name="nama_pegawai" required>
                                <div class="invalid-feedback">
                                    Nama pegawai harus diisi
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="nik" class="col-sm-3 col-form-label">NIK</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" placeholder="NIK" id="nik" name="nik">
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="nipd" class="col-sm-3 col-form-label">NIPD</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" placeholder="NIPD" id="nipd" nama="nipd">
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="nip" class="col-sm-3 col-form-label">NIP</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" placeholder="NIP" id="nip" nama="nip">
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="tempat" class="col-sm-3 col-form-label">Tempat Lahir</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" placeholder="Tempat Lahir" id="tempat"
                                    nama="tempat">
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="date_of_birth" class="col-sm-3 col-form-label">Tanggal Lahir</label>
                            <div class="col-sm-9">
                                <input class="form-control" type="date" value="" id="date_of_birth" name="date_of_birth">
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="gender" class="col-sm-3 col-form-label">Jenis Kelamin</label>
                            <div class="col-sm-9">
                                <div class="form-check mb-3">
                                    <input class="form-check-input" type="radio" name="laki-laki" id="laki-laki" checked>
                                    <label class="form-check-label" for="laki-laki">
                                        Laki-Laki
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="perempuan" id="perempuan">
                                    <label class="form-check-label" for="perempuan">
                                        Perempuan
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="pendidikan" class="col-sm-3 col-form-label">Pendidikan</label>
                            <div class="col-sm-9">
                                <select class="form-select" id="pendidikan" name="pendidikan">
                                    <option>Pilih Pendidikan</option>
                                    <option value="Tidak/Belum Sekolah">Tidak/Belum Sekolah</option>
                                    <option value="Belum Tamat SD/Sederajat">Belum Tamat SD/Sederajat</option>
                                    <option value="Belum Tamata SLTP/Sederajat">Belum Tamat SLTP/Sederajat</option>
                                    <option value="Belum Tamata SLTA/Sederajat">Belum Tamat SLTA/Sederajat</option>
                                    <option value="Diploma I/II">Diploma I/II</option>
                                    <option value="Akademi/Diploma III/S.Muda">Akademi/Diploma III/S.Muda</option>
                                    <option value="Diploma IV/Strata I">Diploma IV/Strata I</option>
                                    <option value="Strata II">Strata II</option>
                                    <option value="Strata III">Strata III</option>
                                </select>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="agama" class="col-sm-3 col-form-label">Agama</label>
                            <div class="col-sm-9">
                                <select class="form-select" id="agama" name="agama">
                                    <option>Pilih Agama</option>
                                    <option value="Islam">Islam</option>
                                    <option value="Kristen">Kristen</option>
                                    <option value="Khatolik">Khatolik</option>
                                    <option value="Hindu">Hindu</option>
                                    <option value="Budha">Budha</option>
                                    <option value="Khonghucu">Khonghucu</option>
                                </select>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="pangkat" class="col-sm-3 col-form-label">Pangkat/Golongan</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" placeholder="Pangkat" id="pangkat" name="pangkat">

                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="no_sk_pengangkatan" class="col-sm-3 col-form-label">Nomor SK Pengangkatan</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" placeholder="Nomor SK Pengangkatan"
                                    id="no_sk_pengangkatan" name="no_sk_pengangkatan">
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="tgl_sk_pengangkatan" class="col-sm-3 col-form-label">Tanggal SK Pengangkatan</label>
                            <div class="col-sm-9">
                                <input class="form-control" type="date" value="" id="tgl_sk_pengangkatan"
                                    name="tgl_sk_pengangkatan">

                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="no_sk_pemberhentian" class="col-sm-3 col-form-label">Nomor SK Pemberhentian</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" placeholder="Nomor SK Pemberhentian"
                                    id="no_sk_pemberhentian" name="no_sk_pemberhentian">
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="tgl_sk_pemberhentian" class="col-sm-3 col-form-label">Tanggal SK
                                Pemberhentian</label>
                            <div class="col-sm-9">
                                <input class="form-control" type="date" value="" id="tgl_sk_pemberhentian"
                                    name="tgl_sk_pemberhentian">
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="masa_jabatan" class="col-sm-3 col-form-label">Masa Jabatan</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" placeholder="Masa Jabatan" id="masa_jabatan"
                                    name="masa_jabatan" required>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="jabatan" class="col-sm-3 col-form-label">Jabatan</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" placeholder="Jabatan" id="jabatan" name="jabatan"
                                    required>
                                <div class="invalid-feedback">
                                    Jabatan harus diisi
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="status" class="col-sm-3 col-form-label">Status Pegawai</label>
                            <div class="col-sm-9">
                                <div class="form-check mb-3">
                                    <input class="form-check-input" type="radio" name="formRadios" id="formRadios1" checked>
                                    <label class="form-check-label" for="formRadios1">
                                        Aktif
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="formRadios" id="formRadios2">
                                    <label class="form-check-label" for="formRadios2">
                                        Tidka Aktif
                                    </label>
                                </div>
                            </div>
                        </div>
                        <button class="btn btn-primary" type="submit">Simpan</button>
                    </form>
                </div>
                <!-- end card body -->
            </div>
        </div>
    </div>
    <!-- end col -->
    <!-- end row -->
@endsection
@section('script')
    <script src="{{ URL::asset('assets/libs/dropzone/dropzone.min.js') }}"></script>
    <script src="{{ URL::asset('assets/js/pages/form-validation.init.js') }}"></script>
    <script src="{{ URL::asset('/assets/js/app.min.js') }}"></script>
@endsection
