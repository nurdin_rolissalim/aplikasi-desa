@extends('layouts.admin.master')
@section('title')
    Info Desa - Ubah Identitas Desa
@endsection
@section('css')
    <link href="{{ URL::asset('assets/libs/dropzone/dropzone.min.css') }}" rel="stylesheet">
@endsection
@section('content')
    @component('components.breadcrumb')
        @slot('li_1')
            <a href="{{ route('identitasDesa.index') }}"> Identitas Desa</a>
        @endslot
        @slot('title')
            Ubah Identitas Desa
        @endslot
    @endcomponent

    <div class="row">
        <div class="col-md-4">
            <div class="card">
                <div class="card-body">
                    <div>
                        <form action="#" class="dropzone">
                            @csrf
                            <div class="fallback">
                                <input name="file" type="file" multiple="multiple">
                            </div>
                            <div class="dz-message needsclick">
                                <div class="mb-3">
                                    <i class="display-4 text-muted bx bx-cloud-upload"></i>
                                </div>

                                <h5>Drop files here or click to upload.</h5>
                            </div>
                        </form>
                    </div>

                    <div class="text-center mt-4">
                        <button type="button" class="btn btn-primary waves-effect waves-light">Send
                            Files</button>
                    </div>
                </div>
                <!-- end card body -->
            </div>
            <div class="card">
                <div class="card-body">
                    <div>
                        <form action="#" class="dropzone">
                            @csrf
                            <div class="fallback">
                                <input name="file" type="file">
                            </div>
                            <div class="dz-message needsclick">
                                <div class="mb-3">
                                    <i class="display-4 text-muted bx bx-cloud-upload"></i>
                                </div>

                                <h5>Drop files here or click to upload.</h5>
                            </div>
                        </form>
                    </div>

                    <div class="text-center mt-4">
                        <button type="button" class="btn btn-primary waves-effect waves-light">Send
                            Files</button>
                    </div>
                </div>
                <!-- end card body -->
            </div>
        </div>
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    <form class="needs-validation" novalidate>
                        <div class="row mb-3">
                            <label for="kode_desa" class="col-sm-3 col-form-label">Kode Desa</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" placeholder="Kode Desa" id="kode_desa"
                                    name="kode_desa" required>
                                <div class="invalid-feedback">
                                    Kode Desa harus diisi
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="kode_pos_desa" class="col-sm-3 col-form-label">Kode Pos Desa</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" placeholder="Kode Desa" id="kode_pos_desa"
                                    name="kode_pos_desa" required>
                                <div class="invalid-feedback">
                                    Kode Pos Desa harus diisi
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="nama_kepala" class="col-sm-3 col-form-label">Nama Kepla</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" placeholder="Nama Kepala" id="nama_kepala"
                                    name="nama_kepala" required>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="nik_kepala" class="col-sm-3 col-form-label">NIK Kepala</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" placeholder="NIK Kepala" id="nik_kepala"
                                    nama="nik_kepala" required>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="alamat" class="col-sm-3 col-form-label">Alamat Desa</label>
                            <div class="col-sm-9">
                                <textarea class="form-control" placeholder="Alamat Desa" id="alamat" name="alamat" required></textarea>
                                <div class="invalid-feedback">
                                    Alamat desa harus diisi
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="email_desa" class="col-sm-3 col-form-label">Email Desa</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" placeholder="Email Desa" id="email_desa"
                                    name="email_desa" required>
                                <div class="invalid-feedback">
                                    Email desa harus diisi
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="telepon_desa" class="col-sm-3 col-form-label">Telepon Desa</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" placeholder="Telepon Desa" id="telepon_desa"
                                    name="telepon_desa" required>
                                <div class="invalid-feedback">
                                    Telepon desa harus diisi
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="website_desa" class="col-sm-3 col-form-label">Website Desa</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" placeholder="Website Desa" id="website_desa"
                                    name="website_desa" required>
                                <div class="invalid-feedback">
                                    Website desa harus diisi
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="kode_kecamatan" class="col-sm-3 col-form-label">Kode Kecamatan</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" placeholder="Kode Kecamatan" id="kode_kecamatan"
                                    name="kode_kecamatan" required>
                                <div class="invalid-feedback">
                                    Kode kecamatan harus diisi
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="nama_kecamatan" class="col-sm-3 col-form-label">Nama Kecamatan</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" placeholder="Nama Kecamatan" id="nama_kecamatan"
                                    name="nama_kecamatan" required>
                                <div class="invalid-feedback">
                                    Nama kecamatan harus diisi
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="nama_camat" class="col-sm-3 col-form-label">Nama Camat</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" placeholder="Nama Camat" id="nama_camat"
                                    name="nama_camat" required>
                                <div class="invalid-feedback">
                                    Nama camat harus diisi
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="nip_camat" class="col-sm-3 col-form-label">NIP Camat</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" placeholder="NIP Camat" id="nip_camat"
                                    name="nip_camat" required>
                                <div class="invalid-feedback">
                                    NIP camat harus diisi
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="kode_kabupaten" class="col-sm-3 col-form-label">Kode Kabupaten</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" placeholder="Kode kabupaten" id="kode_kabupaten"
                                    name="kode_kabupaten" required>
                                <div class="invalid-feedback">
                                    Kode kabupaten harus diisi
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="kode_desa" class="col-sm-3 col-form-label">Nama Kabupaten</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" placeholder="Nama Kabupaten" id="nama_kabupaten"
                                    name="nama_kabupaten" required>
                                <div class="invalid-feedback">
                                    Nama kabupaten harus diisi
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="kode_provinsi" class="col-sm-3 col-form-label">Kode Provinsi</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" placeholder="Kode Provinsi" id="kode_provinsi"
                                    name="kode_provinsi" required>
                                <div class="invalid-feedback">
                                    Kode provinsi harus diisi
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="nama_provinsi" class="col-sm-3 col-form-label">Nama Provinsi</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" placeholder="Nama Provinsi" id="nama_provinsi"
                                    required>
                                <div class="invalid-feedback">
                                    Nama provinsi harus diisi
                                </div>
                            </div>
                        </div>
                        <button class="btn btn-primary" type="submit">Simpan</button>
                    </form>
                </div>
                <!-- end card body -->
            </div>
        </div>
    </div>
    <!-- end col -->
    <!-- end row -->
@endsection
@section('script')
    <script src="{{ URL::asset('assets/libs/dropzone/dropzone.min.js') }}"></script>
    <script src="{{ URL::asset('assets/js/pages/form-validation.init.js') }}"></script>
    <script src="{{ URL::asset('/assets/js/app.min.js') }}"></script>
@endsection
