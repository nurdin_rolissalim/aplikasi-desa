@extends('layouts.admin.master')
@section('title')
    Info Desa - Identitas Desa
@endsection
@section('content')
    @component('components.breadcrumb')
        @slot('li_1')
            Info Desa
        @endslot
        @slot('title')
            <a href="{{ route('identitasDesa.index') }}"> Identitas Desa</a>
        @endslot
    @endcomponent

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    {{-- <div class="card-header">
                        <h4 class="card-title">Desa</h4>

                    </div> --}}
                    <div class="table-responsive">
                        <div>
                            <a href="{{ route('identitasDesa.edit') }}"
                                class="btn btn-warning waves-effect btn-label waves-light">
                                <i class="bx bx-pencil label-icon"></i> Ubah Data Desa
                            </a>
                            <button type="button" class="btn btn-primary waves-effect btn-label waves-light">
                                <i class="bx bx-map label-icon"></i> Primary
                            </button>
                            <button type="button" class="btn btn-primary waves-effect btn-label waves-light">
                                <i class="bx bx-feather label-icon"></i> Peta WIlayah Desa
                            </button>
                        </div>
                        <div>
                            image
                        </div>
                        <table class="table table-striped table-sm m-0">
                            <thead class="table-info">
                                <tr>
                                    <th colspan="2">Desa</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th><span class="co-name">Nama Desa</span> </th>
                                    <td>Cililin</td>
                                </tr>
                                <tr>
                                    <th><span class="co-name">Kode Desa</span> </th>
                                    <td>Cililin</td>
                                </tr>

                                <tr>
                                    <th><span class="co-name">Kode Pos Desa</span> </th>
                                    <td>Cililin</td>
                                </tr>

                            </tbody>
                        </table>
                        <table class="table table-striped table-sm m-0 mt-1">
                            <thead class="table-info">
                                <tr>
                                    <th colspan="2">Kecamatan</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th><span class="co-name">Nama Desa</span> </th>
                                    <td>Cililin</td>
                                </tr>
                                <tr>
                                    <th><span class="co-name">Kode Desa</span> </th>
                                    <td>Cililin</td>
                                </tr>

                                <tr>
                                    <th><span class="co-name">Kode Pos Desa</span> </th>
                                    <td>Cililin</td>
                                </tr>

                            </tbody>
                        </table>
                        <table class="table table-striped table-sm m-0 mt-1">
                            <thead class="table-info">
                                <tr>
                                    <th colspan="2">Kabupten</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th><span class="co-name">Nama Desa</span> </th>
                                    <td>Cililin</td>
                                </tr>
                                <tr>
                                    <th><span class="co-name">Kode Desa</span> </th>
                                    <td>Cililin</td>
                                </tr>

                                <tr>
                                    <th><span class="co-name">Kode Pos Desa</span> </th>
                                    <td>Cililin</td>
                                </tr>

                            </tbody>
                        </table>
                        <table class="table table-striped table-sm m-0 mt-1">
                            <thead class="table-info">
                                <tr>
                                    <th colspan="2">Provinsi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th><span class="co-name">Nama Desa</span> </th>
                                    <td>Cililin</td>
                                </tr>
                                <tr>
                                    <th><span class="co-name">Kode Desa</span> </th>
                                    <td>Cililin</td>
                                </tr>

                                <tr>
                                    <th><span class="co-name">Kode Pos Desa</span> </th>
                                    <td>Cililin</td>
                                </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- end card body -->
            </div>
        </div>
    </div>
    <!-- end col -->
    <!-- end row -->
@endsection
@section('script')
    <script src="{{ URL::asset('/assets/js/app.min.js') }}"></script>
@endsection
