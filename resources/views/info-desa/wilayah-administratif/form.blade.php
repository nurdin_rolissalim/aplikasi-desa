@extends('layouts.admin.master')
@section('title')
    Info Desa - Ubah Identitas Desa
@endsection
@section('css')
    <link href="{{ URL::asset('assets/libs/dropzone/dropzone.min.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('assets/libs/choices.js/choices.js.min.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('assets/libs/@simonwep/@simonwep.min.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('assets/libs/flatpickr/flatpickr.min.css') }}" rel="stylesheet">
@endsection
@section('content')
    @component('components.breadcrumb')
        @slot('li_1')
            <a href="{{ route('identitasDesa.index') }}"> Identitas Desa</a>
        @endslot
        @slot('title')
            Ubah Identitas Desa
        @endslot
    @endcomponent

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <form class="needs-validation" novalidate>
                        <div class="row mb-3">
                            <label for="kode_desa" class="col-sm-3 col-form-label">Nama Dusun</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" placeholder="Nama Dusun" id="nama_dusun"
                                    name="nama_dusun" required>
                                <div class="invalid-feedback">
                                    Nama dusun harus diisi
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="kode_desa" class="col-sm-3 col-form-label">Nama Kepla Dusun</label>
                            <div class="col-sm-9">
                                <select class="form-control" data-trigger name="choices-single-default"
                                    id="choices-single-default" placeholder="This is a search placeholder">
                                    <option value="">This is a placeholder</option>
                                    <option value="Choice 1">Choice 1</option>
                                    <option value="Choice 2">Choice 2</option>
                                    <option value="Choice 3">Choice 3</option>
                                </select>
                            </div>
                        </div>
                        <button class="btn btn-primary mt-3" type="submit">Simpan</button>

                    </form>
                </div>
                <!-- end card body -->
            </div>
        </div>
    </div>
    <!-- end col -->
    <!-- end row -->
@endsection
@section('script')
    <script src="{{ URL::asset('assets/libs/dropzone/dropzone.min.js') }}"></script>
    <script src="{{ URL::asset('assets/js/pages/form-validation.init.js') }}"></script>
    <script src="{{ URL::asset('assets/libs/choices.js/choices.js.min.js') }}"></script>
    <script src="{{ URL::asset('assets/libs/@simonwep/@simonwep.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/js/app.min.js') }}"></script>
@endsection
