@extends('layouts.master-layout-member')
@section('title')
    Ganti PIN
@endsection
@section('css')

@endsection
@section('content')
    <div class="row">
        <div class="col-xl-3">
            @include('layouts.master-member-profile')
            <!-- end card -->
        </div>
        <!-- end col -->

        <div class="col-xl-9">
            <div class="card">
                <div class="card-header">
                    <h4>Ganti PIN</h4>
                </div>
                <div class="card-body">
                    <form class="needs-validation" novalidate>
                       
                        <div class="row mb-3">
                            <label for="no_hp" class="col-sm-3 col-form-label">PIN Lama</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" placeholder="NO HP " id="no_hp"
                                    name="no_hp" required>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="no_hp" class="col-sm-3 col-form-label">PIN Baru</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" placeholder="NO HP " id="no_hp"
                                    name="no_hp" required>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="no_hp" class="col-sm-3 col-form-label">Konfirmasi PIN Baru</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" placeholder="NO HP " id="no_hp"
                                    name="no_hp" required>
                            </div>
                        </div>
                       
                        <div class="row mb-3">
                            <label for="no_hp" class="col-sm-3 col-form-label"></label>
                            <div class="col-sm-9">
                                <button class="btn btn-primary mt-3" type="submit">Simpan</button>

                            </div>
                        </div>

                    </form>
                </div>
                <!-- end card body -->
            </div>
            <!-- end card -->
        </div>
        <!-- end col -->

        <!-- end col -->
    </div><!-- end row -->
@endsection
@section('script')
    <script src="{{ URL::asset('/assets/js/app.min.js') }}"></script>
@endsection
