@extends('layouts.master-layout-member')
@section('title')
    Profile
@endsection
@section('css')
    <link href="{{ URL::asset('assets/libs/jsvectormap/css/jsvectormap.min.css') }}" rel="stylesheet">
@endsection
@section('content')
    <div class="row">
        <div class="col-xl-3">
            @include('layouts.master-member-profile')
            <!-- end card -->
        </div>
        <!-- end col -->

        <div class="col-xl-9">
            <div class="card">
                <div class="card-header align-items-center d-flex">
                    <h4 class="card-title mb-0 flex-grow-1">Profile</h4>
                </div><!-- end card header -->

                <div class="card-body px-0 pt-2">
                    <div class="table-responsive px-3" data-simplebar style="max-height: 395px;">
                        <div class="table-responsive">
                            <table class="table mb-0">
                                <tbody>
                                    <tr class="table-light">
                                        <th scope="row" colspan="2">Data Dasar</th>
                                    </tr>
                                    <tr>
                                        <th scope="row">NIK</th>
                                        <td>123123</td>

                                    </tr>
                                    <tr>
                                        <th scope="row">Nama</th>
                                        <td>ahmad</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">status Kepemilikan KTP</th>
                                        <td>
                                            <table class="table mb-0">
                                                <tbody>
                                                    <tr class="table-light">
                                                        <th scope="row">Wajib KTP</th>
                                                        <th scope="row">KTP EL</th>
                                                        <th scope="row">Status Rekam</th>
                                                        <th scope="row">Tag ID Card</th>
                                                    </tr>
                                                    <tr>
                                                        <td scope="row">Wajib</td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>

                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Nomor Kartu Keluarga</th>
                                        <td>123123</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Nomor KK Sebelumnya</th>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Hubungan Dalam Keluarga</th>
                                        <td>istri</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Jenis Kelamin</th>
                                        <td>perempuan</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Agama</th>
                                        <td>ahmad</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Status Penduduk</th>
                                        <td>WNI</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Agama</th>
                                        <td>islam</td>
                                    </tr>

                                    <tr class="table-light">
                                        <th scope="row" colspan="2">Data Kelahiran</th>
                                    </tr>
                                    <tr>
                                        <th scope="row">Akta Kelahiran</th>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Tempat Tanggal Lahir</th>
                                        <td>jakarta, 21 juni 2022</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Tempat Dilahirkan</th>
                                        <td>jakarta</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Jenis kelahiran</th>
                                        <td>normal</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Anak ke</th>
                                        <td>1</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Penolong Kelahiran</th>
                                        <td>Dukun beranak</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Berat Kelahiran</th>
                                        <td>Gram</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Panjang Kelahiran</th>
                                        <td>cm</td>
                                    </tr>

                                    <tr class="table-light">
                                        <th scope="row" colspan="2">Data Pendidikan dan Pekerjaan</th>
                                    </tr>
                                    <tr>
                                        <th scope="row">Pendidikan dalam KK</th>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Pendidikan sedang ditempuh</th>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Pekerjaan</th>
                                        <td></td>
                                    </tr>


                                    <tr class="table-light">
                                        <th scope="row" colspan="2">Data Kewarganegaraan</th>
                                    </tr>
                                    <tr>
                                        <th scope="row">Warga Negara</th>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Nomor Paspor</th>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Tanggal Berakhir Paspor</th>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Nomor KITAS/KITAP</th>
                                        <td></td>
                                    </tr>

                                    <tr class="table-light">
                                        <th scope="row" colspan="2">Data Orang Tua</th>
                                    </tr>
                                    <tr>
                                        <th scope="row">NIK Ayah</th>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Nama Ayah</th>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">NIK Ibu</th>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Nama Ibu</th>
                                        <td></td>
                                    </tr>


                                    <tr class="table-light">
                                        <th scope="row" colspan="2">Data Alamat</th>
                                    </tr>
                                    <tr>
                                        <th scope="row">Nomor Telepon</th>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Email</th>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Alamat</th>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Dusun</th>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">RT/RW</th>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Alamat Sebelumnya</th>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Status Kawin</th>
                                        <td></td>
                                    </tr>

                                    <tr class="table-light">
                                        <th scope="row" colspan="2">Data Perkawinan</th>
                                    </tr>
                                    <tr>
                                        <th scope="row">Status Kawin</th>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Akta perkawinan </th>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Tanggal perkawinan </th>
                                        <td></td>
                                    </tr>
                                </tbody>
                            </table>

                        </div>
                        <h4 class="mt-3">DOKUMEN / KELENGKAPAN PENDUDUK</h4>
                        <div class="table-responsive">
                            <table class="table mb-0">
                                <tbody>
                                    <tr class="table-light">
                                        <th scope="row">No</th>
                                        <th>Aksi</th>
                                        <th>Nama Dokumen</th>
                                        <th>Tanggal Upload</th>
                                    </tr>
                                    <tr>
                                        <th scope="row">1</th>
                                        <td>dwonload</td>
                                        <td>test</td>
                                        <td>Tanggal Upload</td>

                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>


                </div>
                <!-- end card body -->
            </div>
            <!-- end card -->
        </div>
        <!-- end col -->

        <!-- end col -->
    </div><!-- end row -->
@endsection
@section('script')
    <script src="{{ URL::asset('assets/libs/apexcharts/apexcharts.min.js') }}"></script>
    <script src="{{ URL::asset('assets/libs/jsvectormap/jsvectormap.min.js') }}"></script>
    <script src="{{ URL::asset('assets/libs/jsvectormap/maps/world-merc.js') }}"></script>
    <script src="{{ URL::asset('assets/js/pages/dashboard-analytics.init.js') }}"></script>
    <script src="{{ URL::asset('/assets/js/app.min.js') }}"></script>
@endsection
