@extends('layouts.master-layout-member')
@section('title')
    Surat
@endsection
@section('css')
    <link href="{{ URL::asset('assets/libs/jsvectormap/css/jsvectormap.min.css') }}" rel="stylesheet">
@endsection
@section('content')
    <div class="row">
        <div class="col-xl-3">
            @include('layouts.master-member-profile')
            <!-- end card -->
        </div>
        <!-- end col -->

        <div class="col-xl-9">
            <div class="card">
                <div class="card-header align-items-center d-flex">
                    <h4 class="card-title mb-0 flex-grow-1">DAFTAR PERMOHONAN SURAT</h4>
                    <a href="{{ route('profileSurat.create') }}" class="btn btn-primary waves-effect btn-label waves-light">
                        <i class="bx bx-plus label-icon"></i> Buat Surat
                    </a>
                </div><!-- end card header -->

                <div class="card-body px-0 pt-2">
                  
                    <div class="table-responsive mt-2">
                        <table class="table mb-0">
                            <tbody>
                                <tr class="table-light">
                                    <th scope="row">No</th>
                                    <th>Aksi</th>
                                    <th>No Antrian</th>
                                    <th>Jenis Surat</th>
                                    <th>Tanggal Kirim</th>

                                </tr>
                                <tr>
                                    <th scope="row">1</th>
                                    <td>dwonload</td>
                                    <td>test</td>
                                    <td>Tanggal Upload</td>
                                    <td>Tanggal Upload</td>

                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- end card body -->
            </div>
            <!-- end card -->
        </div>
        <!-- end col -->

        <!-- end col -->
    </div><!-- end row -->
@endsection
@section('script')
    <script src="{{ URL::asset('assets/libs/apexcharts/apexcharts.min.js') }}"></script>
    <script src="{{ URL::asset('assets/libs/jsvectormap/jsvectormap.min.js') }}"></script>
    <script src="{{ URL::asset('assets/libs/jsvectormap/maps/world-merc.js') }}"></script>
    <script src="{{ URL::asset('assets/js/pages/dashboard-analytics.init.js') }}"></script>
    <script src="{{ URL::asset('/assets/js/app.min.js') }}"></script>
@endsection
