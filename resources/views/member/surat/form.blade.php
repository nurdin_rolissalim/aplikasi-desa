@extends('layouts.master-layout-member')
@section('title')
    Buat Surat
@endsection
@section('css')
    <link href="{{ URL::asset('assets/libs/jsvectormap/css/jsvectormap.min.css') }}" rel="stylesheet">
@endsection
@section('content')
    <div class="row">
        <div class="col-xl-3">
            @include('layouts.master-member-profile')
            <!-- end card -->
        </div>
        <!-- end col -->

        <div class="col-xl-9">
            <div class="card">
                <div class="card-header">
                    <h4>Surat</h4>
                </div>
                <div class="card-body">
                    <form class="needs-validation" novalidate>
                        <div class="row mb-3">
                            <label for="kode_desa" class="col-sm-3 col-form-label">Jenis Surat</label>
                            <div class="col-sm-9">
                                <select class="form-control" data-trigger name="choices-single-default"
                                    id="choices-single-default" placeholder="This is a search placeholder">
                                    <option value="">Pilih Jenis Surat</option>
                                    <option value="Choice 1">Choice 1</option>
                                    <option value="Choice 2">Choice 2</option>
                                    <option value="Choice 3">Choice 3</option>
                                </select>

                                <div class="invalid-feedback">
                                    Jenis surat harus dipilih
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="kode_desa" class="col-sm-3 col-form-label">Keterangan Surat</label>
                            <div class="col-sm-9">
                                <textarea rows="3" class="form-control"></textarea>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="no_hp" class="col-sm-3 col-form-label">No HP Aktif</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" placeholder="NO HP " id="no_hp"
                                    name="no_hp" required>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="no_hp" class="col-sm-3 col-form-label"></label>
                            <div class="col-sm-9">
                                <button class="btn btn-primary mt-3" type="submit">Simpan</button>

                            </div>
                        </div>

                    </form>
                </div>
                <!-- end card body -->
            </div>
            <!-- end card -->
        </div>
        <!-- end col -->

        <!-- end col -->
    </div><!-- end row -->
@endsection
@section('script')
    <script src="{{ URL::asset('/assets/js/app.min.js') }}"></script>
@endsection
