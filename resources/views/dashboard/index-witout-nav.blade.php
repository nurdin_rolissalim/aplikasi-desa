@extends('layouts.master-without-nav')
@section('title')
    Dashboard
@endsection
@section('content')
    <div class="container-fluid ">
        <div class="row py-4">
            <div class="col-md-12">
                <div id="dashboard" class="carousel slide" data-bs-ride="carousel">
                    <div class="carousel-inner" role="listbox">
                        <div class="carousel-item active">
                            <div class="row">
                                <div class="col-xl-3 col-md-6">
                                    <!-- card -->
                                    <div class="card card-h-100">
                                        <!-- card body -->
                                        <div class="card-body">
                                            <div class="d-flex align-items-center">
                                                <div class="flex-grow-1">
                                                    <span class="text-muted mb-3 lh-1 d-block text-truncate">Total
                                                        Penduduk</span>
                                                    <h4 class="mb-3">
                                                        <span class="counter-value" data-target="354">0</span>k
                                                    </h4>
                                                    <div class="text-nowrap">
                                                        <span class="badge bg-soft-success text-success">20.9k</span>
                                                        <span class="ms-1 text-muted font-size-13">Tahun Ini</span>
                                                    </div>
                                                </div>

                                                <div class="flex-shrink-0 text-end dash-widget">
                                                    <div id="penduduk" data-colors='["#1c84ee", "#33c38e"]'
                                                        class="apex-charts"></div>
                                                </div>
                                            </div>
                                        </div><!-- end card body -->
                                    </div><!-- end card -->
                                </div><!-- end col -->

                                <div class="col-xl-3 col-md-6">
                                    <!-- card -->
                                    <div class="card card-h-100">
                                        <!-- card body -->
                                        <div class="card-body">
                                            <div class="d-flex align-items-center">
                                                <div class="flex-grow-1">
                                                    <span class="text-muted mb-3 lh-1 d-block text-truncate">Total
                                                        Calon Pemilih</span>
                                                    <h4 class="mb-3">
                                                        <span class="counter-value" data-target="1256">0</span>
                                                    </h4>
                                                    <div class="text-nowrap">
                                                        <span class="badge bg-soft-danger text-danger">29 Trades</span>
                                                        <span class="ms-1 text-muted font-size-13">Tahun Ini</span>
                                                    </div>
                                                </div>
                                                <div class="flex-shrink-0 text-end dash-widget">
                                                    <div id="calon-pemilih" data-colors='["#1c84ee", "#33c38e"]'
                                                        class="apex-charts"></div>
                                                </div>
                                            </div>
                                        </div><!-- end card body -->
                                    </div><!-- end card -->
                                </div>

                                <div class="col-xl-3 col-md-6">
                                    <!-- card -->
                                    <div class="card card-h-100">
                                        <!-- card body -->
                                        <div class="card-body">
                                            <div class="d-flex align-items-center">
                                                <div class="flex-grow-1">
                                                    <span class="text-muted mb-3 lh-1 d-block text-truncate">Program
                                                        Desa</span>
                                                    <h4 class="mb-3">
                                                        <span class="counter-value" data-target="7.54">0</span>M
                                                    </h4>
                                                    <div class="text-nowrap">
                                                        <span class="badge bg-soft-success text-success">2.8k</span>
                                                        <span class="ms-1 text-muted font-size-13">Tahun Ini</span>
                                                    </div>
                                                </div>
                                                <div class="flex-shrink-0 text-end dash-widget">
                                                    <div id="program-desa" data-colors='["#1c84ee", "#33c38e"]'
                                                        class="apex-charts"></div>
                                                </div>
                                            </div>
                                        </div><!-- end card body -->
                                    </div><!-- end card -->
                                </div>

                                <div class="col-xl-3 col-md-6">
                                    <!-- card -->
                                    <div class="card card-h-100">
                                        <!-- card body -->
                                        <div class="card-body">
                                            <div class="d-flex align-items-center">
                                                <div class="flex-grow-1">
                                                    <span class="text-muted mb-3 lh-1 d-block text-truncate">Total Pengajuan
                                                        Surat</span>
                                                    <h4 class="mb-3">
                                                        <span class="counter-value" data-target="18.34">0</span>%
                                                    </h4>
                                                    <div class="text-nowrap">
                                                        <span class="badge bg-soft-success text-success">5.32%</span>
                                                        <span class="ms-1 text-muted font-size-13">Tahun Ini</span>
                                                    </div>
                                                </div>
                                                <div class="flex-shrink-0 text-end dash-widget">
                                                    <div id="pengajuan-surat" data-colors='["#1c84ee", "#33c38e"]'
                                                        class="apex-charts"></div>
                                                </div>
                                            </div>
                                        </div><!-- end card body -->
                                    </div><!-- end card -->
                                </div>
                            </div><!-- end row-->

                            <div class="row">
                                <div class="col-xl-6">
                                    <!-- card -->
                                    <div class="card">
                                        <!-- card body -->
                                        <div class="card-body">
                                            <div class="d-flex flex-wrap align-items-center mb-4">
                                                <h5 class="card-title me-2">Market Overview</h5>
                                            </div>
                                            <div class="row align-items-center">
                                                <div class="col-xl-8">
                                                    <div>
                                                        <div id="market-overview" data-colors='["#1c84ee", "#33c38e"]'
                                                            class="apex-charts"></div>
                                                    </div>
                                                </div>
                                                <div class="col-xl-4">
                                                    <div class="p-4">
                                                        <div class="mt-0">
                                                            <div class="d-flex align-items-center">
                                                                <div class="avatar-sm m-auto">
                                                                    <span
                                                                        class="avatar-title rounded-circle bg-light text-dark font-size-13">
                                                                        1
                                                                    </span>
                                                                </div>
                                                                <div class="flex-grow-1 ms-3">
                                                                    <span class="font-size-14">Mobile Phones</span>
                                                                </div>

                                                                <div class="flex-shrink-0">
                                                                    <span
                                                                        class="badge rounded-pill badge-soft-success font-size-12 fw-medium">+5.4%</span>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="mt-3">
                                                            <div class="d-flex align-items-center">
                                                                <div class="avatar-sm m-auto">
                                                                    <span
                                                                        class="avatar-title rounded-circle bg-light text-dark font-size-13">
                                                                        2
                                                                    </span>
                                                                </div>
                                                                <div class="flex-grow-1 ms-3">
                                                                    <span class="font-size-14">Smart Watch</span>
                                                                </div>

                                                                <div class="flex-shrink-0">
                                                                    <span
                                                                        class="badge rounded-pill badge-soft-success font-size-12 fw-medium">+6.8%</span>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="mt-3">
                                                            <div class="d-flex align-items-center">
                                                                <div class="avatar-sm m-auto">
                                                                    <span
                                                                        class="avatar-title rounded-circle bg-light text-dark font-size-13">
                                                                        3
                                                                    </span>
                                                                </div>
                                                                <div class="flex-grow-1 ms-3">
                                                                    <span class="font-size-14">Protable Acoustics</span>
                                                                </div>

                                                                <div class="flex-shrink-0">
                                                                    <span
                                                                        class="badge rounded-pill badge-soft-danger font-size-12 fw-medium">-4.9%</span>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="mt-3">
                                                            <div class="d-flex align-items-center">
                                                                <div class="avatar-sm m-auto">
                                                                    <span
                                                                        class="avatar-title rounded-circle bg-light text-dark font-size-13">
                                                                        4
                                                                    </span>
                                                                </div>
                                                                <div class="flex-grow-1 ms-3">
                                                                    <span class="font-size-14">Smart Speakers</span>
                                                                </div>

                                                                <div class="flex-shrink-0">
                                                                    <span
                                                                        class="badge rounded-pill badge-soft-success font-size-12 fw-medium">+3.5%</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- end card -->
                                    </div>
                                    <!-- end col -->
                                </div>
                                <!-- end row-->

                                <div class="col-xl-6">
                                    <!-- card -->
                                    <div class="card">
                                        <!-- card body -->
                                        <div class="card-body">
                                            <div class="d-flex flex-wrap align-items-center mb-4">
                                                <h5 class="card-title me-2">Market Overview</h5>
                                            </div>
                                            <div class="row align-items-center">
                                                <div class="col-xl-8">
                                                    <div>
                                                        <div id="market-overview" data-colors='["#1c84ee", "#33c38e"]'
                                                            class="apex-charts"></div>
                                                    </div>
                                                </div>
                                                <div class="col-xl-4">
                                                    <div class="p-4">
                                                        <div class="mt-0">
                                                            <div class="d-flex align-items-center">
                                                                <div class="avatar-sm m-auto">
                                                                    <span
                                                                        class="avatar-title rounded-circle bg-light text-dark font-size-13">
                                                                        1
                                                                    </span>
                                                                </div>
                                                                <div class="flex-grow-1 ms-3">
                                                                    <span class="font-size-14">Mobile Phones</span>
                                                                </div>

                                                                <div class="flex-shrink-0">
                                                                    <span
                                                                        class="badge rounded-pill badge-soft-success font-size-12 fw-medium">+5.4%</span>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="mt-3">
                                                            <div class="d-flex align-items-center">
                                                                <div class="avatar-sm m-auto">
                                                                    <span
                                                                        class="avatar-title rounded-circle bg-light text-dark font-size-13">
                                                                        2
                                                                    </span>
                                                                </div>
                                                                <div class="flex-grow-1 ms-3">
                                                                    <span class="font-size-14">Smart Watch</span>
                                                                </div>

                                                                <div class="flex-shrink-0">
                                                                    <span
                                                                        class="badge rounded-pill badge-soft-success font-size-12 fw-medium">+6.8%</span>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="mt-3">
                                                            <div class="d-flex align-items-center">
                                                                <div class="avatar-sm m-auto">
                                                                    <span
                                                                        class="avatar-title rounded-circle bg-light text-dark font-size-13">
                                                                        3
                                                                    </span>
                                                                </div>
                                                                <div class="flex-grow-1 ms-3">
                                                                    <span class="font-size-14">Protable Acoustics</span>
                                                                </div>

                                                                <div class="flex-shrink-0">
                                                                    <span
                                                                        class="badge rounded-pill badge-soft-danger font-size-12 fw-medium">-4.9%</span>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="mt-3">
                                                            <div class="d-flex align-items-center">
                                                                <div class="avatar-sm m-auto">
                                                                    <span
                                                                        class="avatar-title rounded-circle bg-light text-dark font-size-13">
                                                                        4
                                                                    </span>
                                                                </div>
                                                                <div class="flex-grow-1 ms-3">
                                                                    <span class="font-size-14">Smart Speakers</span>
                                                                </div>

                                                                <div class="flex-shrink-0">
                                                                    <span
                                                                        class="badge rounded-pill badge-soft-success font-size-12 fw-medium">+3.5%</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- end card -->
                                    </div>
                                    <!-- end col -->
                                </div>
                                <!-- end col -->
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="row">
                                <div class="col-xl-12">
                                    <!-- card -->
                                    <div class="card">
                                        <!-- card body -->
                                        <div class="card-body">
                                            <div class="d-flex flex-wrap align-items-center mb-4">
                                                <h5 class="card-title me-2">Dana Desa</h5>
                                            </div>
                                            <div class="row align-items-center">
                                                <div class="col-xl-12">
                                                    <div>
                                                        <div id="dana-desa" data-colors='["#1c84ee", "#33c38e"]'
                                                            class="apex-charts"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- end card -->
                                    </div>
                                    <!-- end col -->
                                </div>
                                <!-- end row-->

                                <!-- end col -->
                            </div>
                        </div>
                    </div>
                    {{-- <a class="carousel-control-prev" href="#carouselExampleControls" role="button"
                        data-bs-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleControls" role="button"
                        data-bs-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a> --}}
                </div>
            </div>
        </div>
        <!-- end row -->
    </div>
    <!-- end container fluid -->
@endsection
@section('script')
    <script src="{{ URL::asset('/assets/libs/apexcharts/apexcharts.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/libs/admin-resources/admin-resources.min.js') }}"></script>
    <script src="{{ URL::asset('js/pages/dashboard/index.js') }}"></script>
    {{-- <script src="{{ URL::asset('/assets/js/app.min.js') }}"></script> --}}
    {{-- <script src="{{ URL::asset('assets/libs/prismjs/prismjs.min.js') }}"></script> --}}
@endsection
