@extends('layouts.master-layouts-witout-top-bar')
@section('title')
    Dashboard
@endsection
@section('css')
@endsection

@section('content')
    <div class="row py-4">
        <div class="col-md-12">
            <div id="dashboard" class="carousel slide" data-bs-ride="carousel">
                <div class="carousel-inner" role="listbox">
                    <div class="carousel-item active">
                        <div class="row">
                            <div class="col-xl-3 col-md-6">
                                <!-- card -->
                                <div class="card card-h-100">
                                    <!-- card body -->
                                    <div class="card-body">
                                        <div class="d-flex align-items-center">
                                            <div class="flex-grow-1">
                                                <span class="text-muted mb-3 lh-2 d-block text-truncate">Total
                                                    Penduduk</span>
                                                <h4 class="mb-3">
                                                    <span class="counter-value" data-target="354">0</span>k
                                                </h4>
                                                <div class="text-nowrap">
                                                    <span class="badge bg-soft-danger text-danger font-size-22">
                                                        <i class=" fa-solid fas fa-female"></i>
                                                    </span>
                                                    <span class="badge bg-soft-primary text-primary font-size-22">
                                                        <i class=" fa-solid fas fa-male"></i>
                                                </div>
                                            </div>

                                            <div class="flex-shrink-0 text-end dash-widget">
                                                <div id="pie-penduduk" data-colors='["#1c84ee", "#ef6767"]'
                                                    class="apex-charts"></div>
                                            </div>
                                        </div>
                                    </div><!-- end card body -->
                                </div><!-- end card -->
                            </div><!-- end col -->

                            <div class="col-xl-3 col-md-6">
                                <!-- card -->
                                <div class="card card-h-100">
                                    <!-- card body -->
                                    <div class="card-body">
                                        <div class="d-flex align-items-center">
                                            <div class="flex-grow-1">
                                                <span class="text-muted mb-3 lh-2 d-block text-truncate">Calon
                                                    Pemilih</span>
                                                <h4 class="mb-3">
                                                    <span class="counter-value" data-target="1256">0</span>
                                                </h4>
                                                <div class="text-nowrap">
                                                    <span class="badge bg-soft-danger text-danger font-size-22">
                                                        <i class=" fa-solid fas fa-female"></i>
                                                    </span>
                                                    <span class="badge bg-soft-primary text-primary font-size-22">
                                                        <i class=" fa-solid fas fa-male"></i>
                                                </div>
                                            </div>
                                            <div class="flex-shrink-0 text-end dash-widget">
                                                <div id="pie-calon-pemilih" data-colors='["#1c84ee", "#ef6767"]'
                                                    class="apex-charts"></div>
                                            </div>
                                        </div>
                                    </div><!-- end card body -->
                                </div><!-- end card -->
                            </div><!-- end col-->

                            <div class="col-xl-3 col-md-6">
                                <!-- card -->
                                <div class="card card-h-100">
                                    <!-- card body -->
                                    <div class="card-body">
                                        <div class="d-flex align-items-center">
                                            <div class="flex-grow-1">
                                                <span class="text-muted mb-3 lh-2 d-block text-truncate">Pengajuan
                                                    Surat</span>
                                                <h4 class="mb-3">
                                                    <span class="counter-value" data-target="7.54">0</span>M
                                                </h4>
                                                <div class="text-nowrap">
                                                    <span class="badge bg-soft-danger text-danger font-size-22">
                                                        <i class=" fa-solid far fa-envelope"></i>
                                                    </span>
                                                    <span class="badge bg-soft-primary text-primary font-size-22">
                                                        <i class=" fa-solid  far fa-envelope-open"></i>
                                                </div>
                                            </div>
                                            <div class="flex-shrink-0 text-end dash-widget">
                                                <div id="pie-pengajuan-surat" data-colors='["#1c84ee", "#ef6767"]'
                                                    class="apex-charts"></div>
                                            </div>
                                        </div>
                                    </div><!-- end card body -->
                                </div><!-- end card -->
                            </div><!-- end col -->

                            <div class="col-xl-3 col-md-6">
                                <!-- card -->
                                <div class="card card-h-100">
                                    <!-- card body -->
                                    <div class="card-body">
                                        <div class="d-flex align-items-center">
                                            <div class="flex-grow-1">
                                                <span class="text-muted mb-3 lh-2 d-block text-truncate">Program Desa</span>
                                                <h4 class="mb-3">
                                                    <span class="counter-value" data-target="18">0</span>%
                                                </h4>
                                                <div class="text-nowrap">
                                                    <span class="badge bg-soft-danger text-danger font-size-22">
                                                        <i class=" fa-solid far fa-envelope"></i>
                                                    </span>
                                                    <span class="badge bg-soft-primary text-primary font-size-22">
                                                        <i class=" fa-solid  far fa-envelope-open"></i>
                                                </div>
                                            </div>
                                            <div class="flex-shrink-0 text-end dash-widget">
                                                <div id="pie-program-desa" data-colors='["#1c84ee", "#33c38e"]'
                                                    class="apex-charts"></div>
                                            </div>
                                        </div>
                                    </div><!-- end card body -->
                                </div><!-- end card -->
                            </div><!-- end col -->
                        </div><!-- end row-->

                        <div class="row">
                            <div class="col-xl-4 col-md-6">
                                <!-- card -->
                                <div class="card card-h-100">
                                    <div class="card-header">
                                        <h4 class="card-title mb-0">Usia Rata-Rata Penduduk</h4>
                                    </div>
                                    <!-- card body -->
                                    <div class="card-body">
                                        <div id="age" class="apex-charts"></div>
                                    </div><!-- end card body -->
                                </div><!-- end card -->
                            </div><!-- end col -->
                            <div class="col-xl-4 col-md-6">
                                <!-- card -->
                                <div class="card card-h-100">
                                    <div class="card-header">
                                        <h4 class="card-title mb-0">Pendidikan Penduduk KK</h4>
                                    </div>
                                    <!-- card body -->
                                    <div class="card-body">
                                        <div id="pendidikan" class="apex-charts"></div>
                                    </div>
                                </div><!-- end card body -->
                            </div><!-- end card -->
                            <div class="col-xl-4 col-md-6">
                                <!-- card -->
                                <div class="card card-h-100">
                                    <div class="card-header">
                                        <h4 class="card-title mb-0">Status Perkawinan Penduduk</h4>
                                    </div>
                                    <!-- card body -->
                                    <div class="card-body">
                                        <div id="perkawinan" class="apex-charts"></div>
                                    </div>
                                </div><!-- end card body -->
                            </div><!-- end card -->
                        </div><!-- end col -->
                    </div>
                </div>
                <div class="carousel-item">
                    <div class="row">
                        <div class="col-xl-12">
                            <!-- card -->
                            <div class="card">
                                <!-- card body -->
                                <div class="card-body">
                                    <div class="d-flex flex-wrap align-items-center mb-4">
                                        <h5 class="card-title me-2">Dana Desa</h5>
                                    </div>
                                    <div class="row align-items-center">
                                        <div class="col-xl-12">
                                            <div>
                                                <div id="dana-desa" data-colors='["#1c84ee", "#33c38e"]'
                                                    class="apex-charts"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- end card -->
                            </div>
                            <!-- end col -->
                        </div>
                        <!-- end row-->

                        <!-- end col -->
                    </div>
                </div>
            </div>
            {{-- <div id="carouselExampleSlidesOnly" class="carousel slide" data-bs-ride="carousel" data-bs-interval="3000">
                <div class="carousel-inner" role="listbox">
                    <div class="carousel-item active">
                        <img class="d-block img-fluid mx-auto" src="{{ URL::asset('assets/images/small/img-1.jpg') }}" alt="First slide">
                    </div>
                    <div class="carousel-item">
                        <img class="d-block img-fluid mx-auto" src="{{ URL::asset('assets/images/small/img-2.jpg') }}" alt="Second slide">
                    </div>
                    <div class="carousel-item">
                        <img class="d-block img-fluid mx-auto" src="{{ URL::asset('assets/images/small/img-3.jpg') }}" alt="Third slide">
                    </div>
                </div> --}}
        </div>
    </div>
    </div>
@endsection
@section('script')
    <script src="{{ URL::asset('assets/libs/apexcharts/apexcharts.min.js') }}"></script>
    <script src="{{ URL::asset('assets/js/pages/dashboard/index.js') }}"></script>
    <script src="{{ URL::asset('assets/js/app.min.js') }}"></script>
@endsection
