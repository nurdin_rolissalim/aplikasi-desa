<!-- ========== Left Sidebar Start ========== -->
<div class="vertical-menu">

    <div data-simplebar class="h-100">

        <!--- Sidemenu -->
        <div id="sidebar-menu">
            <!-- Left Menu Start -->
            <ul class="metismenu list-unstyled" id="side-menu">
                @if (Auth::guard('admin')->check() && session('adminMenus'))
                    @foreach (session('adminMenus') as $item)
                        @if ($item['child'] == null)
                            <li>
                                <a href="{{ route($item['route']) }}">
                                    <i class="{{ $item['icon'] }}"></i>
                                    <span class="badge rounded-pill bg-soft-success text-success float-end">9+</span>
                                    <span data-key="t-{{ $item['path'] }}">{{ $item['name'] }}</span>
                                </a>
                            </li>
                        @else
                            <li>
                                <a href="javascript: void(0);" class="has-arrow">
                                    <i class="{{ $item['icon'] }}"></i>
                                    <span data-key="t-{{ $item['path'] }}">{{ $item['name'] }}</span>
                                </a>
                                <ul class="sub-menu" aria-expanded="false">
                                    @foreach ($item['child'] as $itemChild)
                                        @if ($itemChild['child'] == null)
                                            <li>
                                                <a href="{{ route($itemChild['route']) }}"
                                                    data-key="t-{{ $itemChild['path'] }}">
                                                    {{ $itemChild['name'] }}
                                                </a>
                                            </li>
                                        @else
                                            <li>
                                                <a href="javascript: void(0);" class="has-arrow">
                                                    <span
                                                        data-key="t-{{ $itemChild['path'] }}">{{ $itemChild['name'] }}</span>
                                                </a>
                                                @foreach ($itemChild['child'] as $itemChild2)
                                                    <ul class="sub-menu" aria-expanded="false">
                                                        <li>
                                                            <a href="{{ route($itemChild2['route']) }}"
                                                                data-key="t-{{ $itemChild2['path'] }}">
                                                                {{ $itemChild2['name'] }}
                                                            </a>
                                                        </li>
                                                    </ul>
                                                @endforeach
                                            </li>
                                        @endif
                                    @endforeach
                                </ul>
                            <li>
                        @endif
                    @endforeach
                @elseif (Auth::guard('web')->check() && session('menus'))
                    @foreach (session('menus') as $item)
                        @if ($item['child'] == null)
                            <li>
                                <a href="{{ route($item['route']) }}">
                                    <i class="{{ $item['icon'] }}"></i>
                                    <span class="badge rounded-pill bg-soft-success text-success float-end">9+</span>
                                    <span data-key="t-{{ $item['path'] }}">{{ $item['name'] }}</span>
                                </a>
                            </li>
                        @else
                            <li>
                                <a href="javascript: void(0);" class="has-arrow">
                                    <i class="{{ $item['icon'] }}"></i>
                                    <span data-key="t-{{ $item['path'] }}">{{ $item['name'] }}</span>
                                </a>
                                <ul class="sub-menu" aria-expanded="false">
                                    @foreach ($item['child'] as $itemChild)
                                        @if ($itemChild['child'] == null)
                                            <li>
                                                <a href="{{ route($itemChild['route']) }}"
                                                    data-key="t-{{ $itemChild['path'] }}">
                                                    {{ $itemChild['name'] }}
                                                </a>
                                            </li>
                                        @else
                                            <li>
                                                <a href="javascript: void(0);" class="has-arrow">
                                                    <span
                                                        data-key="t-{{ $itemChild['path'] }}">{{ $itemChild['name'] }}</span>
                                                </a>
                                                @foreach ($itemChild['child'] as $itemChild2)
                                                    <ul class="sub-menu" aria-expanded="false">
                                                        <li>
                                                            <a href="{{ route($itemChild2['route']) }}"
                                                                data-key="t-{{ $itemChild2['path'] }}">
                                                                {{ $itemChild2['name'] }}
                                                            </a>
                                                        </li>
                                                    </ul>
                                                @endforeach
                                            </li>
                                        @endif
                                    @endforeach
                                </ul>
                            <li>
                        @endif
                    @endforeach
                @endif

            </ul>
        </div>
        <!-- Sidebar -->
    </div>
</div>
<!-- Left Sidebar End -->
