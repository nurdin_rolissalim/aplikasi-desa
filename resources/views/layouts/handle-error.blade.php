@if ($errors->any())
    <div class="alert alert-danger alert-border-left alert-dismissible fade show" role="alert">
        {{-- <i class="mdi mdi-block-helper me-3 align-middle"></i> --}}
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
        {{-- <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button> --}}
    </div>
@endif

@if (!empty($success) && $success->any())
    <div class="alert alert-success alert-border-left alert-dismissible fade show" role="alert">
        {{-- <i class="mdi mdi-check-all me-3 align-middle"></i> --}}
        <ul>
            @foreach ($success->all() as $item)
                <li>{{ $item }}</li>
            @endforeach
        </ul>
        {{-- <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button> --}}
    </div>
@endif
