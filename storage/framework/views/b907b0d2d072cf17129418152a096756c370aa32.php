<?php if($errors->any()): ?>
    <div class="alert alert-danger alert-border-left alert-dismissible fade show" role="alert">
        
        <ul>
            <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <li><?php echo e($error); ?></li>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </ul>
        
    </div>
<?php endif; ?>

<?php if(!empty($success) && $success->any()): ?>
    <div class="alert alert-success alert-border-left alert-dismissible fade show" role="alert">
        
        <ul>
            <?php $__currentLoopData = $success->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <li><?php echo e($item); ?></li>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </ul>
        
    </div>
<?php endif; ?>
<?php /**PATH C:\wamp64\www\aplikasi-desa\resources\views/layouts/handle-error.blade.php ENDPATH**/ ?>