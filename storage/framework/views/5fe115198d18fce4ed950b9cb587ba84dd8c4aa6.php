
<?php $__env->startSection('title'); ?>
    Administrator - Menu
<?php $__env->stopSection(); ?>
<?php $__env->startSection('css'); ?>
    <link href="<?php echo e(URL::asset('assets/libs/dropzone/dropzone.min.css')); ?>" rel="stylesheet">
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <?php $__env->startComponent('components.breadcrumb'); ?>
        <?php $__env->slot('li_1'); ?>
            <a href="<?php echo e(route('menu.index')); ?>">Menu</a>
        <?php $__env->endSlot(); ?>
        <?php $__env->slot('title'); ?>
            List Menu
        <?php $__env->endSlot(); ?>
    <?php echo $__env->renderComponent(); ?>

    <div class="row">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body ">
                    <div class="list-data-menu">
                        <?php if($menus['total'] > 0): ?>
                            <?php $__currentLoopData = $menus['data']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <?php if($item['child'] === null): ?>
                                    <div class="list-item-menu border p-2 ms-3 mb-1  "
                                        style="border-left:0.3rem solid #32a1ce !important">
                                        <div class="d-flex justify-content-between">
                                            <div><?php echo e($item['name']); ?></div>
                                            <div class="dropdown align-self-start">
                                                <a class="dropdown-toggle" href="#" role="button"
                                                    data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="bx bx-dots-horizontal-rounded font-size-18 text-dark"></i>
                                                </a>
                                                <div class="dropdown-menu">
                                                    <a class="dropdown-item" href="#">Copy</a>
                                                    <a class="dropdown-item" href="#">Save</a>
                                                    <a class="dropdown-item" href="#">Forward</a>
                                                    <a class="dropdown-item" href="#">Delete</a>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                <?php else: ?>
                                    <div class="list-item-menu">
                                        <div class="list-item-menu border p-2 ms-3 mb-1  "
                                            style="border-left:0.3rem solid #32a1ce !important">
                                            <div class="d-flex justify-content-between">
                                                <div><?php echo e($item['name']); ?></div>
                                                <div class="dropdown align-self-start">
                                                    <a class="dropdown-toggle" href="#" role="button"
                                                        data-bs-toggle="dropdown" aria-haspopup="true"
                                                        aria-expanded="false">
                                                        <i class="bx bx-dots-horizontal-rounded font-size-18 text-dark"></i>
                                                    </a>
                                                    <div class="dropdown-menu">
                                                        <a class="dropdown-item" href="#">Copy</a>
                                                        <a class="dropdown-item" href="#">Save</a>
                                                        <a class="dropdown-item" href="#">Forward</a>
                                                        <a class="dropdown-item" href="#">Delete</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="list-item-menu  ms-3  mb-1">
                                            <?php $__currentLoopData = $item['child']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $itemChild): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <?php if($itemChild['child'] === null): ?>
                                                    <div class="list-item-menu border p-2 ms-3 mb-1  "
                                                        style="border-left:0.3rem solid #32a1ce !important">
                                                        <div class="d-flex justify-content-between">
                                                            <div><?php echo e($itemChild['name']); ?></div>
                                                            <div class="dropdown align-self-start">
                                                                <a class="dropdown-toggle" href="#" role="button"
                                                                    data-bs-toggle="dropdown" aria-haspopup="true"
                                                                    aria-expanded="false">
                                                                    <i
                                                                        class="bx bx-dots-horizontal-rounded font-size-18 text-dark"></i>
                                                                </a>
                                                                <div class="dropdown-menu">
                                                                    <a class="dropdown-item" href="#">Copy</a>
                                                                    <a class="dropdown-item" href="#">Save</a>
                                                                    <a class="dropdown-item" href="#">Forward</a>
                                                                    <a class="dropdown-item" href="#">Delete</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php else: ?>
                                                    <div class="list-item-menu ms-3">
                                                        <div class="list-item-menu border p-2 ms-3 mb-1  "
                                                            style="border-left:0.3rem solid #32a1ce !important">
                                                            <div class="d-flex justify-content-between">
                                                                <div><?php echo e($itemChild['name']); ?></div>
                                                                <div class="dropdown align-self-start">
                                                                    <a class="dropdown-toggle" href="#" role="button"
                                                                        data-bs-toggle="dropdown" aria-haspopup="true"
                                                                        aria-expanded="false">
                                                                        <i
                                                                            class="bx bx-dots-horizontal-rounded font-size-18 text-dark"></i>
                                                                    </a>
                                                                    <div class="dropdown-menu">
                                                                        <a class="dropdown-item" href="#">Copy</a>
                                                                        <a class="dropdown-item" href="#">Save</a>
                                                                        <a class="dropdown-item" href="#">Forward</a>
                                                                        <a class="dropdown-item" href="#">Delete</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="list-item-menu  ms-3  mb-1">
                                                            <?php $__currentLoopData = $itemChild['child']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $itemChild2): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                <?php if($itemChild2['child'] === null): ?>
                                                                    <div class="list-item-menu border p-2 ms-3 mb-1  "
                                                                        style="border-left:0.3rem solid #32a1ce !important">
                                                                        <div class="d-flex justify-content-between">
                                                                            <div><?php echo e($itemChild2['name']); ?></div>
                                                                            <div class="dropdown align-self-start">
                                                                                <a class="dropdown-toggle" href="#"
                                                                                    role="button" data-bs-toggle="dropdown"
                                                                                    aria-haspopup="true"
                                                                                    aria-expanded="false">
                                                                                    <i
                                                                                        class="bx bx-dots-horizontal-rounded font-size-18 text-dark"></i>
                                                                                </a>
                                                                                <div class="dropdown-menu">
                                                                                    <a class="dropdown-item"
                                                                                        href="#">Copy</a>
                                                                                    <a class="dropdown-item"
                                                                                        href="#">Save</a>
                                                                                    <a class="dropdown-item"
                                                                                        href="#">Forward</a>
                                                                                    <a class="dropdown-item"
                                                                                        href="#">Delete</a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                <?php endif; ?>
                                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                        </div>
                                                    </div>
                                                <?php endif; ?>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </div>
                                    </div>
                                <?php endif; ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php endif; ?>

                        

                        
                    </div>
                </div>
                <!-- end card body -->
            </div>
        </div>
        <div class="col-md-4">
            <form class="needs-validation form-validate" novalidate method="post" action=<?php echo e(route('menu.store')); ?>>
                <?php echo csrf_field(); ?>
                <div class="card">
                    <div class="card-header">
                        Form Menu
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-12  mb-3">
                                <label for="name" class="form-label">Display Menu</label>
                                <input type="text" class="form-control" placeholder="Display Menu" id="name"
                                    name="name" required>
                                <div class="invalid-feedback">
                                    Display Menu harus diisi
                                </div>
                            </div>
                            <div class="col-sm-12  mb-3">
                                <label for="path" class="form-label">Path Menu</label>
                                <input type="text" class="form-control" placeholder="Path Menu" id="path"
                                    name="path">
                                <div class="invalid-feedback">
                                    Path Menu harus diisi
                                </div>
                            </div>
                            <div class="col-sm-12  mb-3">
                                <label for="sort" class="form-label">Sort Menu</label>
                                <input type="number" class="form-control" placeholder="Sort Menu" id="sort"
                                    name="sort" required>
                                <div class="invalid-feedback">
                                    Sor Menu harus diisi
                                </div>
                            </div>
                            <div class="col-sm-12  mb-3">
                                <label for="icon" class="form-label">Icon Menu</label>
                                <input type="text" class="form-control" placeholder="Icon Menu" id="icon"
                                    name="icon">
                                <div class="invalid-feedback">
                                    Icon Menu harus diisi
                                </div>
                            </div>
                            <div class="col-sm-12  mb-3">
                                <label for="parent_id" class="form-label">Parent Menu</label>
                                <select class="form-select" id="parent_id" name="parent_id">
                                    <option value="">Pilih Parent Menu</option>
                                    <?php if($menus['total'] > 0): ?>
                                        <?php $__currentLoopData = $menus['data']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($item['id']); ?>"><?php echo e($item['name']); ?></option>
                                            <?php if($item['child'] != null): ?>
                                                <?php $__currentLoopData = $item['child']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $itemChild): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option value="<?php echo e($itemChild['id']); ?>">-<?php echo e($itemChild['name']); ?>

                                                    </option>
                                                    <?php if($itemChild['child'] != null): ?>
                                                        <?php $__currentLoopData = $itemChild['child']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $itemChild2): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <option value="<?php echo e($itemChild2['id']); ?>">
                                                                --<?php echo e($itemChild2['name']); ?></option>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    <?php endif; ?>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            <?php endif; ?>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <?php endif; ?>


                                </select>
                            </div>
                            <div class="col-sm-12  mb-3">
                                <div class="form-group mb-3 inline-label">
                                    <label class="form-label">Hak Akses (required, min=1)</label>
                                    <div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" value="1" id="view"
                                                type="checkbox" min="1" name="view" required
                                                data-pristine-min-message="Select at least 1" />
                                            <label class="form-check-label" for="view">View</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" value="1" id="tambah"
                                                type="checkbox" min="1" name="add"
                                                data-pristine-min-message="Select at least 1" />
                                            <label class="form-check-label" for="tambah">Tambah</label><br />
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" value="1" id="update"
                                                type="checkbox" min="1" name="update"
                                                data-pristine-min-message="Select at least 1" />
                                            <label class="form-check-label" for="update">Update</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" value="1" id="delete"
                                                type="checkbox" min="1" name="delete"
                                                data-pristine-min-message="Select at least 1" />
                                            <label class="form-check-label" for="delete">Delete</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12  mb-3">
                                <label for="status" class="form-label">Status</label>
                                <div class="form-check mb-3">
                                    <input class="form-check-input" type="radio" name="status" value="1"
                                        checked>
                                    <label class="form-check-label" for="laki-laki">
                                        Aktif
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="status" value="0">
                                    <label class="form-check-label" for="status">
                                        TIdak Aktif
                                    </label>
                                </div>
                            </div>
                        </div>


                    </div>
                    <div class="card-footer bg-transparent">
                        <button class="btn btn-primary" type="submit">Simpan</button>
                    </div>
                    <!-- end card body -->
                </div>
            </form>
        </div>
    </div>
    <!-- end col -->
    <!-- end row -->
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
    <script src="<?php echo e(URL::asset('assets/libs/dropzone/dropzone.min.js')); ?>"></script>
    <script src="<?php echo e(URL::asset('assets/libs/pristinejs/pristinejs.min.js')); ?>"></script>
    <script src="<?php echo e(URL::asset('js/pages/administrator/menu/index.js')); ?>"></script>
    <script src="<?php echo e(URL::asset('assets/js/pages/form-validation.init.js')); ?>"></script>
    <script src="<?php echo e(URL::asset('/assets/js/app.min.js')); ?>"></script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\wamp64\www\aplikasi-desa\resources\views/administrator/menu/index.blade.php ENDPATH**/ ?>