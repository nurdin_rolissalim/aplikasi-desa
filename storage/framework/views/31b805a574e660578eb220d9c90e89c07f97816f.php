
<?php $__env->startSection('title'); ?>
    Info Desa - Identitas Desa
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <?php $__env->startComponent('components.breadcrumb'); ?>
        <?php $__env->slot('li_1'); ?>
            Info Desa
        <?php $__env->endSlot(); ?>
        <?php $__env->slot('title'); ?>
            <a href="<?php echo e(route('identitasDesa.index')); ?>"> Identitas Desa</a>
        <?php $__env->endSlot(); ?>
    <?php echo $__env->renderComponent(); ?>

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    
                    <div class="table-responsive">
                        <div>
                            <a href="<?php echo e(route('identitasDesa.edit')); ?>"
                                class="btn btn-warning waves-effect btn-label waves-light">
                                <i class="bx bx-pencil label-icon"></i> Ubah Data Desa
                            </a>
                            <button type="button" class="btn btn-primary waves-effect btn-label waves-light">
                                <i class="bx bx-map label-icon"></i> Primary
                            </button>
                            <button type="button" class="btn btn-primary waves-effect btn-label waves-light">
                                <i class="bx bx-feather label-icon"></i> Peta WIlayah Desa
                            </button>
                        </div>
                        <div>
                            image
                        </div>
                        <table class="table table-striped table-sm m-0">
                            <thead class="table-info">
                                <tr>
                                    <th colspan="2">Desa</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th><span class="co-name">Nama Desa</span> </th>
                                    <td>Cililin</td>
                                </tr>
                                <tr>
                                    <th><span class="co-name">Kode Desa</span> </th>
                                    <td>Cililin</td>
                                </tr>

                                <tr>
                                    <th><span class="co-name">Kode Pos Desa</span> </th>
                                    <td>Cililin</td>
                                </tr>

                            </tbody>
                        </table>
                        <table class="table table-striped table-sm m-0 mt-1">
                            <thead class="table-info">
                                <tr>
                                    <th colspan="2">Kecamatan</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th><span class="co-name">Nama Desa</span> </th>
                                    <td>Cililin</td>
                                </tr>
                                <tr>
                                    <th><span class="co-name">Kode Desa</span> </th>
                                    <td>Cililin</td>
                                </tr>

                                <tr>
                                    <th><span class="co-name">Kode Pos Desa</span> </th>
                                    <td>Cililin</td>
                                </tr>

                            </tbody>
                        </table>
                        <table class="table table-striped table-sm m-0 mt-1">
                            <thead class="table-info">
                                <tr>
                                    <th colspan="2">Kabupten</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th><span class="co-name">Nama Desa</span> </th>
                                    <td>Cililin</td>
                                </tr>
                                <tr>
                                    <th><span class="co-name">Kode Desa</span> </th>
                                    <td>Cililin</td>
                                </tr>

                                <tr>
                                    <th><span class="co-name">Kode Pos Desa</span> </th>
                                    <td>Cililin</td>
                                </tr>

                            </tbody>
                        </table>
                        <table class="table table-striped table-sm m-0 mt-1">
                            <thead class="table-info">
                                <tr>
                                    <th colspan="2">Provinsi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th><span class="co-name">Nama Desa</span> </th>
                                    <td>Cililin</td>
                                </tr>
                                <tr>
                                    <th><span class="co-name">Kode Desa</span> </th>
                                    <td>Cililin</td>
                                </tr>

                                <tr>
                                    <th><span class="co-name">Kode Pos Desa</span> </th>
                                    <td>Cililin</td>
                                </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- end card body -->
            </div>
        </div>
    </div>
    <!-- end col -->
    <!-- end row -->
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
    <script src="<?php echo e(URL::asset('/assets/js/app.min.js')); ?>"></script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\wamp64\www\aplikasi-desa\resources\views/info-desa/identitas-desa/index.blade.php ENDPATH**/ ?>