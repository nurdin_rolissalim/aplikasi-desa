
<?php $__env->startSection('title'); ?>
    Kependudukan - Form Penduduk
<?php $__env->stopSection(); ?>
<?php $__env->startSection('css'); ?>
    <link href="<?php echo e(URL::asset('assets/libs/dropzone/dropzone.min.css')); ?>" rel="stylesheet">
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <?php $__env->startComponent('components.breadcrumb'); ?>
        <?php $__env->slot('li_1'); ?>
            <a href="<?php echo e(route('penduduk.index')); ?>"> Penduduk</a>
        <?php $__env->endSlot(); ?>
        <?php $__env->slot('title'); ?>
            Form Penduduk
        <?php $__env->endSlot(); ?>
    <?php echo $__env->renderComponent(); ?>

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div>
                        <a href="<?php echo e(route('daftarPersyaratan.index')); ?>"
                            class="btn btn-primary waves-effect btn-label waves-light">
                            <i class="bx bx-plus label-icon"></i> Kembali ke Daftar Persyaratan
                        </a>
                    </div>
                </div>
                <div class="card-body">
                    <form class="needs-validation" novalidate>
                        <div class="row mb-3">
                            <label for="nama_dokumen" class="col-sm-3 col-form-label">Nama Dokumen</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" placeholder="Nama Dokumen" id="nama_dokumen"
                                    name="nama_dokumen" required>
                                <div class="invalid-feedback">
                                    Nama Dokumen harus diisi
                                </div>
                                <button class="btn btn-primary mt-3" type="submit">Simpan</button>
                            </div>
                        </div>                       
                    </form>
                </div>
                <!-- end card body -->
            </div>
        </div>
    </div>
    <!-- end col -->
    <!-- end row -->
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
    <script src="<?php echo e(URL::asset('assets/libs/dropzone/dropzone.min.js')); ?>"></script>
    <script src="<?php echo e(URL::asset('assets/js/pages/form-validation.init.js')); ?>"></script>
    <script src="<?php echo e(URL::asset('/assets/js/app.min.js')); ?>"></script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\wamp64\www\aplikasi-desa\resources\views/layanan-surat/daftar-persyaratan/form.blade.php ENDPATH**/ ?>