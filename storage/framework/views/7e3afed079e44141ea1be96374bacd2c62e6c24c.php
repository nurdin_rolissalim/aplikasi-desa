
<?php $__env->startSection('title'); ?>
    Ganti PIN
<?php $__env->stopSection(); ?>
<?php $__env->startSection('css'); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <div class="row">
        <div class="col-xl-3">
            <?php echo $__env->make('layouts.master-member-profile', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            <!-- end card -->
        </div>
        <!-- end col -->

        <div class="col-xl-9">
            <div class="card">
                <div class="card-header">
                    <h4>Ganti PIN</h4>
                </div>
                <div class="card-body">
                    <form class="needs-validation" novalidate>
                       
                        <div class="row mb-3">
                            <label for="no_hp" class="col-sm-3 col-form-label">PIN Lama</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" placeholder="NO HP " id="no_hp"
                                    name="no_hp" required>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="no_hp" class="col-sm-3 col-form-label">PIN Baru</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" placeholder="NO HP " id="no_hp"
                                    name="no_hp" required>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="no_hp" class="col-sm-3 col-form-label">Konfirmasi PIN Baru</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" placeholder="NO HP " id="no_hp"
                                    name="no_hp" required>
                            </div>
                        </div>
                       
                        <div class="row mb-3">
                            <label for="no_hp" class="col-sm-3 col-form-label"></label>
                            <div class="col-sm-9">
                                <button class="btn btn-primary mt-3" type="submit">Simpan</button>

                            </div>
                        </div>

                    </form>
                </div>
                <!-- end card body -->
            </div>
            <!-- end card -->
        </div>
        <!-- end col -->

        <!-- end col -->
    </div><!-- end row -->
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
    <script src="<?php echo e(URL::asset('/assets/js/app.min.js')); ?>"></script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.master-layout-member', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\wamp64\www\aplikasi-desa\resources\views/member/profile/ganti-pin.blade.php ENDPATH**/ ?>