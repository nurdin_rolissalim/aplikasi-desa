<div class="card">
    <div class="card-body">
        <div class="" style="height: 10rem">
            <img src="./assets/images/product/img-1.png" alt="" class="image-object-fit">
        </div>
    </div>
    <div class="card-footer d-grid gap-2">
        <button type="button" class="btn btn-success waves-effect waves-light">
            <i class="bx bx-download font-size-16 align-middle me-2"></i> Cetak Biodata
        </button>
        <button type="button" class="btn btn-primary waves-effect waves-light">
            <i class="bx bx-download font-size-16 align-middle me-2"></i> Cetak KK
        </button>
        <a href="<?php echo e(route('profileMember.pin')); ?>" class="btn btn-warning waves-effect waves-light">
            <i class="bx bx-pencil font-size-16 align-middle me-2"></i> Ubah PIN
        </a>
        <a href="<?php echo e(route('logout')); ?>" class="btn btn-danger waves-effect waves-light">
            <i class="bx bx-power-off font-size-16 align-middle me-2"></i> Logout
        </a>
    </div>
</div>
<?php /**PATH C:\wamp64\www\aplikasi-desa\resources\views/layouts/master-member-profile.blade.php ENDPATH**/ ?>