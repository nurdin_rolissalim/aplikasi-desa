<?php $__env->startSection('title'); ?>
    <?php echo app('translator')->get('translation.Dashboards'); ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('css'); ?>
    <link href="<?php echo e(URL::asset('/assets/libs/admin-resources/admin-resources.min.css')); ?>" rel="stylesheet">
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <?php $__env->startComponent('components.breadcrumb'); ?>
        <?php $__env->slot('li_1'); ?>
            Dashboard
        <?php $__env->endSlot(); ?>
        <?php $__env->slot('title'); ?>
            Welcome !
        <?php $__env->endSlot(); ?>
    <?php echo $__env->renderComponent(); ?>

    <div class="row">
        <div class="col-xl-3 col-md-6">
            <!-- card -->
            <div class="card card-h-100">
                <!-- card body -->
                <div class="card-body">
                    <div class="d-flex align-items-center">
                        <div class="flex-grow-1">
                            <span class="text-muted mb-3 lh-2 d-block text-truncate">Total Penduduk</span>
                            <h4 class="mb-3">
                                <span class="counter-value" data-target="354">0</span>k
                            </h4>
                            <div class="text-nowrap">
                                <span class="badge bg-soft-danger text-danger font-size-22">
                                    <i class=" fa-solid fas fa-female"></i>
                                </span>
                                <span class="badge bg-soft-primary text-primary font-size-22">
                                    <i class=" fa-solid fas fa-male"></i>
                            </div>
                        </div>

                        <div class="flex-shrink-0 text-end dash-widget">
                            <div id="pie-penduduk" data-colors='["#1c84ee", "#ef6767"]' class="apex-charts"></div>
                        </div>
                    </div>
                </div><!-- end card body -->
            </div><!-- end card -->
        </div><!-- end col -->

        <div class="col-xl-3 col-md-6">
            <!-- card -->
            <div class="card card-h-100">
                <!-- card body -->
                <div class="card-body">
                    <div class="d-flex align-items-center">
                        <div class="flex-grow-1">
                            <span class="text-muted mb-3 lh-2 d-block text-truncate">Calon Pemilih</span>
                            <h4 class="mb-3">
                                <span class="counter-value" data-target="1256">0</span>
                            </h4>
                            <div class="text-nowrap">
                                <span class="badge bg-soft-danger text-danger font-size-22">
                                    <i class=" fa-solid fas fa-female"></i>
                                </span>
                                <span class="badge bg-soft-primary text-primary font-size-22">
                                    <i class=" fa-solid fas fa-male"></i>
                            </div>
                        </div>
                        <div class="flex-shrink-0 text-end dash-widget">
                            <div id="pie-calon-pemilih" data-colors='["#1c84ee", "#ef6767"]' class="apex-charts"></div>
                        </div>
                    </div>
                </div><!-- end card body -->
            </div><!-- end card -->
        </div><!-- end col-->

        <div class="col-xl-3 col-md-6">
            <!-- card -->
            <div class="card card-h-100">
                <!-- card body -->
                <div class="card-body">
                    <div class="d-flex align-items-center">
                        <div class="flex-grow-1">
                            <span class="text-muted mb-3 lh-2 d-block text-truncate">Pengajuan Surat</span>
                            <h4 class="mb-3">
                                <span class="counter-value" data-target="7.54">0</span>M
                            </h4>
                            <div class="text-nowrap">
                                <span class="badge bg-soft-danger text-danger font-size-22">
                                    <i class=" fa-solid far fa-envelope"></i>
                                </span>
                                <span class="badge bg-soft-primary text-primary font-size-22">
                                    <i class=" fa-solid  far fa-envelope-open"></i>
                            </div>
                        </div>
                        <div class="flex-shrink-0 text-end dash-widget">
                            <div id="pie-pengajuan-surat" data-colors='["#1c84ee", "#ef6767"]' class="apex-charts"></div>
                        </div>
                    </div>
                </div><!-- end card body -->
            </div><!-- end card -->
        </div><!-- end col -->

        <div class="col-xl-3 col-md-6">
            <!-- card -->
            <div class="card card-h-100">
                <!-- card body -->
                <div class="card-body">
                    <div class="d-flex align-items-center">
                        <div class="flex-grow-1">
                            <span class="text-muted mb-3 lh-2 d-block text-truncate">Program Desa</span>
                            <h4 class="mb-3">
                                <span class="counter-value" data-target="18">0</span>%
                            </h4>
                            <div class="text-nowrap">
                                <span class="badge bg-soft-danger text-danger font-size-22">
                                    <i class=" fa-solid far fa-envelope"></i>
                                </span>
                                <span class="badge bg-soft-primary text-primary font-size-22">
                                    <i class=" fa-solid  far fa-envelope-open"></i>
                            </div>
                        </div>
                        <div class="flex-shrink-0 text-end dash-widget">
                            <div id="pie-program-desa" data-colors='["#1c84ee", "#33c38e"]' class="apex-charts"></div>
                        </div>
                    </div>
                </div><!-- end card body -->
            </div><!-- end card -->
        </div><!-- end col -->
    </div><!-- end row-->

    <div class="row">
        <div class="col-xl-3 col-md-6">
            <!-- card -->
            <div class="card card-h-100">
                <div class="card-header">
                    <h4 class="card-title mb-0">Usia Rata-Rata Penduduk</h4>
                </div>
                <!-- card body -->
                <div class="card-body">
                    <div id="age" class="apex-charts"></div>
                </div><!-- end card body -->
            </div><!-- end card -->
        </div><!-- end col -->
        <div class="col-xl-3 col-md-6">
            <!-- card -->
            <div class="card card-h-100">
                <div class="card-header">
                    <h4 class="card-title mb-0">Pendidikan Penduduk KK</h4>
                </div>
                <!-- card body -->
                <div class="card-body">
                    <div id="pendidikan" class="apex-charts"></div>
                </div>
            </div><!-- end card body -->
        </div><!-- end card -->
        <div class="col-xl-3 col-md-6">
            <!-- card -->
            <div class="card card-h-100">
                <div class="card-header">
                    <h4 class="card-title mb-0">Status Perkawinan Penduduk</h4>
                </div>
                <!-- card body -->
                <div class="card-body">
                    <div id="perkawinan" class="apex-charts"></div>
                </div>
            </div><!-- end card body -->
        </div><!-- end card -->
        <div class="col-xl-3 col-md-6">
            <!-- card -->
            <div class="card card-h-100">
                <div class="card-header">
                    <h4 class="card-title mb-0">Penduduk Yang Cacat</h4>
                </div>
                <!-- card body -->
                <div class="card-body">
                    <div id="cacat" class="apex-charts"></div>
                </div>
            </div><!-- end card body -->
        </div><!-- end card -->
    </div><!-- end col -->
    </div>


    <!-- end row-->
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
    <!-- apexcharts -->
    <script src="<?php echo e(URL::asset('/assets/libs/apexcharts/apexcharts.min.js')); ?>"></script>
    <script src="<?php echo e(URL::asset('/assets/libs/admin-resources/admin-resources.min.js')); ?>"></script>

    <!-- dashboard init -->
    <script src="<?php echo e(URL::asset('/assets/js/pages/dashboard.init.js')); ?>"></script>
    <script src="<?php echo e(URL::asset('/assets/js/app.min.js')); ?>"></script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\wamp64\www\aplikasi-desa\resources\views/index.blade.php ENDPATH**/ ?>