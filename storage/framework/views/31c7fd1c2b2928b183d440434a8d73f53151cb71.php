
<?php $__env->startSection('title'); ?>
    Administrator - Hak Akses
<?php $__env->stopSection(); ?>
<?php $__env->startSection('css'); ?>
    <link href="<?php echo e(URL::asset('assets/libs/dropzone/dropzone.min.css')); ?>" rel="stylesheet">
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <?php $__env->startComponent('components.breadcrumb'); ?>
        <?php $__env->slot('li_1'); ?>
            <a href="<?php echo e(route('role.index')); ?>">Hak Akses</a>
        <?php $__env->endSlot(); ?>
        <?php $__env->slot('title'); ?>
            Setting Hak Akses <?php echo e($data->name); ?>

        <?php $__env->endSlot(); ?>
    <?php echo $__env->renderComponent(); ?>

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <?php if($errors->any()): ?>
                        <div class="alert alert-danger">
                            <ul>
                                <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <li><?php echo e($error); ?></li>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </ul>
                        </div>
                    <?php endif; ?>
                    <form method="post" class="needs-validation form-validate" novalidate action=<?php echo e($route); ?>>
                        <?php echo csrf_field(); ?>

                        <input type="hidden" value="<?php echo e($data->id ?? ''); ?>" id="id" name="id">
                        <div class="list-data-menu">
                            <?php if($menus['total'] > 0): ?>
                                <?php $__currentLoopData = $menus['data']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <?php if($item['child'] === null): ?>
                                        <div class="list-item-menu border p-2 ms-3 mb-1  "
                                            style="border-left:0.3rem solid #32a1ce !important">
                                            <div class="d-flex justify-content-between">
                                                <div><?php echo e($item['name']); ?>

                                                    <input class="ms-1 role_menu form-check-input" type="checkbox"
                                                        name="<?php echo e($item['id']); ?>[]" value="all"
                                                        <?php echo e(!empty($permission && $permission[$item['id']] && array_search('all', $permission[$item['id']]) > -1) ? 'checked' : ''); ?>

                                                        data-item-id="<?php echo e($item['id']); ?>" id="all_<?php echo e($item['id']); ?>">
                                                    <label class=" form-check-label" for="all_<?php echo e($item['id']); ?>">
                                                        All
                                                    </label>
                                                </div>
                                                <div class="d-flex flex-row">
                                                    <!-- item-->
                                                    <div class="ms-1">
                                                        <input class="ms-1 role_menu_checked form-check-input"
                                                            type="checkbox" id="view_<?php echo e($item['id']); ?>"
                                                            name="<?php echo e($item['id']); ?>[]"
                                                            <?php echo e(!empty($permission && $permission[$item['id']] && array_search('view', $permission[$item['id']]) > -1) ? 'checked' : ''); ?>

                                                            data-checked-parent="<?php echo e($item['id']); ?>" value="view">
                                                        <label class=" form-check-label" for="view_<?php echo e($item['id']); ?>">
                                                            View
                                                        </label>
                                                    </div>

                                                    <div class="ms-1">
                                                        <input class="ms-1 form-check-input role_menu_checked"
                                                            type="checkbox" id="add_<?php echo e($item['id']); ?>"
                                                            name="<?php echo e($item['id']); ?>[]"
                                                            <?php echo e(!empty($permission && $permission[$item['id']] && array_search('add', $permission[$item['id']]) > -1) ? 'checked' : ''); ?>

                                                            data-checked-parent="<?php echo e($item['id']); ?>" value="add">
                                                        <label class=" form-check-label" for="add_<?php echo e($item['id']); ?>">
                                                            Tambah
                                                        </label>
                                                    </div>
                                                    <div class="ms-1">
                                                        <input class="ms-1 form-check-input role_menu_checked"
                                                            type="checkbox" id="edit_<?php echo e($item['id']); ?>"
                                                            name="<?php echo e($item['id']); ?>[]"
                                                            <?php echo e(!empty($permission && $permission[$item['id']] && array_search('edit', $permission[$item['id']]) > -1) ? 'checked' : ''); ?>

                                                            data-checked-parent="<?php echo e($item['id']); ?>" value="edit">
                                                        <label class=" form-check-label" for="edit_<?php echo e($item['id']); ?>">
                                                            Edit
                                                        </label>
                                                    </div>
                                                    <div class="ms-1">
                                                        <input class="ms-1 form-check-input role_menu_checked"
                                                            type="checkbox" id="delete_<?php echo e($item['id']); ?>"
                                                            name="<?php echo e($item['id']); ?>[]"
                                                            <?php echo e(!empty($permission && $permission[$item['id']] && array_search('delete', $permission[$item['id']]) > -1) ? 'checked' : ''); ?>

                                                            data-checked-parent="<?php echo e($item['id']); ?>" value="delete">
                                                        <label class=" form-check-label" for="delete_<?php echo e($item['id']); ?>">
                                                            Delete
                                                        </label>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    <?php else: ?>
                                        <div class="list-item-menu">
                                            <div class="list-item-menu border p-2 ms-3 mb-1  "
                                                style="border-left:0.3rem solid #32a1ce !important">
                                                <div class="d-flex justify-content-between">
                                                    <div><?php echo e($item['name']); ?>

                                                        <input class="ms-1 role_menu form-check-input" type="checkbox"
                                                            value="all" id="all_<?php echo e($item['id']); ?>"
                                                            data-item-id="<?php echo e($item['id']); ?>"
                                                            <?php echo e(!empty($permission && $permission[$item['id']] && array_search('all', $permission[$item['id']]) > -1) ? 'checked' : ''); ?>

                                                            name="<?php echo e($item['id']); ?>[]">
                                                        <label class=" form-check-label" for="all_<?php echo e($item['id']); ?>">
                                                            All
                                                        </label>
                                                    </div>
                                                    <div class="d-flex flex-row">
                                                        <!-- item-->
                                                        <div class="ms-1">
                                                            <input class="ms-1 role_menu_checked form-check-input"
                                                                type="checkbox" id="view_<?php echo e($item['id']); ?>"
                                                                <?php echo e(!empty($permission && $permission[$item['id']] && array_search('view', $permission[$item['id']]) > -1) ? 'checked' : ''); ?>

                                                                name="<?php echo e($item['id']); ?>[]" value="view"
                                                                data-checked-parent="<?php echo e($item['id']); ?>">
                                                            <label class=" form-check-label"
                                                                for="view_<?php echo e($item['id']); ?>">
                                                                View
                                                            </label>
                                                        </div>

                                                        <div class="ms-1">
                                                            <input class="ms-1 form-check-input role_menu_checked"
                                                                type="checkbox" id="add_<?php echo e($item['id']); ?>"
                                                                name="<?php echo e($item['id']); ?>[]" value="add"
                                                                <?php echo e(!empty($permission && $permission[$item['id']] && array_search('add', $permission[$item['id']]) > -1) ? 'checked' : ''); ?>

                                                                data-checked-parent="<?php echo e($item['id']); ?>">
                                                            <label class=" form-check-label" for="add_<?php echo e($item['id']); ?>">
                                                                Tambah
                                                            </label>
                                                        </div>
                                                        <div class="ms-1">
                                                            <input class="ms-1 form-check-input role_menu_checked"
                                                                type="checkbox" id="edit_<?php echo e($item['id']); ?>"
                                                                name="<?php echo e($item['id']); ?>[]" value="edit"
                                                                <?php echo e(!empty($permission && $permission[$item['id']] && array_search('edit', $permission[$item['id']]) > -1) ? 'checked' : ''); ?>

                                                                data-checked-parent="<?php echo e($item['id']); ?>">
                                                            <label class=" form-check-label"
                                                                for="edit_<?php echo e($item['id']); ?>">
                                                                Edit
                                                            </label>
                                                        </div>
                                                        <div class="ms-1">
                                                            <input class="ms-1 form-check-input role_menu_checked"
                                                                type="checkbox" id="delete_<?php echo e($item['id']); ?>"
                                                                name="<?php echo e($item['id']); ?>[]" value="delete"
                                                                <?php echo e(!empty($permission && $permission[$item['id']] && array_search('delete', $permission[$item['id']]) > -1) ? 'checked' : ''); ?>

                                                                data-checked-parent="<?php echo e($item['id']); ?>">
                                                            <label class=" form-check-label"
                                                                for="delete_<?php echo e($item['id']); ?>">
                                                                Delete
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="list-item-menu  ms-3  mb-1">
                                                <?php $__currentLoopData = $item['child']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $itemChild): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <?php if($itemChild['child'] === null): ?>
                                                        <div class="list-item-menu border p-2 ms-3 mb-1  "
                                                            style="border-left:0.3rem solid #32a1ce !important">
                                                            <div class="d-flex justify-content-between">
                                                                <div><?php echo e($itemChild['name']); ?>

                                                                    <input class="ms-1 role_menu form-check-input"
                                                                        type="checkbox" value="all"
                                                                        <?php echo e(!empty($permission && $permission[$itemChild['id']] && array_search('all', $permission[$itemChild['id']]) > -1) ? 'checked' : ''); ?>

                                                                        data-item-id="<?php echo e($itemChild['id']); ?>"
                                                                        name="<?php echo e($itemChild['id']); ?>[]"
                                                                        id="all_<?php echo e($itemChild['id']); ?>">
                                                                    <label class=" form-check-label"
                                                                        for="all_<?php echo e($itemChild['id']); ?>">
                                                                        All
                                                                    </label>
                                                                </div>
                                                                <div class="d-flex flex-row">
                                                                    <!-- item-->
                                                                    <div class="ms-1">
                                                                        <input
                                                                            class="ms-1 role_menu_checked form-check-input"
                                                                            type="checkbox"
                                                                            <?php echo e(!empty($permission && $permission[$itemChild['id']] && array_search('view', $permission[$itemChild['id']]) > -1) ? 'checked' : ''); ?>

                                                                            id="view_<?php echo e($itemChild['id']); ?>"
                                                                            name="<?php echo e($itemChild['id']); ?>[]"
                                                                            value="view"
                                                                            data-checked-parent="<?php echo e($itemChild['id']); ?>">
                                                                        <label class=" form-check-label"
                                                                            for="view_<?php echo e($itemChild['id']); ?>">
                                                                            View
                                                                        </label>
                                                                    </div>

                                                                    <div class="ms-1">
                                                                        <input
                                                                            class="ms-1 form-check-input role_menu_checked"
                                                                            type="checkbox"
                                                                            <?php echo e(!empty($permission && $permission[$itemChild['id']] && array_search('add', $permission[$itemChild['id']]) > -1) ? 'checked' : ''); ?>

                                                                            id="add_<?php echo e($itemChild['id']); ?>"
                                                                            name="<?php echo e($itemChild['id']); ?>[]"
                                                                            value="add"
                                                                            data-checked-parent="<?php echo e($itemChild['id']); ?>">
                                                                        <label class=" form-check-label"
                                                                            for="add_<?php echo e($itemChild['id']); ?>">
                                                                            Tambah
                                                                        </label>
                                                                    </div>
                                                                    <div class="ms-1">
                                                                        <input
                                                                            class="ms-1 form-check-input role_menu_checked"
                                                                            type="checkbox"
                                                                            <?php echo e(!empty($permission && $permission[$itemChild['id']] && array_search('edit', $permission[$itemChild['id']]) > -1) ? 'checked' : ''); ?>

                                                                            id="edit_<?php echo e($itemChild['id']); ?>"
                                                                            name="<?php echo e($itemChild['id']); ?>[]"
                                                                            value="edit"
                                                                            data-checked-parent="<?php echo e($itemChild['id']); ?>">
                                                                        <label class=" form-check-label"
                                                                            for="edit_<?php echo e($itemChild['id']); ?>">
                                                                            Edit
                                                                        </label>
                                                                    </div>
                                                                    <div class="ms-1">
                                                                        <input
                                                                            class="ms-1 form-check-input role_menu_checked"
                                                                            type="checkbox"
                                                                            <?php echo e(!empty($permission && $permission[$itemChild['id']] && array_search('delete', $permission[$itemChild['id']]) > -1) ? 'checked' : ''); ?>

                                                                            id="delete_<?php echo e($itemChild['id']); ?>"
                                                                            name="<?php echo e($itemChild['id']); ?>[]"
                                                                            value="delete"
                                                                            data-checked-parent="<?php echo e($itemChild['id']); ?>">
                                                                        <label class=" form-check-label"
                                                                            for="delete_<?php echo e($itemChild['id']); ?>">
                                                                            Delete
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    <?php else: ?>
                                                        <div class="list-item-menu ms-3">
                                                            <div class="list-item-menu border p-2 ms-3 mb-1  "
                                                                style="border-left:0.3rem solid #32a1ce !important">
                                                                <div class="d-flex justify-content-between">
                                                                    <div><?php echo e($itemChild['name']); ?>

                                                                        <input class="ms-1 role_menu form-check-input"
                                                                            type="checkbox" value="all"
                                                                            <?php echo e(!empty($permission && $permission[$itemChild['id']] && array_search('view', $permission[$itemChild['id']]) > -1) ? 'checked' : ''); ?>

                                                                            data-item-id="<?php echo e($itemChild['id']); ?>"
                                                                            name="<?php echo e($itemChild['id']); ?>[]"
                                                                            id="all_<?php echo e($itemChild['id']); ?>">
                                                                        <label class=" form-check-label"
                                                                            for="all_<?php echo e($itemChild['id']); ?>">
                                                                            All
                                                                        </label>
                                                                    </div>
                                                                    <div class="d-flex flex-row">
                                                                        <!-- item-->
                                                                        <div class="ms-1">
                                                                            <input
                                                                                class="ms-1 role_menu_checked form-check-input"
                                                                                type="checkbox"
                                                                                <?php echo e(!empty($permission && $permission[$itemChild['id']] && array_search('all', $permission[$itemChild['id']]) > -1) ? 'checked' : ''); ?>

                                                                                id="view_<?php echo e($itemChild['id']); ?>"
                                                                                name="<?php echo e($itemChild['id']); ?>[]"
                                                                                value="view"
                                                                                data-checked-parent="<?php echo e($itemChild['id']); ?>">
                                                                            <label class=" form-check-label"
                                                                                for="view_<?php echo e($itemChild['id']); ?>">
                                                                                View
                                                                            </label>
                                                                        </div>

                                                                        <div class="ms-1">
                                                                            <input
                                                                                class="ms-1 form-check-input role_menu_checked"
                                                                                type="checkbox" value="add"
                                                                                <?php echo e(!empty($permission && $permission[$itemChild['id']] && array_search('add', $permission[$itemChild['id']]) > -1) ? 'checked' : ''); ?>

                                                                                name="<?php echo e($itemChild['id']); ?>[]}"
                                                                                id="add_<?php echo e($itemChild['id']); ?>"
                                                                                data-checked-parent="<?php echo e($itemChild['id']); ?>">
                                                                            <label class=" form-check-label"
                                                                                for="add_<?php echo e($itemChild['id']); ?>">
                                                                                Tambah
                                                                            </label>
                                                                        </div>
                                                                        <div class="ms-1">
                                                                            <input
                                                                                class="ms-1 form-check-input role_menu_checked"
                                                                                type="checkbox"
                                                                                <?php echo e(!empty($permission && $permission[$itemChild['id']] && array_search('edit', $permission[$itemChild['id']]) > -1) ? 'checked' : ''); ?>

                                                                                id="edit_<?php echo e($itemChild['id']); ?>"
                                                                                name="<?php echo e($itemChild['id']); ?>[]"
                                                                                value="edit"
                                                                                data-checked-parent="<?php echo e($itemChild['id']); ?>">
                                                                            <label class=" form-check-label"
                                                                                for="edit_<?php echo e($itemChild['id']); ?>">
                                                                                Edit
                                                                            </label>
                                                                        </div>
                                                                        <div class="ms-1">
                                                                            <input
                                                                                class="ms-1 form-check-input role_menu_checked"
                                                                                type="checkbox"
                                                                                <?php echo e(!empty($permission && $permission[$itemChild['id']] && array_search('delete', $permission[$itemChild['id']]) > -1) ? 'checked' : ''); ?>

                                                                                id="delete_<?php echo e($itemChild['id']); ?>"
                                                                                name="<?php echo e($itemChild['id']); ?>[]"
                                                                                value="delete"
                                                                                data-checked-parent="<?php echo e($itemChild['id']); ?>">
                                                                            <label class=" form-check-label"
                                                                                for="delete_<?php echo e($itemChild['id']); ?>">
                                                                                Delete
                                                                            </label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="list-item-menu  ms-3  mb-1">
                                                                <?php $__currentLoopData = $itemChild['child']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $itemChild2): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                    <?php if($itemChild2['child'] === null): ?>
                                                                        <div class="list-item-menu border p-2 ms-3 mb-1  "
                                                                            style="border-left:0.3rem solid #32a1ce !important">
                                                                            <div class="d-flex justify-content-between">
                                                                                <div><?php echo e($itemChild2['name']); ?>

                                                                                    <input
                                                                                        <?php echo e(!empty($permission && $permission[$itemChild2['id']] && array_search('all', $permission[$itemChild2['id']]) > -1) ? 'checked' : ''); ?>

                                                                                        class="ms-1 role_menu form-check-input"
                                                                                        type="checkbox" value="all"
                                                                                        data-item-id="<?php echo e($itemChild2['id']); ?>"
                                                                                        name="<?php echo e($itemChild2['id']); ?>[]"
                                                                                        id="all_<?php echo e($itemChild2['id']); ?>">
                                                                                    <label class=" form-check-label"
                                                                                        for="all_<?php echo e($itemChild2['id']); ?>">
                                                                                        All
                                                                                    </label>
                                                                                </div>
                                                                                <div class="d-flex flex-row">
                                                                                    <!-- item-->
                                                                                    <div class="ms-1">
                                                                                        <input
                                                                                            <?php echo e(!empty($permission && $permission[$itemChild2['id']] && array_search('view', $permission[$itemChild2['id']]) > -1) ? 'checked' : ''); ?>

                                                                                            class="ms-1 role_menu_checked form-check-input"
                                                                                            type="checkbox"
                                                                                            id="view_<?php echo e($itemChild2['id']); ?>"
                                                                                            name="<?php echo e($itemChild2['id']); ?>"
                                                                                            data-checked-parent="<?php echo e($itemChild2['id']); ?>">
                                                                                        <label class=" form-check-label"
                                                                                            for="view_<?php echo e($itemChild2['id']); ?>">
                                                                                            View
                                                                                        </label>
                                                                                    </div>

                                                                                    <div class="ms-1">
                                                                                        <input
                                                                                            class="ms-1 form-check-input role_menu_checked"
                                                                                            type="checkbox" value="add"
                                                                                            <?php echo e(!empty($permission && $permission[$itemChild2['id']] && array_search('add', $permission[$itemChild2['id']]) > -1) ? 'checked' : ''); ?>

                                                                                            name="<?php echo e($itemChild2['id']); ?>[]"
                                                                                            id="add_<?php echo e($itemChild2['id']); ?>"
                                                                                            data-checked-parent="<?php echo e($itemChild2['id']); ?>">
                                                                                        <label class=" form-check-label"
                                                                                            for="add_<?php echo e($itemChild2['id']); ?>">
                                                                                            Tambah
                                                                                        </label>
                                                                                    </div>
                                                                                    <div class="ms-1">
                                                                                        <input
                                                                                            class="ms-1 form-check-input role_menu_checked"
                                                                                            type="checkbox"
                                                                                            <?php echo e(!empty($permission && $permission[$itemChild2['id']] && array_search('edit', $permission[$itemChild2['id']]) > -1) ? 'checked' : ''); ?>

                                                                                            id="edit_<?php echo e($itemChild2['id']); ?>"
                                                                                            name="<?php echo e($itemChild2['id']); ?>[]"
                                                                                            value="edit"
                                                                                            data-checked-parent="<?php echo e($itemChild['id']); ?>">
                                                                                        <label class=" form-check-label"
                                                                                            for="edit_<?php echo e($itemChild2['id']); ?>">
                                                                                            Edit
                                                                                        </label>
                                                                                    </div>
                                                                                    <div class="ms-1">
                                                                                        <input
                                                                                            <?php echo e(!empty($permission && $permission[$itemChild2['id']] && array_search('delete', $permission[$itemChild2['id']]) > -1) ? 'checked' : ''); ?>

                                                                                            class="ms-1 form-check-input role_menu_checked"
                                                                                            type="checkbox"
                                                                                            id="delete_<?php echo e($itemChild2['id']); ?>"
                                                                                            name="<?php echo e($itemChild2['id']); ?>[]"
                                                                                            value="delete"
                                                                                            data-checked-parent="<?php echo e($itemChild2['id']); ?>">
                                                                                        <label class=" form-check-label"
                                                                                            for="delete_<?php echo e($itemChild2['id']); ?>">
                                                                                            Delete
                                                                                        </label>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    <?php endif; ?>
                                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                            </div>
                                                        </div>
                                                    <?php endif; ?>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php endif; ?>
                        </div>
                        <button class="btn btn-primary mt-3" type="submit">Simpan</button>
                    </form>
                </div>
                <!-- end card body -->
            </div>
        </div>
    </div>
    <!-- end col -->
    <!-- end row -->
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
    <script src="<?php echo e(URL::asset('assets/libs/dropzone/dropzone.min.js')); ?>"></script>
    <script src="<?php echo e(URL::asset('assets/js/pages/form-validation.init.js')); ?>"></script>
    <script src="<?php echo e(URL::asset('js/pages/administrator/role/privilage.js')); ?>"></script>
    <script src="<?php echo e(URL::asset('/assets/js/app.min.js')); ?>"></script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\wamp64\www\aplikasi-desa\resources\views/administrator/role/privilage-form.blade.php ENDPATH**/ ?>