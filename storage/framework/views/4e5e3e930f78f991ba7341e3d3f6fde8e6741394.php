<!-- ========== Left Sidebar Start ========== -->
<div class="vertical-menu">

    <div data-simplebar class="h-100">

        <!--- Sidemenu -->
        <div id="sidebar-menu">
            <!-- Left Menu Start -->
            <ul class="metismenu list-unstyled" id="side-menu">
                <?php if(Auth::guard('admin')->check() && session('adminMenus')): ?>
                    <?php $__currentLoopData = session('adminMenus'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php if($item['child'] == null): ?>
                            <li>
                                <a href="<?php echo e(route($item['route'])); ?>">
                                    <i class="<?php echo e($item['icon']); ?>"></i>
                                    <span class="badge rounded-pill bg-soft-success text-success float-end">9+</span>
                                    <span data-key="t-<?php echo e($item['path']); ?>"><?php echo e($item['name']); ?></span>
                                </a>
                            </li>
                        <?php else: ?>
                            <li>
                                <a href="javascript: void(0);" class="has-arrow">
                                    <i class="<?php echo e($item['icon']); ?>"></i>
                                    <span data-key="t-<?php echo e($item['path']); ?>"><?php echo e($item['name']); ?></span>
                                </a>
                                <ul class="sub-menu" aria-expanded="false">
                                    <?php $__currentLoopData = $item['child']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $itemChild): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <?php if($itemChild['child'] == null): ?>
                                            <li>
                                                <a href="<?php echo e(route($itemChild['route'])); ?>"
                                                    data-key="t-<?php echo e($itemChild['path']); ?>">
                                                    <?php echo e($itemChild['name']); ?>

                                                </a>
                                            </li>
                                        <?php else: ?>
                                            <li>
                                                <a href="javascript: void(0);" class="has-arrow">
                                                    <span
                                                        data-key="t-<?php echo e($itemChild['path']); ?>"><?php echo e($itemChild['name']); ?></span>
                                                </a>
                                                <?php $__currentLoopData = $itemChild['child']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $itemChild2): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <ul class="sub-menu" aria-expanded="false">
                                                        <li>
                                                            <a href="<?php echo e(route($itemChild2['route'])); ?>"
                                                                data-key="t-<?php echo e($itemChild2['path']); ?>">
                                                                <?php echo e($itemChild2['name']); ?>

                                                            </a>
                                                        </li>
                                                    </ul>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </li>
                                        <?php endif; ?>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </ul>
                            <li>
                        <?php endif; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php elseif(Auth::guard('web')->check() && session('menus')): ?>
                    <?php $__currentLoopData = session('menus'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php if($item['child'] == null): ?>
                            <li>
                                <a href="<?php echo e(route($item['route'])); ?>">
                                    <i class="<?php echo e($item['icon']); ?>"></i>
                                    <span class="badge rounded-pill bg-soft-success text-success float-end">9+</span>
                                    <span data-key="t-<?php echo e($item['path']); ?>"><?php echo e($item['name']); ?></span>
                                </a>
                            </li>
                        <?php else: ?>
                            <li>
                                <a href="javascript: void(0);" class="has-arrow">
                                    <i class="<?php echo e($item['icon']); ?>"></i>
                                    <span data-key="t-<?php echo e($item['path']); ?>"><?php echo e($item['name']); ?></span>
                                </a>
                                <ul class="sub-menu" aria-expanded="false">
                                    <?php $__currentLoopData = $item['child']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $itemChild): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <?php if($itemChild['child'] == null): ?>
                                            <li>
                                                <a href="<?php echo e(route($itemChild['route'])); ?>"
                                                    data-key="t-<?php echo e($itemChild['path']); ?>">
                                                    <?php echo e($itemChild['name']); ?>

                                                </a>
                                            </li>
                                        <?php else: ?>
                                            <li>
                                                <a href="javascript: void(0);" class="has-arrow">
                                                    <span
                                                        data-key="t-<?php echo e($itemChild['path']); ?>"><?php echo e($itemChild['name']); ?></span>
                                                </a>
                                                <?php $__currentLoopData = $itemChild['child']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $itemChild2): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <ul class="sub-menu" aria-expanded="false">
                                                        <li>
                                                            <a href="<?php echo e(route($itemChild2['route'])); ?>"
                                                                data-key="t-<?php echo e($itemChild2['path']); ?>">
                                                                <?php echo e($itemChild2['name']); ?>

                                                            </a>
                                                        </li>
                                                    </ul>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </li>
                                        <?php endif; ?>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </ul>
                            <li>
                        <?php endif; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php endif; ?>

            </ul>
        </div>
        <!-- Sidebar -->
    </div>
</div>
<!-- Left Sidebar End -->
<?php /**PATH C:\wamp64\www\aplikasi-desa\resources\views/layouts/sidebar.blade.php ENDPATH**/ ?>