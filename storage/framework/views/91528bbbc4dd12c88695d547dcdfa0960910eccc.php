<?php echo $__env->yieldContent('css'); ?>
<meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
<!-- preloader css -->
<link rel="stylesheet" href="<?php echo e(URL::asset('assets/css/preloader.min.css')); ?>" type="text/css" />

<!-- Bootstrap Css -->
<link href="<?php echo e(URL::asset('assets/css/bootstrap.min.css')); ?>" id="bootstrap-style" rel="stylesheet" type="text/css" />
<!-- Icons Css -->
<link href="<?php echo e(URL::asset('assets/css/icons.min.css')); ?>" rel="stylesheet" type="text/css" />
<!-- App Css-->


<link href="<?php echo e(URL::asset('assets/css/app.min.css')); ?>" id="app-style" rel="stylesheet" type="text/css" />
<?php /**PATH C:\wamp64\www\aplikasi-desa\resources\views/layouts/head-css.blade.php ENDPATH**/ ?>