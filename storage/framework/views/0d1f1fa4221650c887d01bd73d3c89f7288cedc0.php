
<?php $__env->startSection('title'); ?>
    Administrator - User
<?php $__env->stopSection(); ?>
<?php $__env->startSection('css'); ?>
    <link href="<?php echo e(URL::asset('assets/libs/dropzone/dropzone.min.css')); ?>" rel="stylesheet">
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <?php $__env->startComponent('components.breadcrumb'); ?>
        <?php $__env->slot('li_1'); ?>
            <a href="<?php echo e(route('user.index')); ?>">User</a>
        <?php $__env->endSlot(); ?>
        <?php $__env->slot('title'); ?>
            Tambah Data User
        <?php $__env->endSlot(); ?>
    <?php echo $__env->renderComponent(); ?>

    <div class="row">
        <div class="col-md-4">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex justify-content-center" style="height: 200px">
                        <div class="align-middle">
                            <img id="fileSrcImage" src="<?php echo e($data->avatar ?? ''); ?>" class="img-fluid image-object-fit"
                                alt="..."
                                onerror="this.onerror=null;this.src='<?php echo e(URL::asset('assets/images/no-image.svg')); ?>';">
                        </div>

                    </div>

                </div>
                <!-- end card body -->
            </div>
        </div>
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    <form method="post" enctype="multipart/form-data" class="needs-validation form-validate" novalidate
                        action=<?php echo e($route); ?>>
                        <?php echo csrf_field(); ?>
                        <input type="hidden" value="<?php echo e($data->id ?? ''); ?>" id="id" name="id">
                        <div class="row mb-3">
                            <label for="name" class="col-sm-3 col-form-label">Nama</label>
                            <div class="col-sm-9">
                                <input type="text" value="<?php echo e($data->name ?? ''); ?>" class="form-control"
                                    placeholder="Nama" id="name" name="name" required>
                                <div class="invalid-feedback">
                                    Nama harus diisi
                                </div>
                            </div>
                        </div>
                        
                        <div class="row mb-3">
                            <label for="email" class="col-sm-3 col-form-label">Email</label>
                            <div class="col-sm-9">
                                <input type="email" value="<?php echo e($data->email ?? ''); ?>" class="form-control"
                                    placeholder="Email" id="email" name="email" required>
                                <div class="invalid-feedback">
                                    Email harus diisi dan unique
                                </div>
                            </div>
                        </div>
                        
                        <div class="row mb-3">
                            <label for="role_id" class="col-sm-3 col-form-label">Hak Akses</label>
                            <div class="col-sm-9">
                                <select class="form-select" id="role_id" name="role_id" required>
                                    <option value="">Pilih Hak Akses</option>
                                    <?php if(!empty($roles)): ?>
                                        <?php $__currentLoopData = $roles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($item->id); ?>"
                                                <?php echo e(old('status', $data->role_id) == $item->id ? 'selected' : ''); ?>>
                                                <?php echo e($item->name); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <?php endif; ?>

                                </select>
                                <div class="invalid-feedback">
                                    Hak akses belum dipilih
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="role_id" class="col-sm-3 col-form-label">Image</label>
                            <div class="col-sm-9">
                                <div class="input-group mb-3">
                                    <input class="form-control" type="file" name="file" id="formImageFile"
                                        onchange="preview()" accept="image/x-png,image/jpeg">
                                    <div class="input-group-append" onclick="clearImage()">
                                        <button class="input-group-text" type="button">Clear</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="status" class="col-sm-3 col-form-label">Status</label>
                            <div class="col-sm-9">
                                <div class="form-check mb-3">
                                    <input class="form-check-input" type="radio" name="status" id="active"
                                        value="1"
                                        <?php echo e(!empty($data->status && $data->status == 1) ? 'checked' : ''); ?>>
                                    <label class="form-check-label" for="aktif">
                                        Aktif
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="status" id="inaktive"
                                        value="0"
                                        <?php echo e(!empty($data->status && $data->status == 0) ? 'checked' : ''); ?>>
                                    <label class="form-check-label" for="status">
                                        TIdak Aktif
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="password" class="col-sm-3 col-form-label">Password</label>
                            <div class="col-sm-9">
                                <input type="password" class="form-control" placeholder="Password" id="password"
                                    name="password">
                            </div>
                        </div>
                        <button class="btn btn-primary" type="submit">Simpan</button>
                    </form>
                </div>
                <!-- end card body -->
            </div>
        </div>
    </div>

    <!-- end col -->
    <!-- end row -->
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
    <script src="<?php echo e(URL::asset('assets/js/pages/form-validation.init.js')); ?>"></script>
    <script src="<?php echo e(URL::asset('assets/js/pages/administrator/user/form.js')); ?>"></script>
    <script src="<?php echo e(URL::asset('assets/js/app.min.js')); ?>"></script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\wamp64\www\aplikasi-desa\resources\views/administrator/user/form.blade.php ENDPATH**/ ?>