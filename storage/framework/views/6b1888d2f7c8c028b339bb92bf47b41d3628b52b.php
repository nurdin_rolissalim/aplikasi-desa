<!-- ========== Left Sidebar Start ========== -->
<div class="vertical-menu">

    <div data-simplebar class="h-100">

        <!--- Sidemenu -->
        <div id="sidebar-menu">
            <!-- Left Menu Start -->
            <ul class="metismenu list-unstyled" id="side-menu">
                

                <li>
                    <a href="index">
                        <i data-feather="home"></i>
                        <span class="badge rounded-pill bg-soft-success text-success float-end">9+</span>
                        <span data-key="t-dashboard"><?php echo app('translator')->get('translation.Dashboards'); ?></span>
                    </a>
                </li>



                
                <li>
                    <a href="javascript: void(0);" class="has-arrow">
                        <i data-feather="users"></i>
                        <span data-key="t-desa">Info Desa</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li><a href="<?php echo e(route('identitasDesa.index')); ?>" data-key="t-identitas-desa">Identitas
                                Desa</a>
                        </li>
                        <li><a href="<?php echo e(route('wilayahAdministratif.index')); ?>" data-key="t-wilayah-administratif">
                                Wilayah
                                Administratif
                            </a>
                        </li>
                        <li>
                            <a href="javascript: void(0);" class="has-arrow"
                                data-key="t-pemerintahan-desa">Pemerintahan
                                Desa</a>
                            <ul class="sub-menu" aria-expanded="true">
                                <li>
                                    <a href="<?php echo e(route('bukuPeraturanDesa.index')); ?>"
                                        data-key="t-buku-peraturan-desa">Buku Peraturan
                                        Desa
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript: void(0);" data-key="t-buku-keputusan-kepala">Buku Keputusan
                                        Kepala</a>
                                </li>
                                <li>
                                    <a href="javascript: void(0);" data-key="t-buku-inventaris-kekayaan-desa">Buku
                                        Inventaris dan Kekayaan Desa</a>
                                </li>
                                <li>
                                    <a href="javascript: void(0);" data-key="t-aparat-pemerintah-desa">Buku Aparat
                                        Pemerintah Desa</a>
                                </li>
                                <li>
                                    <a href="javascript: void(0);" data-key="t-buku-tanah-kas-desa">Buku Tanah Kas
                                        Desa</a>
                                </li>
                                <li>
                                    <a href="javascript: void(0);" data-key="t-buku-tanah-desa">Buku Tanah di Desa</a>
                                </li>
                                <li>
                                    <a href="javascript: void(0);" data-key="t-buku-agenda-surat-keluar">Buku Agenda -
                                        Surat Keluar</a>
                                </li>
                                <li>
                                    <a href="javascript: void(0);" data-key="t-buku-agenda-surat-masuk">Buku Agenda -
                                        Surat Masuk</a>
                                </li>
                                <li>
                                    <a href="javascript: void(0);" data-key="t-buku-ekspedisi">Buku Ekspedisi</a>
                                </li>
                                <li>
                                    <a href="javascript: void(0);" data-key="t-buku-ekspedisi">Buku Lembaran Desa dan
                                        Berita Desa</a>
                                </li>
                            </ul>
                        </li>
                        <li><a href="apps-contacts-grid" data-key="t-desa-grid">Status Desa</a></li>
                        <li><a href="apps-contacts-grid" data-key="t-desa-grid">Lembaga Desa</a></li>
                        <li><a href="apps-contacts-grid" data-key="t-desa-grid">Layanan Desa</a></li>
                        <li><a href="apps-contacts-grid" data-key="t-desa-grid">Pendaftaran Kerjasama</a></li>
                    </ul>
                </li>
                <li>
                    <a href="javascript: void(0);" class="has-arrow">
                        <i data-feather="users"></i>
                        <span data-key="t-kependudukan">Kependudukan</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li><a href="<?php echo e(route('penduduk.index')); ?>" data-key="t-penduduk-grid">Penduduk</a></li>
                        <li><a href="<?php echo e(route('keluarga.index')); ?>" data-key="t-keluarga-grid">Keluarga</a></li>
                        <li><a href="<?php echo e(route('rumahTangga.index')); ?>" data-key="t-rumah-tangga-grid">Rumah Tangga</a>
                        </li>
                        <li><a href="apps-contacts-grid" data-key="t-kependudukan-grid">Kelompok</a></li>
                        <li><a href="apps-contacts-grid" data-key="t-kependudukan-grid">Data Suplemen</a></li>
                        <li><a href="<?php echo e(route('calonPemilih.index')); ?>" data-key="t-calon-pemilih-grid">Calon
                                Pemilih</a></li>
                    </ul>
                </li>
                <li>
                    <a href="javascript: void(0);" class="has-arrow">
                        <i data-feather="mail"></i>
                        <span data-key="t-layanan-surat">Layanan Surat</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li><a href="<?php echo e(route('pengaturanSurat.index')); ?>" data-key="t-pengaturan-surat">Pengaturan
                                Surat</a></li>
                        <li><a href="<?php echo e(route('cetakSurat.index')); ?>" data-key="t-layanan-surat-grid">Cetak
                                Surat</a></li>
                        <li><a href="<?php echo e(route('arsipLayanan.index')); ?>" data-key="t-arsip-layanan">Arsip Layanan</a>
                        </li>
                        <li><a href="<?php echo e(route('panduan.index')); ?>" data-key="t-panduan">Panduan</a></li>
                        <li><a href="<?php echo e(route('daftarPersyaratan.index')); ?>" data-key="t-daftar-persyaratan">Daftar
                                Persyaratan</a></li>
                    </ul>
                </li>
                <li>
                    <a href="javascript: void(0);" class="has-arrow">
                        <i data-feather="mail"></i>
                        <span data-key="t-layanan-mandiri">Layanan Mandiri</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li><a href="<?php echo e(route('permohonanSurat.index')); ?>" data-key="t-permohonan-surat">Permohonanan
                                Surat</a></li>
                        <li><a href="<?php echo e(route('kotakPesan.index')); ?>" data-key="t-kotak-pesan">Kotak Pesan</a></li>
                        <li><a href="<?php echo e(route('pendaftarLayananMandiri.index')); ?>"
                                data-key="t-daftar-layanan-mandiri">Pendaftar Layanan Mandiri</a>
                        </li>
                        <li><a href="<?php echo e(route('anjungan.index')); ?>" data-key="t-anjungan">Anjungan</a></li>
                        <li><a href="<?php echo e(route('pendapat.index')); ?>" data-key="t-pendapat">Pendapat</a></li>
                    </ul>
                </li>

            </ul>
        </div>
        <!-- Sidebar -->
    </div>
</div>
<!-- Left Sidebar End -->
<?php /**PATH C:\wamp64\www\template-dason\resources\views/layouts/sidebar.blade.php ENDPATH**/ ?>