<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Upload extends Model
{
    public static function uploadFile($request, $path)
    {
        $fileName = time() . '.' . $request->file->extension();

        $request->file->move(public_path('images/' . $path), $fileName);
        return "/images/" . $path . "/" . $fileName;
    }
}
