<?php

namespace App\Models\Administrator;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Menu extends Model
{
    use HasFactory;

    protected $guarded = ["id"];

    public function scopeActive($query)
    {
        return $query->where("status", 1);
    }

    public function scopeInActive($query)
    {
        return $query->where("status", 0);
    }

    public function scopeFilters($query, array $filters)
    {
        if (isset($filters['keyword'])) {
            $query->where(function ($query) use ($filters) {
                $query->where("name", "LIKE", "%" . $filters['keyword'])
                    ->orWhere("email", "LIKE", "%" . $filters['keyword']);
            });
            return  $query;
        }
    }



    public static function getMenus($type = "web", $parent = NULL, $unik_name = "parent", $number_unix = 0)
    {
        $menu = array();

        $fiturWeb = DB::table('menus as m')
            // ->where("m.tipe", $type)
            ->where("m.parent_id", $parent)
            ->where("m.status", "1")
            ->orderBy('m.sort')
            ->get()
            ->toArray();
        // echo json_encode($fiturWeb);
        // exit;
        foreach ($fiturWeb as $key => $value) {
            $number_unix++;
            // print_r($value->name);exit;
            $rows = self::getMenus(
                $type,
                $value->id,
                str_replace(" ", "_", $value->name),
                $number_unix
            );
            $row = array(
                "id" => $value->id,
                "parent_id" =>  $value->parent_id,
                "name" =>  $value->name,
                "type" =>  $value->type,
                "path" =>  $value->path,
                "route" =>  $value->route ? (string) $value->route : "",
                "sort" =>  $value->sort,
                "icon" =>  $value->icon,
                "unique_name" => $unik_name . "_" . $number_unix,
                "status" =>  $value->status,
                "role" =>  $value->privilege,
                "created_at" =>  $value->created_at,
                "updated_at" =>  $value->updated_at,
                "child" => $rows ? $rows : null
            );

            $menu[] = $row;
            // $menu = count($menu['data']);
        }
        // echo json_encode($menu);
        // exit;
        return $menu;
    }
}
