<?php

namespace App\Models\Administrator;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Role extends Model
{
    use HasFactory;
    protected $guarded = ["id"];

    public function scopeActive($query)
    {
        return $query->where("status", 1);
    }

    public function scopeInActive($query)
    {
        return $query->where("status", 0);
    }

    public function scopeFilters($query, array $filters)
    {
        if (isset($filters['keyword'])) {
            $query->where(function ($query) use ($filters) {
                $query->where("name", "LIKE", "%" . $filters['keyword'])
                    ->orWhere("email", "LIKE", "%" . $filters['keyword']);
            });
            return  $query;
        }
    }

    public static function menuLogin($id)
    {
        $menu =  Menu::getMenus();
        $data = Role::findOrFail($id);
        $permission = json_decode($data->privilage, true);
        $menus = array();
        foreach ($menu as $key => $value) {
            $getId = array_search('view', $permission[$value['id']]);
            if ($getId > -1) {
                $menus[] = $value;
            }
        }
        return $menus;
    }

    public static function menuLoginAdmin($id)
    {
        $menu =  Menu::getMenus();
        $data = Role::findOrFail($id);
        $permission = json_decode($data->privilage, true);
        $menus = array();
        foreach ($menu as $key => $value) {
            $getId = array_search('view', $permission[$value['id']]);
            if ($getId > -1) {
                $menus[] = $value;
            }
        }
        return $menus;
    }

    public function user()
    {
        return $this->hasOne(User::class);
    }
}
