<?php

namespace App\Models;

use App\Models\Administrator\Role;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'avatar',
        'role_id',
    ];



    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function scopeActive($query)
    {
        return $query->where("status", 1);
    }

    public function scopeInActive($query)
    {
        return $query->where("status", 0);
    }
    public function scopeFilters($query, array $filters)
    {
        if (isset($filters['keyword'])) {
            $query->where(function ($query) use ($filters) {
                $query->where("name", "LIKE", "%" . $filters['keyword'])
                    ->orWhere("email", "LIKE", "%" . $filters['keyword']);
            });
            return  $query;
        }
    }
    public function scopeOrders($query, array $orders)
    {
        if (isset($orders['order'])) {
            $query->where(function ($query, $orders) {
                $query->where("name", "LIKE", "%" . $orders['keyword'])
                    ->orWhere("email", "LIKE", "%" . $orders['keyword']);
            });
            return  $query;
        }
    }

    public function privilage()
    {
        return $this->belongsTo(Role::class);
    }
}
