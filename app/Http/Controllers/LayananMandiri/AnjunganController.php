<?php

namespace App\Http\Controllers\LayananMandiri;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AnjunganController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth:admin');
    // }

    public function index()
    {
        return view('layanan-mandiri.anjungan.index');
    }
}
