<?php

namespace App\Http\Controllers\Kependudukan;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CalonPemilihController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }
    
    public function index()
    {
        return view('kependudukan.calon-pemilih.index');
    }
}
