<?php

namespace App\Http\Controllers\InfoDesa;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class IdentitasDesaController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    
    
    public function index()
    {
        return view('info-desa.identitas-desa.index');
    }

    public function edit()
    {
        return view('info-desa.identitas-desa.form');
    }
}
