<?php

namespace App\Http\Controllers\InfoDesa;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AparatDesaController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }
    
    public function index()
    {
        return view('info-desa.aparat-desa.index');
    }

    public function create()
    {
        return view('info-desa.aparat-desa.form');
    }
}
