<?php

namespace App\Http\Controllers\InfoDesa;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class WilayahAdministratifController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    
    
    public function index()
    {
        return view('info-desa.wilayah-administratif.index');
    }

    public function create()
    {
        return view('info-desa.wilayah-administratif.form');
    }
}
