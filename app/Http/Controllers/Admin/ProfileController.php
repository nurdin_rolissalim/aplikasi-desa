<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function index()
    {
        view("admin.profile");
    }
    public function lockScreen()
    {
        view("admin.auth.lock-screen");
    }
    public function unLock()
    {
        view("admin.profile");
    }
}
