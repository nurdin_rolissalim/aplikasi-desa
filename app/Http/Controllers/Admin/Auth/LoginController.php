<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Http\Controllers\Controller;
use App\Models\Administrator\Menu;
use App\Models\Administrator\Role;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    // use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::ADMIN;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('guest:admin')->except('logout');
    // }

    public function login(Request $request)
    {
        request()->validate(
            [
                'email' => 'required|email',
                'password' => 'required',
            ]
        );

        $kredensil = $request->only('email', 'password');

        if (Auth::guard('admin')->attempt($kredensil)) {
            $user = Auth::guard('admin')->user();
            if ($user->role_id) {
                $data = Role::findOrFail($user->role_id);
                $dataMenu = Role::menuLoginAdmin($user->role_id);
                session(['adminMenus' => $dataMenu]);
                session(['adminRole' => $data]);
                return redirect(route('admin.index'));
            }
            return redirect()->back()->withInput()->withError('Hak akses belum tidak ada');
        }
        return redirect()->back()->withInput()->withError('Email atau Passowrd tidak sesuai');     
    }

    public function showLoginForm()
    {
        if (Auth::guard('admin')->check()) {
            return redirect()->route('admin.dashboard');
        } else {
            return view('admin.auth.login');
        }
    }

    public function logout(Request $request)
    {
        $request->session()->flush();

        Auth::guard('admin')->logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect(route('admin.root'));
    }
}
