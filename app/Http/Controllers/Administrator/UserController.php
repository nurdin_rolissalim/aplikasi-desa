<?php

namespace App\Http\Controllers\Administrator;

use App\Http\Controllers\Controller;
use App\Models\Administrator\Role;
use App\Models\Upload;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use stdClass;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    

    public function index()
    {        
        $data = [
            'title' => "User",
            'ajaxData' => "user.ajaxData",

        ];
        return view('administrator.user.index', $data);
    }

    public function create()
    {
        $roles = Role::all();
        $route = route("user.store");
        $data = new stdClass();
        $data->id = null;
        $data->name = null;
        $data->status = 1;
        return view('administrator.user.form', compact('roles', 'data', 'route'));
    }

    public function store(Request $request)
    {
        $this->validatation($request);
        $params = $request->input();
        if ($request->input("password")) {
            $params['password'] = bcrypt('password');
        }
        // print_r($params);exit;
        $params['avatar'] = Upload::uploadFile($request, "users");
        User::create($params);
        return redirect()->route('user.index');
    }


    public function edit($id)
    {
        $roles = Role::all();
        $data = User::findOrFail($id);
        $route = route("user.update");
        $method = "post";
        return view('administrator.user.form',  compact('data', 'route', 'method', 'roles'));
    }

    public function update(Request $request)
    {
        $this->validatation($request);
        $params = $request->input();
        if ($request->input("password")) {
            $params['password'] = bcrypt('password');
        }
        $data = Role::find($request->input("id"))->update($params);

        return redirect()->route('user.index');
    }


    public function ajaxData(Request $request)
    {
        // print_r($request->input('search[value]'));
        // exit;
        $data = User::Latest()->filters(
            array(
                'keyword' => $request->input('keyword')
            )
        );

        if ($request->input('order')) {
        }
        $total = $data->count();

        $data = $data->skip(0)->take(10)->get();
        $dataArray = [];

        foreach ($data as $item) {
            $row = array();
            $row[] = $item->name;
            $row[] = "username";
            $row[] = "hak akses";
            $row[] = $item->email;
            $row[] = "status";
            $row[] = "photo";
            $row[] = "phone";
            $aksi = "";

            $aksi .= '<a href="' . route("user.edit", $item->id) . '"
            class="btn btn-outline-secondary waves-effect waves-light btn-sm"
            data-toggle="tooltip" data-placement="top" title=""
            data-original-title="Edit">
            <i class=" fas fa-pencil-alt"></i></a>';
            $aksi .= '<button class="btn btn-outline-secondary waves-effect waves-light btn-sm ms-1"
            data-toggle="tooltip" data-placement="top" title=""
            data-original-title="Setting Hak Akses">
            <i class=" fas fa-trash"></i></button>';

            $row[] = $aksi;

            $dataArray[] = $row;
        }

        $total_filter = 0;

        $output = array(
            "draw" => $request->input('draw'),
            "recordsTotal" => $total,
            "recordsFiltered" => $total_filter,
            "data" => $dataArray,
        );

        return $output;
    }

    private function validatation(Request $request)
    {
        $validator = [
            'name' => 'required',
            'email' => 'required|unique:users,email',
            // 'file' => 'required|mimes:png,jpeg|max:2048',
            'role_id' => 'required',
            'password' => 'required',
        ];



        if ($request->input("id")) {
            $validator['email'] = 'required|unique:users,email,' . $request->input("id") . ',id';
            unset($validator['password']);
        }

        $request->validate(
            $validator,
            [
                'name.required' => 'Nama harus diisi',
                // 'file.required' => 'Image harus diisi',
                'role_id.required' => 'Hak akses belum dipilih',
                'email.required' => 'Email harus diisi',
                'email.unique' => 'Email sudah ada'
            ]
        );



        return $request;
    }
}
