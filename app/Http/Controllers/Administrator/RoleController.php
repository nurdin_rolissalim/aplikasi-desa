<?php

namespace App\Http\Controllers\Administrator;

use App\Http\Controllers\Controller;
use App\Models\Administrator\Menu;
use App\Models\Administrator\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use stdClass;

class RoleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    

    public function index()
    {
        $data = array(
            "title" => "adsadasd"
        );
        return view('administrator.role.index', $data);
    }

    public function create()
    {
        $route = route("role.store");
        $data = new stdClass();
        $data->id = null;
        $data->status = 1;
        return view('administrator.role.form', compact('data', 'route'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
        ]);

        Role::create($request->input());
        return redirect()->route('role.index');
    }


    public function edit($id)
    {
        $data = Role::findOrFail($id);
        $route = route("role.update");
        $method = "post";
        return view('administrator.role.form',  compact('data', 'route', 'method'));
    }

    public function update(Request $request)
    {
        $request->validate([
            'name' => 'required',
        ]);
        $data = Role::find($request->input("id"))->update($request->input());

        return redirect()->route('role.index');
    }

    public function privilage($id)
    {
        $data = Role::findOrFail($id);
        $route = route("role.privilageStore");
        $dataMenu = Menu::getMenus();
        $menus = array(
            "total" => count($dataMenu),
            "data" => $dataMenu
        );
        $permission = json_decode($data->privilage, true);
        // $permission = ($data->privilage && $data->privilage != "") ? json_decode($data->privilage, true) : array();

        // echo $permission;exit;
        return view('administrator.role.privilage-form',  compact('data', 'route', 'menus', 'permission'));
    }
    public function privilageStore(Request $request)
    {
        $params = $request->input();
        $id = $params['id'];
        unset($params['id']);
        unset($params['_token']);

        $data = Role::findOrFail($request->input("id"));
        $data->update([
            "privilage" => json_encode($params)
        ]);
        // $route = route("role.update");
        // return view('administrator.role.privilage-form',  compact('data', 'route'));
    }

    public function ajaxData(Request $request)
    {

        $data = Role::Latest()->filters(
            array(
                'keyword' => $request->input('keyword')
            )
        );

        if ($request->input('order')) {
        }
        $total = $data->count();

        $data = $data->skip(0)->take(10)->get();
        $dataArray = [];

        foreach ($data as $item) {
            $row = array();
            $row[] = $item->name;
            $row[] = $item->status;
            $aksi = "";
            // if ($permission['hapus']) {
            //     $aksi .=  '<button class="btn mb-1 btn-flat btn-outline-danger btn-sm mr-1"  title="Delete"  data-toggle="tooltip"
            //                 data-placement="top" title="" data-original-title="Hapus" onclick="deleteData(' . "'" . $value['id_user'] . "'" . ')"><i class="mdi mdi-delete"></i></button>';
            // }

            // $aksi .= '<button onclick="editData(' . "'" . $item['id'] . "'" . ')"
            //             class="btn btn-outline-secondary waves-effect waves-light btn-sm"
            //             data-toggle="tooltip" data-placement="top" title=""
            //             data-original-title="detail">
            //             <i class=" fas fa-pencil-alt"></i></button';
            $aksi .= '<a href="' . route("role.edit", $item->id) . '"
                        class="btn btn-outline-secondary waves-effect waves-light btn-sm"
                        data-toggle="tooltip" data-placement="top" title=""
                        data-original-title="Edit">
                        <i class=" fas fa-pencil-alt"></i></a>';
            $aksi .= '<a href="' . route("role.privilage", $item->id) . '"
                        class="btn btn-outline-secondary waves-effect waves-light btn-sm ms-1"
                        data-toggle="tooltip" data-placement="top" title=""
                        data-original-title="Setting Hak Akses">
                        <i class=" fas fa-key"></i></a>';


            $row[] = $aksi;

            $dataArray[] = $row;
        }


        $output = array(
            "draw" => $request->input('draw'),
            "recordsTotal" => $total,
            "recordsFiltered" => $total,
            "data" => $dataArray,
        );

        return $output;
    }
}
