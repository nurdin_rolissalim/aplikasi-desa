<?php

namespace App\Http\Controllers\Administrator;

use App\Http\Controllers\Controller;
use App\Models\Administrator\Menu;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MenuController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
   
    
    public function index()
    {
        // $data = config('menu');
        $dataMenu = Menu::getMenus();
        // var_dump($dataMenu);exit;
        $data = array(
            "menus" => array(
                "total" => count($dataMenu),
                "data" => $dataMenu
            ),
        );
        return view('administrator.menu.index', $data);
    }

    public function store(Request $request)
    {
        // $data = config('menu');

        $params = $request->input();
        $privilege = [];
        if (isset($params["add"])) {
            $privilege[] = "add";
            unset($params['add']);
        }
        if (isset($params["view"])) {
            $privilege[] = "view";
            unset($params['view']);
        }
        if (isset($params["delete"])) {
            $privilege[] = "delete";
            unset($params['delete']);
        }
        if (isset($params["update"])) {
            $privilege[] = "update";
            unset($params['update']);
        }
        $params['privilege'] = json_encode($privilege);
        // print_r($request->input());
        // exit;
        Menu::create($params);
        return redirect()->route('menu.index');
    }
}
