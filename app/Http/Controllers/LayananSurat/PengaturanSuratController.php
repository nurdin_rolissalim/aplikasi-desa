<?php

namespace App\Http\Controllers\LayananSurat;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PengaturanSuratController extends Controller
{
    public function index()
    {
        return view('layanan-surat.pengaturan-surat.index');
    }
    
    public function create()
    {
        return view('layanan-surat.pengaturan-surat.form');
    }
}
