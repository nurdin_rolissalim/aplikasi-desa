<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function index()
    {
        return view("member.profile.index");
    }
    public function pin()
    {
        return view("member.profile.ganti-pin");
    }
}
