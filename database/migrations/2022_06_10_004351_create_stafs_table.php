<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stafs', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('nik')->nullable();
            $table->string('nipd')->nullable();
            $table->string('nip')->nullable();
            $table->string('place_of_birth')->nullable();
            $table->date('date_of_birth')->nullable();
            $table->enum('gender', ['Laki-Laki', 'Perempuan'])->default('Laki-Laki');
            $table->enum('education', [
                'Tidak/Belum Sekolah',
                'Belum Tamat SD/Sederajat',
                'Tamat SD/Sederajat',
                'SLTP SD/Sederajat',
                'SLTA SD/Sederajat',
                'Diploma I/II',
                'Akademi/Diploma III/S.Muda',
                'Diploma IV/Strata I',
                'Strata II',
                'Strata III',
            ])->default('Tidak/Belum Sekolah');
            $table->enum('religi', [
                'Islam',
                'Kristen',
                'Khatolik',
                'Budha',
                'Hindu',
                'Khonghucu',
            ])->default('Islam');
            $table->string('golongan')->nullable();
            $table->string('no_sk_pengangkatan')->nullable();
            $table->date('date_sk_pengangkatan')->nullable();
            $table->string('no_sk_pemberhentian')->nullable();
            $table->date('date_sk_pemberhentian')->nullable();
            $table->string('long_of_service')->nullable();
            $table->string('jabatan');
            $table->foreignId('staf_id')->nullable();
            $table->boolean('status')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stafs');
    }
};
