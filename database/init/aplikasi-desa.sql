-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jun 30, 2022 at 07:26 AM
-- Server version: 5.7.36
-- PHP Version: 7.4.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dason_laravel`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

DROP TABLE IF EXISTS `admins`;
CREATE TABLE IF NOT EXISTS `admins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) DEFAULT NULL,
  `email` varchar(191) NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) NOT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `role_id` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `user_type` enum('Administrator','Member') NOT NULL DEFAULT 'Member',
  `avatar` text NOT NULL,
  `created_at` timestamp NOT NULL,
  `updated_at` timestamp NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `nama`, `email`, `email_verified_at`, `password`, `remember_token`, `role_id`, `status`, `user_type`, `avatar`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin@gmail.com', NULL, '$2y$10$m1EimTE1Ay0TiSb2jP7GeeP0NeclhAJfkzd.JV6jDQQNGXvnjAJj2', NULL, 1, 1, 'Administrator', '', '2022-06-28 08:16:49', '2022-06-28 08:16:49');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE IF NOT EXISTS `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uuid` varchar(191) NOT NULL,
  `connection` text NOT NULL,
  `queue` text NOT NULL,
  `payload` longtext NOT NULL,
  `exception` longtext NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `members`
--

DROP TABLE IF EXISTS `members`;
CREATE TABLE IF NOT EXISTS `members` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nik` int(15) NOT NULL,
  `name` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `members`
--

INSERT INTO `members` (`id`, `nik`, `name`, `password`) VALUES
(1, 1, 'nurdin', '$2y$10$m1EimTE1Ay0TiSb2jP7GeeP0NeclhAJfkzd.JV6jDQQNGXvnjAJj2');

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

DROP TABLE IF EXISTS `menus`;
CREATE TABLE IF NOT EXISTS `menus` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) NOT NULL,
  `path` varchar(191) DEFAULT NULL,
  `sort` int(11) NOT NULL,
  `icon` varchar(191) DEFAULT NULL,
  `status` tinyint(4) NOT NULL,
  `type` enum('Administrator','Member','Public') NOT NULL DEFAULT 'Administrator',
  `parent_id` bigint(20) UNSIGNED DEFAULT NULL,
  `privilege` varchar(255) DEFAULT NULL,
  `route` varchar(50) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `name`, `path`, `sort`, `icon`, `status`, `type`, `parent_id`, `privilege`, `route`, `created_at`, `updated_at`) VALUES
(1, 'Dashboard', 'dashboard', 1, 'fas fa-home', 1, 'Public', NULL, NULL, 'dashboard.index', '2022-06-20 00:58:20', '2022-06-20 00:58:20'),
(2, 'Info Desa', NULL, 2, 'fas fa-air-freshener', 1, 'Administrator', NULL, NULL, NULL, '2022-06-20 01:22:28', '2022-06-20 01:22:28'),
(3, 'Kependudukan', NULL, 3, 'fas fa-user-friends', 1, 'Administrator', NULL, NULL, NULL, '2022-06-20 02:26:31', '2022-06-20 02:26:31'),
(4, 'Layanan Surat', NULL, 4, 'far fa-envelope', 1, 'Administrator', NULL, NULL, NULL, '2022-06-20 02:27:25', '2022-06-20 02:27:25'),
(5, 'Layanan Surat Mandiri', NULL, 5, 'far fa-envelope', 1, 'Administrator', NULL, NULL, NULL, '2022-06-20 02:28:06', '2022-06-20 02:28:06'),
(6, 'Menu', 'menu', 6, 'fas fa-bars', 1, 'Administrator', NULL, NULL, 'menu.index', '2022-06-20 02:35:49', '2022-06-20 02:35:49'),
(7, 'Hak Akses', 'role', 7, 'fas fa-user-lock', 1, 'Administrator', NULL, NULL, 'role.index', '2022-06-20 02:36:52', '2022-06-20 02:36:52'),
(8, 'User', 'user', 8, 'fas fa-user-alt', 1, 'Administrator', NULL, NULL, 'user.index', '2022-06-20 02:37:20', '2022-06-20 02:37:20'),
(9, 'Identitas Desa', 'identitas-desa', 1, NULL, 1, 'Administrator', 2, '[\"view\",\"update\"]', 'identitasDesa.index', '2022-06-21 00:06:53', '2022-06-21 00:06:53'),
(10, 'Wilayah Administratif', 'wilayah-administratif', 2, NULL, 1, 'Administrator', 2, '[\"add\",\"view\",\"delete\",\"update\"]', 'wilayahAdministratif.index', '2022-06-21 00:09:52', '2022-06-21 00:09:52'),
(11, 'Pemerintahan Desa', NULL, 3, NULL, 1, 'Administrator', 2, '[\"view\"]', NULL, '2022-06-21 00:11:13', '2022-06-21 00:11:13'),
(12, 'Buku Pemerintahan Desa', 'buku-pemerintahan-desa', 1, NULL, 1, 'Administrator', 11, '[\"add\",\"view\",\"delete\",\"update\"]', 'bukuPeraturanDesa.index', '2022-06-21 00:12:46', '2022-06-21 00:12:46'),
(13, 'Aparat Pemerintahan Desa', 'aparat-desa', 2, NULL, 1, 'Administrator', 11, '[\"add\",\"view\",\"delete\",\"update\"]', 'aparatDesa.index', '2022-06-21 19:33:25', '2022-06-21 19:33:25'),
(14, 'Penduduk', 'penduduk', 1, NULL, 1, 'Administrator', 3, '[\"add\",\"view\",\"delete\",\"update\"]', 'penduduk.index', '2022-06-21 19:34:05', '2022-06-21 19:34:05'),
(15, 'Keluarga', 'keluarga', 2, NULL, 1, 'Administrator', 3, '[\"add\",\"view\",\"delete\",\"update\"]', 'keluarga.index', '2022-06-21 19:34:38', '2022-06-21 19:34:38'),
(16, 'Rumah Tangga', 'rumah-tangga', 3, NULL, 1, 'Administrator', 3, '[\"add\",\"view\",\"delete\",\"update\"]', 'rumahTangga.index', '2022-06-21 19:35:08', '2022-06-21 19:35:08'),
(17, 'Calon Pemilih', 'calon-pemilih', 4, NULL, 1, 'Administrator', 3, '[\"add\",\"view\",\"delete\",\"update\"]', 'calonPemilih.index', '2022-06-21 19:35:46', '2022-06-21 19:35:46'),
(18, 'Pengaturan Surat', 'pengaturan-surat', 1, NULL, 1, 'Administrator', 4, '[\"add\",\"view\",\"delete\",\"update\"]', 'pengaturanSurat.index', '2022-06-21 19:36:25', '2022-06-21 19:36:25'),
(19, 'Cetak Surat', 'cetak-surat', 2, NULL, 1, 'Administrator', 4, '[\"add\",\"view\",\"delete\",\"update\"]', 'cetakSurat.index', '2022-06-21 19:36:57', '2022-06-21 19:36:57'),
(20, 'Panduan', 'panduan', 3, NULL, 1, 'Administrator', 4, '[\"add\",\"view\",\"delete\",\"update\"]', 'panduan.index', '2022-06-21 19:37:43', '2022-06-21 19:37:43'),
(21, 'Daftar Persyaratan', 'daftar-persyaratan', 4, NULL, 1, 'Administrator', 4, '[\"add\",\"view\",\"delete\",\"update\"]', 'daftarPersyaratan.index', '2022-06-21 19:38:27', '2022-06-21 19:38:27'),
(22, 'Permohonan Surat', 'permohonan-surat', 1, NULL, 1, 'Administrator', 5, '[\"add\",\"view\",\"delete\",\"update\"]', 'permohonanSurat.index', '2022-06-21 19:39:13', '2022-06-21 19:39:13'),
(23, 'Pendaftar Layanan Mandiri', 'pendaftar-layanan-mandiri', 2, NULL, 1, 'Administrator', 5, '[\"add\",\"view\",\"delete\",\"update\"]', 'pendaftarLayananMandiri.index', '2022-06-21 19:39:59', '2022-06-21 19:39:59');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(5, '2022_06_10_004351_create_stafs_table', 1),
(6, '2022_06_20_045629_create_menus_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) NOT NULL,
  `token` varchar(191) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

DROP TABLE IF EXISTS `personal_access_tokens`;
CREATE TABLE IF NOT EXISTS `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `tokenable_type` varchar(191) NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) NOT NULL,
  `token` varchar(64) NOT NULL,
  `abilities` text,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `privilage` text,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `status`, `privilage`, `updated_at`, `created_at`) VALUES
(1, 'Admin', 1, '{\"1\":[\"all\",\"view\",\"add\",\"edit\",\"delete\"],\"2\":[\"all\",\"view\",\"add\",\"edit\",\"delete\"],\"9\":[\"all\",\"view\",\"add\",\"edit\",\"delete\"],\"10\":[\"all\",\"view\",\"add\",\"edit\",\"delete\"],\"11\":[\"all\",\"view\",\"add\",\"edit\",\"delete\"],\"12\":[\"add\",\"edit\",\"delete\"],\"13\":[\"add\",\"edit\",\"delete\"],\"3\":[\"all\",\"view\",\"add\",\"edit\",\"delete\"],\"14\":[\"all\",\"view\",\"add\",\"edit\",\"delete\"],\"15\":[\"all\",\"view\",\"add\",\"edit\",\"delete\"],\"16\":[\"all\",\"view\",\"add\",\"edit\",\"delete\"],\"17\":[\"all\",\"view\",\"add\",\"edit\",\"delete\"],\"4\":[\"all\",\"view\",\"add\",\"edit\",\"delete\"],\"18\":[\"all\",\"view\",\"add\",\"edit\",\"delete\"],\"19\":[\"all\",\"view\",\"add\",\"edit\",\"delete\"],\"20\":[\"all\",\"view\",\"add\",\"edit\",\"delete\"],\"21\":[\"all\",\"view\",\"add\",\"edit\",\"delete\"],\"5\":[\"all\",\"view\",\"add\",\"edit\",\"delete\"],\"22\":[\"all\",\"view\",\"add\",\"edit\",\"delete\"],\"23\":[\"all\",\"view\",\"add\",\"edit\",\"delete\"],\"6\":[\"all\",\"view\",\"add\",\"edit\",\"delete\"],\"7\":[\"all\",\"view\",\"add\",\"edit\",\"delete\"],\"8\":[\"all\",\"view\",\"add\",\"edit\",\"delete\"]}', '2022-06-22 02:47:12', '2022-06-21 21:02:41'),
(2, 'Operator', 1, NULL, '2022-06-24 02:48:51', '2022-06-22 05:02:50');

-- --------------------------------------------------------

--
-- Table structure for table `stafs`
--

DROP TABLE IF EXISTS `stafs`;
CREATE TABLE IF NOT EXISTS `stafs` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) NOT NULL,
  `nik` varchar(191) DEFAULT NULL,
  `nipd` varchar(191) DEFAULT NULL,
  `nip` varchar(191) DEFAULT NULL,
  `place_of_birth` varchar(191) DEFAULT NULL,
  `date_of_birth` date DEFAULT NULL,
  `gender` enum('Laki-Laki','Perempuan') NOT NULL DEFAULT 'Laki-Laki',
  `education` enum('Tidak/Belum Sekolah','Belum Tamat SD/Sederajat','Tamat SD/Sederajat','SLTP SD/Sederajat','SLTA SD/Sederajat','Diploma I/II','Akademi/Diploma III/S.Muda','Diploma IV/Strata I','Strata II','Strata III') NOT NULL DEFAULT 'Tidak/Belum Sekolah',
  `religi` enum('Islam','Kristen','Khatolik','Budha','Hindu','Khonghucu') NOT NULL DEFAULT 'Islam',
  `golongan` varchar(191) DEFAULT NULL,
  `no_sk_pengangkatan` varchar(191) DEFAULT NULL,
  `date_sk_pengangkatan` date DEFAULT NULL,
  `no_sk_pemberhentian` varchar(191) DEFAULT NULL,
  `date_sk_pemberhentian` date DEFAULT NULL,
  `long_of_service` varchar(191) DEFAULT NULL,
  `jabatan` varchar(191) NOT NULL,
  `staf_id` bigint(20) UNSIGNED DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) NOT NULL,
  `email` varchar(191) NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) NOT NULL,
  `avatar` text NOT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  `status` tinyint(4) NOT NULL,
  `user_type` enum('Administrator','Member') NOT NULL DEFAULT 'Member',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `user_role_foregnkey_01` (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `avatar`, `remember_token`, `role_id`, `status`, `user_type`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin@themesdesign.com', NULL, '$2y$10$m1EimTE1Ay0TiSb2jP7GeeP0NeclhAJfkzd.JV6jDQQNGXvnjAJj2', 'avatar-1.jpg', NULL, 1, 1, 'Administrator', '2022-06-20 00:58:17', '2022-06-20 00:58:17'),
(2, 'nurdin', 'nurdinif14@gmail.com', NULL, '$2y$10$m1EimTE1Ay0TiSb2jP7GeeP0NeclhAJfkzd.JV6jDQQNGXvnjAJj2', '/images/users/1655970813.jpg', NULL, 1, 1, 'Member', '2022-06-23 00:53:33', '2022-06-23 00:53:33'),
(3, 'andri', 'andri@gmail.com', NULL, '$2y$10$m1EimTE1Ay0TiSb2jP7GeeP0NeclhAJfkzd.JV6jDQQNGXvnjAJj2', '/images/users/1655971220.jpg', NULL, 2, 1, 'Member', '2022-06-23 01:00:20', '2022-06-23 01:00:20'),
(4, 'subroto', 'subroto@gmail.com', NULL, '$2y$10$m1EimTE1Ay0TiSb2jP7GeeP0NeclhAJfkzd.JV6jDQQNGXvnjAJj2', '/images/users/1655971447.jpg', NULL, 1, 1, 'Member', '2022-06-23 01:04:07', '2022-06-23 01:04:07'),
(5, 'asep', 'asep@gmail.com', NULL, '$2y$10$m1EimTE1Ay0TiSb2jP7GeeP0NeclhAJfkzd.JV6jDQQNGXvnjAJj2', '/images/users/1655971527.png', NULL, 1, 1, 'Member', '2022-06-23 01:05:27', '2022-06-23 01:05:27');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
