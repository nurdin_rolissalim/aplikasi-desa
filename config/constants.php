<?php

return [
    /*
    |--------------------------------------------------------------------------
    | ALL OPTIONS DALAM APLIKASI 
    |--------------------------------------------------------------------------
    |
    */

    'gender' => [
        "LAKI-LAKI", "PEREMPUAN"
    ],
    'warga_negara' => [
        "WNI", "WNA","DUA KEWARGANEGARAAN"
    ],
    'blood_group' => [
        "A", "B", "AB", "O", "A+", "B+", "AB+", "O+", "A-", "B-", "AB-", "O-",
    ],
    'status_kawin' => [
        "BELUM MENIKAH", "MENIKAH","CERAI MATI","CERAI HIDUP"
    ],
    'status_penduduk' => [
        "TETAP", "TIDAK TETAP"
    ],
    'status_dasar_penduduk' => [
        "HIDUP", "MATI","HILANG","PINDAH","PERGI","TIDAK VALID"
    ],
    'status_ktp_el' => [
        'BELUM', 'KTP-EL', 'KIA'
    ],
    'status_rekam_ktp' => [
        'BELUM REKAM', 'SUDAH REKAM', 'CARD PRINTED', 'PRINT READY RECORD', 'CARD SHIPPED', 'SENT FOR CARD PRINTING', 'CARD ISSUED'
    ],
    'tempat_kelahiran' => [
        'RS/RB', 'KLINIK', 'PUSKESMAS', 'POLINDES', 'RUMAH', 'LAINNYA'
    ],
    'jenis_kelahiran' => [
        'TUNGGAL', 'KEMBAR 2', 'KEMBAR 3', 'KEMBAR 4'
    ],
    'penolong_kelahiran' => [
        'DOKTER', 'BIDAN PERAWAT', 'DUKUN', 'LAINNYA'
    ],
    'mutasi_penduduk' => [
        'LAHIR', 'MATI', 'PINDAH KELUAR', 'HILANG','PINDAH MASUK','PERGI'
    ],
    'masa_berlaku_surat' => [
        'TAHUN','BULAN','HARI'
    ],


];
