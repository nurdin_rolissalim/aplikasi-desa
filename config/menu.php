<?php

return [



    /*
    |--------------------------------------------------------------------------
    | All Menu
    |--------------------------------------------------------------------------
    |
    */


    'menus' => [
        'dashboard' => array(
            "title" => "Dashboard",
            "route" => "",
            "previllage" => ["view", "create", "update", "delete"],
            "child" => null
        ),
        'info-desa' => array(
            "title" => "Info Desa",
            "route" => "",
            "previllage" => ["view"],
            "child" => [
                'identitas-desa' => array(
                    "title" => "Identitas Desa",
                    "route" => "",
                    "previllage" => ["view", "create", "update", "delete"],
                    "child" => null
                ),
                'wilaya-administratif' => array(
                    "title" => "Wilayah Administratif",
                    "route" => "",
                    "previllage" => ["view", "create", "update", "delete"],
                    "child" => null
                ),
                'pemerintahan-desa' => array(
                    "title" => "Pemerintahan Desa",
                    "route" => "",
                    "previllage" => ["view", "create", "update", "delete"],
                    "child" => [
                        'buku-pemerintahan-desa' => array(
                            "title" => "Buku Pemerintahan Desa",
                            "route" => "",
                            "previllage" => ["view", "create", "update", "delete"],
                            "child" => null
                        ),
                        'aparat-pemerintahan-desa' => array(
                            "title" => "Aparat Pemerintahan Desa",
                            "route" => "",
                            "previllage" => ["view", "create", "update", "delete"],
                            "child" => null
                        ),
                    ]
                ),
            ]
        ),
        'kependudukan' => array(
            "title" => "Kependudukan",
            "route" => "",
            "previllage" => ["view"],
            "child" => [
                'penduduk' => array(
                    "title" => "Penduduk",
                    "route" => "",
                    "previllage" => ["view", "create", "update", "delete"],
                    "child" => null
                ),
                'keluarga' => array(
                    "title" => "Keluarga",
                    "route" => "",
                    "previllage" => ["view", "create", "update", "delete"],
                    "child" => null
                ),
                'rumah-tangga' => array(
                    "title" => "Rumah Tangga",
                    "route" => "",
                    "previllage" => ["view", "create", "update", "delete"],
                    "child" => null
                ),
                'calon-pemilih' => array(
                    "title" => "Calon Pemilih",
                    "route" => "",
                    "previllage" => ["view", "create", "update", "delete"],
                    "child" => null
                ),
            ]
        ),

        'layanan-surat' => array(
            "title" => "Layanan Surat",
            "route" => "",
            "previllage" => ["view"],
            "child" => [
                'pengaturan-surat' => array(
                    "title" => "Pengaturan Surat",
                    "route" => "",
                    "previllage" => ["view", "create", "update", "delete"],
                    "child" => null
                ),
                'cetak-surat' => array(
                    "title" => "Cetak Surat",
                    "route" => "",
                    "previllage" => ["view", "create", "update", "delete"],
                    "child" => null
                ),
                'panduan' => array(
                    "title" => "Panduan",
                    "route" => "",
                    "previllage" => ["view", "create", "update", "delete"],
                    "child" => null
                ),
                'daftar-persyaratan' => array(
                    "title" => "Daftar Persyaratan",
                    "route" => "",
                    "previllage" => ["view", "create", "update", "delete"],
                    "child" => null
                ),
            ]
        ),
        'layanan-surat-mandiri' => array(
            "title" => "Layanan Surat Mandiri",
            "route" => "",
            "previllage" => ["view"],
            "child" => [
                'permohonanan-surat' => array(
                    "title" => "Permohonan Surat",
                    "route" => "",
                    "previllage" => ["view", "create", "update", "delete"],
                    "child" => null
                ),
                'pendaftar-layanan-mandiri' => array(
                    "title" => "Pendaftar Layanan Mandiri",
                    "route" => "",
                    "previllage" => ["view", "create", "update", "delete"],
                    "child" => null
                ),
            ]
        ),
    ],

];
