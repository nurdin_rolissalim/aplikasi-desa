<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\Auth\Admin\AdminController;
use App\Http\Controllers\Admin\Auth\{
    LoginController
};

Route::group([
    'namespace' => 'App\Http\Controllers\Member'
], function () {
    Route::prefix('profile-member')->group(function () {
        Route::get('/', [ProfileController::class, 'index'])->name('profileMember.index');
        Route::post('/update', [ProfileController::class, 'update'])->name('profileMember.update');
        Route::get('/ganti-pin', [ProfileController::class, 'update'])->name('profilePin.show');
        Route::post('/ganti-pin-store', [ProfileController::class, 'update'])->name('profilePin.store');
    });

    Route::prefix('profile-surat')->group(function () {
        Route::get('/', [SuratController::class, 'index'])->name('profileSurat.index');
        Route::get('/create', [SuratController::class, 'create'])->name('profileSurat.create');
        Route::post('/store', [SuratController::class, 'update'])->name('profileSurat.store');
    });      
});

Route::name('admin.')->namespace('Admin')->prefix('admin')->group(function () {
    Route::get('/', [AdminController::class, 'root'])->name('root.admin');

    Route::namespace('Auth')->middleware('guest:admin')->group(function () {
        //login route
        Route::get('/login', 'LoginController@login')->name('login');
        Route::post('/login', 'LoginController@processLogin');
    });

    Route::namespace('Auth')->middleware('auth:admin')->group(function () {

        //Users
        Route::group([
            'namespace' => 'App\Http\Controllers\Admnistrator'
        ], function () {
            Route::prefix('user')->group(function () {
                Route::get('/', [UserController::class, 'index'])->name('user.index');
                Route::get('/create', [UserController::class, 'create'])->name('user.create');
                Route::post('/store', [UserController::class, 'store'])->name('user.store');
                Route::get('/show', [UserController::class, 'show'])->name('user.show');
                Route::get('/edit/{id?}', [UserController::class, 'edit'])->name('user.edit');
                Route::post('/update', [UserController::class, 'update'])->name('user.update');
                Route::delete('/destroy', [UserController::class, 'destroy'])->name('user.destroy');
                Route::post('/ajax-data', [UserController::class, 'ajaxData'])->name('user.ajaxData');
            });
            Route::prefix('menu')->group(function () {
                Route::get('/', [MenuController::class, 'index'])->name('menu.index');
                Route::get('/create', [MenuController::class, 'create'])->name('menu.create');
                Route::post('/store', [MenuController::class, 'store'])->name('menu.store');
                Route::get('/show', [MenuController::class, 'show'])->name('menu.show');
                Route::get('/edit', [MenuController::class, 'edit'])->name('menu.edit');
                Route::put('/update/{id?}', [MenuController::class, 'update'])->name('menu.update');
                Route::delete('/destroy', [MenuController::class, 'destroy'])->name('menu.destroy');
            });
            Route::prefix('role')->group(function () {
                Route::get('/', [RoleController::class, 'index'])->name('role.index');
                Route::get('/create', [RoleController::class, 'create'])->name('role.create');
                Route::post('/store', [RoleController::class, 'store'])->name('role.store');
                Route::get('/show', [RoleController::class, 'show'])->name('role.show');
                Route::get('/edit/{id?}', [RoleController::class, 'edit'])->name('role.edit');
                Route::get('/privilage/{id?}', [RoleController::class, 'privilage'])->name('role.privilage');
                Route::post('/store-privilage', [RoleController::class, 'privilageStore'])->name('role.privilageStore');
                Route::post('/update', [RoleController::class, 'update'])->name('role.update');
                Route::delete('/destroy', [RoleController::class, 'destroy'])->name('role.destroy');
                Route::post('/ajax-data', [RoleController::class, 'ajaxData'])->name('role.ajaxData');
            });
        });

        // Info Desa
        Route::group([
            'namespace' => 'App\Http\Controllers\InfoDesa'
        ], function () {
            Route::prefix('identitas-desa')->group(function () {
                Route::get('/', [IdentitasDesaController::class, 'index'])->name('identitasDesa.index');
                Route::get('/create', [IdentitasDesaController::class, 'create'])->name('identitasDesa.create');
                Route::post('/store', [IdentitasDesaController::class, 'store'])->name('identitasDesa.store');
                Route::get('/show', [IdentitasDesaController::class, 'show'])->name('identitasDesa.show');
                Route::get('/edit/{id?}', [IdentitasDesaController::class, 'edit'])->name('identitasDesa.edit');
                Route::patch('/update', [IdentitasDesaController::class, 'update'])->name('identitasDesa.update');
                Route::delete('/destroy', [IdentitasDesaController::class, 'destroy'])->name('identitasDesa.destroy');
            });
            Route::prefix('wilayah-administratif')->group(function () {
                Route::get('/', [WilayahAdministratifController::class, 'index'])->name('wilayahAdministratif.index');
                Route::get('/create', [WilayahAdministratifController::class, 'create'])->name('wilayahAdministratif.create');
                Route::post('/store', [WilayahAdministratifController::class, 'store'])->name('wilayahAdministratif.store');
                Route::get('/show', [WilayahAdministratifController::class, 'show'])->name('wilayahAdministratif.show');
                Route::get('/edit', [WilayahAdministratifController::class, 'edit'])->name('wilayahAdministratif.edit');
                Route::patch('/update', [WilayahAdministratifController::class, 'update'])->name('wilayahAdministratif.update');
                Route::delete('/destroy', [WilayahAdministratifController::class, 'destroy'])->name('wilayahAdministratif.destroy');
            });

            Route::prefix('buku-peraturan-desa')->group(function () {
                Route::get('/', [BukuPeraturanDesaController::class, 'index'])->name('bukuPeraturanDesa.index');
                Route::get('/create', [BukuPeraturanDesaController::class, 'create'])->name('bukuPeraturanDesa.create');
                Route::post('/store', [BukuPeraturanDesaController::class, 'store'])->name('bukuPeraturanDesa.store');
                Route::get('/show', [BukuPeraturanDesaController::class, 'show'])->name('bukuPeraturanDesa.show');
                Route::get('/edit', [BukuPeraturanDesaController::class, 'edit'])->name('bukuPeraturanDesa.edit');
                Route::patch('/update', [BukuPeraturanDesaController::class, 'update'])->name('bukuPeraturanDesa.update');
                Route::delete('/destroy', [BukuPeraturanDesaController::class, 'destroy'])->name('bukuPeraturanDesa.destroy');
            });

            Route::prefix('aparat-desa')->group(function () {
                Route::get('/', [AparatDesaController::class, 'index'])->name('aparatDesa.index');
                Route::get('/create', [AparatDesaController::class, 'create'])->name('aparatDesa.create');
                Route::post('/store', [AparatDesaController::class, 'store'])->name('aparatDesa.store');
                Route::get('/show', [AparatDesaController::class, 'show'])->name('aparatDesa.show');
                Route::get('/edit', [AparatDesaController::class, 'edit'])->name('aparatDesa.edit');
                Route::patch('/update', [AparatDesaController::class, 'update'])->name('aparatDesa.update');
                Route::delete('/destroy', [AparatDesaController::class, 'destroy'])->name('aparatDesa.destroy');
            });
        });

        // Kependudukan
        Route::group([
            'namespace' => 'App\Http\Controllers\Kependudukan'
        ], function () {
            Route::prefix('penduduk')->group(function () {
                Route::get('/', [PendudukController::class, 'index'])->name('penduduk.index');
                Route::get('/create', [PendudukController::class, 'create'])->name('penduduk.create');
                Route::post('/store', [PendudukController::class, 'store'])->name('penduduk.store');
                Route::get('/show', [PendudukController::class, 'show'])->name('penduduk.show');
                Route::get('/edit', [PendudukController::class, 'edit'])->name('penduduk.edit');
                Route::patch('/update', [PendudukController::class, 'update'])->name('penduduk.update');
                Route::delete('/destroy', [PendudukController::class, 'destroy'])->name('penduduk.destroy');
            });

            Route::prefix('keluarga')->group(function () {
                Route::get('/', [KeluargaController::class, 'index'])->name('keluarga.index');
                Route::get('/create', [KeluargaController::class, 'create'])->name('keluarga.create');
                Route::post('/store', [KeluargaController::class, 'store'])->name('keluarga.store');
                Route::get('/show', [KeluargaController::class, 'show'])->name('keluarga.show');
                Route::get('/edit', [KeluargaController::class, 'edit'])->name('keluarga.edit');
                Route::patch('/update', [KeluargaController::class, 'update'])->name('keluarga.update');
                Route::delete('/destroy', [KeluargaController::class, 'destroy'])->name('keluarga.destroy');
            });
            Route::prefix('rumah-tangga')->group(function () {
                Route::get('/', [RumahTanggaController::class, 'index'])->name('rumahTangga.index');
                Route::get('/create', [RumahTanggaController::class, 'create'])->name('rumahTangga.create');
                Route::post('/store', [RumahTanggaController::class, 'store'])->name('rumahTangga.store');
                Route::get('/show', [RumahTanggaController::class, 'show'])->name('rumahTangga.show');
                Route::get('/edit', [RumahTanggaController::class, 'edit'])->name('rumahTangga.edit');
                Route::patch('/update', [RumahTanggaController::class, 'update'])->name('rumahTangga.update');
                Route::delete('/destroy', [RumahTanggaController::class, 'destroy'])->name('rumahTangga.destroy');
            });
            Route::prefix('calon-pemilih')->group(function () {
                Route::get('/', [CalonPemilihController::class, 'index'])->name('calonPemilih.index');
                Route::get('/create', [CalonPemilihController::class, 'create'])->name('calonPemilih.create');
                Route::post('/store', [CalonPemilihController::class, 'store'])->name('calonPemilih.store');
                Route::get('/show', [CalonPemilihController::class, 'show'])->name('calonPemilih.show');
                Route::get('/edit', [CalonPemilihController::class, 'edit'])->name('calonPemilih.edit');
                Route::patch('/update', [CalonPemilihController::class, 'update'])->name('calonPemilih.update');
                Route::delete('/destroy', [CalonPemilihController::class, 'destroy'])->name('calonPemilih.destroy');
            });
        });

        // Layanan Surat
        Route::group([
            'namespace' => 'App\Http\Controllers\LayananSurat'
        ], function () {
            Route::prefix('pengaturan-surat')->group(function () {
                Route::get('/', [PengaturanSuratController::class, 'index'])->name('pengaturanSurat.index');
                Route::get('/create', [PengaturanSuratController::class, 'create'])->name('pengaturanSurat.create');
                Route::post('/store', [PengaturanSuratController::class, 'store'])->name('pengaturanSurat.store');
                Route::get('/show', [PengaturanSuratController::class, 'show'])->name('pengaturanSurat.show');
                Route::get('/edit', [PengaturanSuratController::class, 'edit'])->name('pengaturanSurat.edit');
                Route::patch('/update', [PengaturanSuratController::class, 'update'])->name('pengaturanSurat.update');
                Route::delete('/destroy', [PengaturanSuratController::class, 'destroy'])->name('pengaturanSurat.destroy');
            });

            Route::prefix('cetak-surat')->group(function () {
                Route::get('/', [CetakSuratController::class, 'index'])->name('cetakSurat.index');
                Route::get('/create', [CetakSuratController::class, 'create'])->name('cetakSurat.create');
                Route::post('/store', [CetakSuratController::class, 'store'])->name('cetakSurat.store');
                Route::get('/show', [CetakSuratController::class, 'show'])->name('cetakSurat.show');
                Route::get('/edit', [CetakSuratController::class, 'edit'])->name('cetakSurat.edit');
                Route::patch('/update', [CetakSuratController::class, 'update'])->name('cetakSurat.update');
                Route::delete('/destroy', [CetakSuratController::class, 'destroy'])->name('cetakSurat.destroy');
            });
            Route::prefix('arsip-layanan')->group(function () {
                Route::get('/', [ArsipLayananSuratController::class, 'index'])->name('arsipLayanan.index');
                Route::get('/create', [ArsipLayananSuratController::class, 'create'])->name('arsipLayanan.create');
                Route::post('/store', [ArsipLayananSuratController::class, 'store'])->name('arsipLayanan.store');
                Route::get('/show', [ArsipLayananSuratController::class, 'show'])->name('arsipLayanan.show');
                Route::get('/edit', [ArsipLayananSuratController::class, 'edit'])->name('arsipLayanan.edit');
                Route::patch('/update', [ArsipLayananSuratController::class, 'update'])->name('arsipLayanan.update');
                Route::delete('/destroy', [ArsipLayananSuratController::class, 'destroy'])->name('arsipLayanan.destroy');
            });
            Route::prefix('panduan')->group(function () {
                Route::get('/', [PanduanSuratController::class, 'index'])->name('panduan.index');
                Route::get('/create', [PanduanSuratController::class, 'create'])->name('panduan.create');
                Route::post('/store', [PanduanSuratController::class, 'store'])->name('panduan.store');
                Route::get('/show', [PanduanSuratController::class, 'show'])->name('panduan.show');
                Route::get('/edit', [PanduanSuratController::class, 'edit'])->name('panduan.edit');
                Route::patch('/update', [PanduanSuratController::class, 'update'])->name('panduan.update');
                Route::delete('/destroy', [PanduanSuratController::class, 'destroy'])->name('panduan.destroy');
            });

            Route::prefix('daftar-persyaratan')->group(function () {
                Route::get('/', [DaftarPersyaratanController::class, 'index'])->name('daftarPersyaratan.index');
                Route::get('/create', [DaftarPersyaratanController::class, 'create'])->name('daftarPersyaratan.create');
                Route::post('/store', [DaftarPersyaratanController::class, 'store'])->name('daftarPersyaratan.store');
                Route::get('/show', [DaftarPersyaratanController::class, 'show'])->name('daftarPersyaratan.show');
                Route::get('/edit', [DaftarPersyaratanController::class, 'edit'])->name('daftarPersyaratan.edit');
                Route::patch('/update', [DaftarPersyaratanController::class, 'update'])->name('daftarPersyaratan.update');
                Route::delete('/destroy', [DaftarPersyaratanController::class, 'destroy'])->name('daftarPersyaratan.destroy');
            });
        });
        // Layanan Mandiri
        Route::group([
            'namespace' => 'App\Http\Controllers\LayananMandiri'
        ], function () {
            Route::prefix('permohonan-surat')->group(function () {
                Route::get('/', [PermohonanSuratController::class, 'index'])->name('permohonanSurat.index');
                Route::get('/create', [PermohonanSuratController::class, 'create'])->name('permohonanSurat.create');
                Route::post('/store', [PermohonanSuratController::class, 'store'])->name('permohonanSurat.store');
                Route::get('/show', [PermohonanSuratController::class, 'show'])->name('permohonanSurat.show');
                Route::get('/edit', [PermohonanSuratController::class, 'edit'])->name('permohonanSurat.edit');
                Route::patch('/update', [PermohonanSuratController::class, 'update'])->name('permohonanSurat.update');
                Route::delete('/destroy', [PermohonanSuratController::class, 'destroy'])->name('permohonanSurat.destroy');
            });

            Route::prefix('kotak-pesan')->group(function () {
                Route::get('/', [KotakPesanController::class, 'index'])->name('kotakPesan.index');
                Route::get('/create', [KotakPesanController::class, 'create'])->name('kotakPesan.create');
                Route::post('/store', [KotakPesanController::class, 'store'])->name('kotakPesan.store');
                Route::get('/show', [KotakPesanController::class, 'show'])->name('kotakPesan.show');
                Route::get('/edit', [KotakPesanController::class, 'edit'])->name('kotakPesan.edit');
                Route::patch('/update', [KotakPesanController::class, 'update'])->name('kotakPesan.update');
                Route::delete('/destroy', [KotakPesanController::class, 'destroy'])->name('kotakPesan.destroy');
            });
            Route::prefix('pendaftar-layanan-mandiri')->group(function () {
                Route::get('/', [PendaftarLayananMandiriController::class, 'index'])->name('pendaftarLayananMandiri.index');
                Route::get('/create', [PendaftarLayananMandiriController::class, 'create'])->name('pendaftarLayananMandiri.create');
                Route::post('/store', [PendaftarLayananMandiriController::class, 'store'])->name('pendaftarLayananMandiri.store');
                Route::get('/show', [PendaftarLayananMandiriController::class, 'show'])->name('pendaftarLayananMandiri.show');
                Route::get('/edit', [PendaftarLayananMandiriController::class, 'edit'])->name('pendaftarLayananMandiri.edit');
                Route::patch('/update', [PendaftarLayananMandiriController::class, 'update'])->name('pendaftarLayananMandiri.update');
                Route::delete('/destroy', [PendaftarLayananMandiriController::class, 'destroy'])->name('pendaftarLayananMandiri.destroy');
            });
            Route::prefix('anjungan')->group(function () {
                Route::get('/', [AnjunganController::class, 'index'])->name('anjungan.index');
                Route::get('/create', [AnjunganController::class, 'create'])->name('anjungan.create');
                Route::post('/store', [AnjunganController::class, 'store'])->name('anjungan.store');
                Route::get('/show', [AnjunganController::class, 'show'])->name('anjungan.show');
                Route::get('/edit', [AnjunganController::class, 'edit'])->name('anjungan.edit');
                Route::patch('/update', [AnjunganController::class, 'update'])->name('anjungan.update');
                Route::delete('/destroy', [AnjunganController::class, 'destroy'])->name('anjungan.destroy');
            });

            Route::prefix('pendapat')->group(function () {
                Route::get('/', [PendapatController::class, 'index'])->name('pendapat.index');
                Route::get('/create', [PendapatController::class, 'create'])->name('pendapat.create');
                Route::post('/store', [PendapatController::class, 'store'])->name('pendapat.store');
                Route::get('/show', [PendapatController::class, 'show'])->name('pendapat.show');
                Route::get('/edit', [PendapatController::class, 'edit'])->name('pendapat.edit');
                Route::patch('/update', [PendapatController::class, 'update'])->name('pendapat.update');
                Route::delete('/destroy', [PendapatController::class, 'destroy'])->name('pendapat.destroy');
            });
        });

        Route::post('/logout', function () {
            Auth::guard('admin')->logout();
            return redirect()->action([
                LoginController::class,
                'login'
            ]);
        })->name('logout');
    });
});
