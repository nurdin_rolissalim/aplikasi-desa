$(document).ready(function () {
  $('.datatable').DataTable({
    responsive: false,
    processing: true, //Feature control the processing indicator.
    serverSide: true, //Feature control DataTables' server-side processing mode.
    ajax: {
      url: route_datatable,
      type: "post",
      data: function (data) {
          data._token = $("meta[name='csrf-token']").attr("content");
          data.keyword =$('.dataTables_filter input').val();
      },
      columns: [
        { "data": "name" },
        { "data": "username" },
        { "data": "email" },
        { "data": "phone" }
        ],
      columnDefs: [{
          targets: [-1], //last column
          orderable: false, //set not orderable
      }],
  },
  });
  $(".dataTables_length select").addClass('form-select form-select-sm');
})