(function($) {

    function initCounterNumber() {
        var counter = document.querySelectorAll('.counter-value');
        var speed = 250; // The lower the slower
        counter.forEach(function(counter_value) {
            function updateCount() {
                var target = +counter_value.getAttribute('data-target');
                var count = +counter_value.innerText;
                var inc = target / speed;
                if (inc < 1) {
                    inc = 1;
                }
                // Check if target is reached
                if (count < target) {
                    // Add inc to count and output in counter_value
                    counter_value.innerText = (count + inc).toFixed(0);
                    // Call function every ms
                    setTimeout(updateCount, 1);
                } else {
                    counter_value.innerText = target;
                }
            };
            updateCount();
        });
    }

// get colors array from the string
function getChartColorsArray(chartId) {
    var colors = $(chartId).attr('data-colors');
    var colors = JSON.parse(colors);
    return colors.map(function (value) {
        var newValue = value.replace(' ', '');
        if (newValue.indexOf('--') != -1) {
            var color = getComputedStyle(document.documentElement).getPropertyValue(newValue);
            if (color) return color;
        } else {
            return newValue;
        }
    })
}

//  MINI CHART

// mini-1
var barchartColors = getChartColorsArray("#penduduk");
var options = {
    series: [60, 40],
    chart: {
        type: 'donut',
        height: 110,
    },
    labels: ['Laki-Laki', 'Perempuan'],
    colors: barchartColors,
    legend: {
        show: false
    },
    dataLabels: {
        enabled: false
    }
};

var chart = new ApexCharts(document.querySelector("#penduduk"), options);
chart.render();


var barchartColors = getChartColorsArray("#calon-pemilih");
var options = {
    series: [60, 40],
    chart: {
        type: 'donut',
        height: 110,
    },
    colors: barchartColors,
    legend: {
        show: false
    },
    dataLabels: {
        enabled: false
    }
};

var chart = new ApexCharts(document.querySelector("#calon-pemilih"), options);
chart.render();


var barchartColors = getChartColorsArray("#program-desa");
var options = {
    series: [60, 40],
    chart: {
        type: 'donut',
        height: 110,
    },
    colors: barchartColors,
    legend: {
        show: false
    },
    dataLabels: {
        enabled: false
    }
};

var chart = new ApexCharts(document.querySelector("#program-desa"), options);
chart.render();


var barchartColors = getChartColorsArray("#pengajuan-surat");
var options = {
    series: [60, 40],
    chart: {
        type: 'donut',
        height: 110,
    },
    colors: barchartColors,
    legend: {
        show: false
    },
    dataLabels: {
        enabled: false
    }
};

var chart = new ApexCharts(document.querySelector("#pengajuan-surat"), options);
chart.render();

var barchartColors = getChartColorsArray("#market-overview");
var options = {
  series: [{
    name: 'Profit',
    data: [12.45, 16.2, 8.9, 11.42, 12.6, 18.1, 18.2, 14.16, 11.1, 8.09, 16.34, 12.88]
  }, {
    name: 'Loss',
    data: [-11.45, -15.42, -7.9, -12.42, -12.6, -18.1, -18.2, -14.16, -11.1, -7.09, -15.34, -11.88]
  }],
  chart: {
    type: 'bar',
    height: 400,
    stacked: true,
    toolbar: {
      show: false
    }
  },
  plotOptions: {
    bar: {
      columnWidth: '20%'
    }
  },
  colors: barchartColors,
  fill: {
    opacity: 1
  },
  dataLabels: {
    enabled: false
  },
  legend: {
    show: false
  },
  yaxis: {
    labels: {
      formatter: function formatter(y) {
        return y.toFixed(0) + "%";
      }
    }
  },
  xaxis: {
    categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
    labels: {
      rotate: -90
    }
  }
};
var chart = new ApexCharts(document.querySelector("#market-overview"), options);
chart.render(); // MAp


var barchartColors = getChartColorsArray("#market-overview");
var options = {
  series: [{
    name: 'Profit',
    data: [12.45, 16.2, 8.9, 11.42, 12.6, 18.1, 18.2, 14.16, 11.1, 8.09, 16.34, 12.88]
  }, {
    name: 'Loss',
    data: [-11.45, -15.42, -7.9, -12.42, -12.6, -18.1, -18.2, -14.16, -11.1, -7.09, -15.34, -11.88]
  }],
  chart: {
    type: 'bar',
    height: 400,
    stacked: true,
    toolbar: {
      show: false
    }
  },
  plotOptions: {
    bar: {
      columnWidth: '20%'
    }
  },
  colors: barchartColors,
  fill: {
    opacity: 1
  },
  dataLabels: {
    enabled: false
  },
  legend: {
    show: false
  },
  yaxis: {
    labels: {
      formatter: function formatter(y) {
        return y.toFixed(0) + "%";
      }
    }
  },
  xaxis: {
    categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
    labels: {
      rotate: -90
    }
  }
};
var chart = new ApexCharts(document.querySelector("#dana-desa"), options);
chart.render(); // MAp



// var options = {
//   series: [{
//   data: [40, 430, 648, 370, 240]
// }],
//   chart: {
//   type: 'bar',
//   height: 350
// },
// plotOptions: {
//   bar: {
//     borderRadius: 4,
//     horizontal: true,
//   }
// },
// dataLabels: {
//   enabled: false
// },
// xaxis: {
//   categories: ['Balita (0-5)', 'Anak-Anak(6-17)', 'Dewasa(18-30)', 'Tua', '31-200'    ],
// }
// };

// var chart = new ApexCharts(document.querySelector("#age"), options);
// chart.render();

function init() {
    initCounterNumber();     
}

init();

})(jQuery)