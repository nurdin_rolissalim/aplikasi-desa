(function($) {

    function initCounterNumber() {
        var counter = document.querySelectorAll('.counter-value');
        var speed = 250; // The lower the slower
        counter.forEach(function(counter_value) {
            function updateCount() {
                var target = +counter_value.getAttribute('data-target');
                var count = +counter_value.innerText;
                var inc = target / speed;
                if (inc < 1) {
                    inc = 1;
                }
                // Check if target is reached
                if (count < target) {
                    // Add inc to count and output in counter_value
                    counter_value.innerText = (count + inc).toFixed(0);
                    // Call function every ms
                    setTimeout(updateCount, 1);
                } else {
                    counter_value.innerText = target;
                }
            };
            updateCount();
        });
    }

// get colors array from the string
function getChartColorsArray(chartId) {
    var colors = $(chartId).attr('data-colors');
    var colors = JSON.parse(colors);
    return colors.map(function (value) {
        var newValue = value.replace(' ', '');
        if (newValue.indexOf('--') != -1) {
            var color = getComputedStyle(document.documentElement).getPropertyValue(newValue);
            if (color) return color;
        } else {
            return newValue;
        }
    })
}

//  MINI CHART

var barchartColors = getChartColorsArray("#pie-penduduk");
var options = {
  series: [60, 40],
  labels: ['Male', 'Female'],
  chart: {
    type: 'donut',
    height: 110
  },  
  colors: barchartColors,
  legend: {
    show: false
  },
  dataLabels: {
    enabled: false
  }
};
var chart = new ApexCharts(document.querySelector("#pie-penduduk"), options);
chart.render(); // mini-2

var barchartColors = getChartColorsArray("#pie-calon-pemilih");
var options = {
  series: [45, 55],
  labels: ['Male', 'Female'],
  chart: {
    type: 'donut',
    height: 110
  },
  colors: barchartColors,
  legend: {
    show: false
  },
  dataLabels: {
    enabled: false
  }
};
var chart = new ApexCharts(document.querySelector("#pie-calon-pemilih"), options);
chart.render(); 

var barchartColors = getChartColorsArray("#pie-pengajuan-surat");
var options = {
  series: [65, 35],
  labels: ['Surat Selesai', 'Surat In Progress'],
  chart: {
    type: 'donut',
    height: 110
  },
  colors: barchartColors,
  legend: {
    show: false
  },
  dataLabels: {
    enabled: false
  }
};
var chart = new ApexCharts(document.querySelector("#pie-pengajuan-surat"), options);
chart.render(); 

var barchartColors = getChartColorsArray("#pie-program-desa");
var options = {
  series: [30, 70],
  chart: {
    type: 'donut',
    height: 110
  },
  labels: ['Belum Terlaksana', 'Terlaksana'],
  colors: barchartColors,
  legend: {
    show: false
  },
  dataLabels: {
    enabled: false
  }
};
var chart = new ApexCharts(document.querySelector("#pie-program-desa"), options);
chart.render();

var options = {
  series: [{
    name:"Total",
  data: [40, 430, 648, 240]
}],
  chart: {
  type: 'bar',
  height: 200,
  toolbar: {
    show: false
  }
},
plotOptions: {
  bar: {
    borderRadius: 4,
    horizontal: true,
  }
},
dataLabels: {
  enabled: false
},
xaxis: {
  categories: ['0-5', '6-17', '18-30', '31-200'    ],
}
};

var chart = new ApexCharts(document.querySelector("#age"), options);
chart.render();


var options = {
  series: [{
    name:"Total",
  data: [40, 430, 300, 200,10,20,50,30,300,50,30]
}],
labels:['TS', 'SD', 'SLTP', 'SLTA','D1','D2',"D3","D4","S1","S2","S3"    ],
  chart: {
  type: 'bar',
  height: 200,
  toolbar: {
    show: false
  }
},
plotOptions: {
  bar: {
    borderRadius: 2,
    horizontal: true,
  }
},
dataLabels: {
  enabled: false,
 
},
xaxis: {
  categories: ['TS', 'SD', 'SLTP', 'SLTA','D1','D2',"D3","D4","S1","S2","S3"    ],
}
};

var chart = new ApexCharts(document.querySelector("#pendidikan"), options);
chart.render();


var options = {
  series: [{
    name:"Total",
  data: [40, 430, 30,100]
}],
  chart: {
  type: 'bar',
  height: 200,
  toolbar: {
    show: false
  }
},
plotOptions: {
  bar: {
    borderRadius: 2,
    horizontal: true,
  }
},
dataLabels: {
  enabled: false,
 
},
xaxis: {
  categories: ['Lajang', 'Kawin', 'Duda', 'Janda'],
}
};

var chart = new ApexCharts(document.querySelector("#perkawinan"), options);
chart.render();

function init() {
    initCounterNumber();     
}

init();

})(jQuery)